package com.ltos.livecrkttv.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.ltos.livecrkttv.R;
import com.ltos.livecrkttv.model.CommentaryModel;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;


public class CommentaryDetailsAdapter extends RecyclerView.Adapter<CommentaryDetailsAdapter.MyViewHolder> {
    final ArrayList<CommentaryModel.Innings.Overs.Balls> ballsArrayList;
    ArrayList<CommentaryModel.Innings.Overs.Balls.Comments> commentsArrayList;
    final int moverNumber;

    public CommentaryDetailsAdapter(Context context, ArrayList<CommentaryModel.Innings.Overs.Balls> arrayList, int overNumber) {
        ballsArrayList = arrayList;
        moverNumber = overNumber;
    }

    @NotNull
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_commentary_details, parent, false);
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NotNull MyViewHolder holder, int position) {
        try {
            holder.tv_balls.setText(String.format("%s.%s", moverNumber - 1, ballsArrayList.get(position).getBallNumber()));

            commentsArrayList = ballsArrayList.get(position).getComments();
            String comments = commentsArrayList.get((commentsArrayList.size() - 1)).getMessage();
            if (comments.contains("FOUR!")) {
                holder.tv_extra.setText("4");
                holder.tv_extra.setBackgroundResource(R.drawable.circle_blue);
            } else if (comments.contains("SIX!")) {
                holder.tv_extra.setText("6");
                holder.tv_extra.setBackgroundResource(R.drawable.circle_purple);
            }else if (comments.contains("OUT!")) {
                holder.tv_extra.setText("W");
                holder.tv_extra.setBackgroundResource(R.drawable.circle_red);
            }
            holder.tv_commentry.setText(comments);
            holder.itemView.setOnClickListener(view -> {

            });
        } catch (Exception exception) {
            exception.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return ballsArrayList.size();
    }


    public static class MyViewHolder extends RecyclerView.ViewHolder {

        public final TextView tv_commentry;
        public final TextView tv_balls;
        public final TextView tv_extra;


        public MyViewHolder(View v) {
            super(v);
            tv_commentry = v.findViewById(R.id.tv_commentry);
            tv_commentry.setLayerType(View.LAYER_TYPE_SOFTWARE,null);
            tv_balls = v.findViewById(R.id.tv_balls);
            tv_extra = v.findViewById(R.id.tv_extra);
        }
    }

}