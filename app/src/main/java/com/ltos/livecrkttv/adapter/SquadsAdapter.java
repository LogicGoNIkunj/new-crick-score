package com.ltos.livecrkttv.adapter;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.ltos.livecrkttv.R;
import com.ltos.livecrkttv.model.SquadsModel;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Objects;

import de.hdodenhof.circleimageview.CircleImageView;


public class SquadsAdapter extends RecyclerView.Adapter<SquadsAdapter.MyViewHolder> {
    private final Context acontext;
    final ArrayList<SquadsModel> squadsModelArrayList;


    public SquadsAdapter(Context context, ArrayList<SquadsModel> arrayList) {
        acontext = context;
        squadsModelArrayList = arrayList;
    }

    @NotNull
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.squadlist_item, parent, false);
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NotNull MyViewHolder holder, @SuppressLint("RecyclerView") int position) {
        try {

            holder.playerimg.setImageResource(squadsModelArrayList.get(position).getPlayer_profile());
            holder.player_status.setImageResource(squadsModelArrayList.get(position).getPlayer_status());
            holder.name.setText(squadsModelArrayList.get(position).getPlayer_name());
            holder.price.setText(squadsModelArrayList.get(position).getPlayer_price());
            holder.mainly.setLayerType(View.LAYER_TYPE_SOFTWARE,null);

            holder.mainly.setOnClickListener(view -> {
                Dialog dialog = new Dialog(acontext);
                dialog.setContentView(R.layout.player_details_dialog);
                Window window = dialog.getWindow();
                Objects.requireNonNull(window);
                window.setBackgroundDrawable(new ColorDrawable(0));
                ((ImageView) dialog.findViewById(R.id.Dplayer_pic)).setImageResource(squadsModelArrayList.get(position).getPlayer_profile());
                ((TextView) dialog.findViewById(R.id.Dplayer_name)).setText(squadsModelArrayList.get(position).getPlayer_name());
                ((TextView) dialog.findViewById(R.id.DP_Role)).setText(squadsModelArrayList.get(position).getRole());
                ((TextView) dialog.findViewById(R.id.DP_battingStyle)).setText(squadsModelArrayList.get(position).getBatting_style());
                ((TextView) dialog.findViewById(R.id.DP_bowlingStyle)).setText(squadsModelArrayList.get(position).getBowling_style());
                ((TextView) dialog.findViewById(R.id.DP_nationality)).setText(squadsModelArrayList.get(position).getNationality());
                dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
                dialog.show();
                dialog.findViewById(R.id.btnDialog).setOnClickListener(
                        view1 -> dialog.dismiss());
            });
        } catch (Exception exception) {
            exception.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return squadsModelArrayList.size();
    }


    public static class MyViewHolder extends RecyclerView.ViewHolder {

        public final CircleImageView playerimg;
        public final TextView name;
        public final TextView price;
        public final ImageView player_status;
        public final CardView mainly;

        public MyViewHolder(View v) {
            super(v);
            name = v.findViewById(R.id.player_name);
            playerimg = v.findViewById(R.id.player_profile);
            price = v.findViewById(R.id.player_price);
            player_status = v.findViewById(R.id.player_status);
            mainly = v.findViewById(R.id.mainly);
        }
    }

}