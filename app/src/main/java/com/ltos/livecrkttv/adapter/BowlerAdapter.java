package com.ltos.livecrkttv.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.ltos.livecrkttv.R;
import com.ltos.livecrkttv.activity.PlayerInfoActivity;
import com.ltos.livecrkttv.model.Scorecard;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;


public class BowlerAdapter extends RecyclerView.Adapter<BowlerAdapter.MyViewHolder> {
    private final Context acontext;
    final ArrayList<Scorecard.Fixture.Innings.Bowlers> batsmanArrayList;
    final String mmatchdata;

    public BowlerAdapter(Context context, ArrayList<Scorecard.Fixture.Innings.Bowlers> arrayList, String matchdata) {
        acontext = context;
        batsmanArrayList = arrayList;
        mmatchdata = matchdata;
    }

    @NotNull
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_bowling, parent, false);
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NotNull MyViewHolder holder, int position) {
        try {
            Scorecard.Fixture.Innings.Bowlers bowlers = batsmanArrayList.get(position);
            if (bowlers.ballsBowled == 0) {
                holder.tv_overs.setText(String.format("%s", bowlers.oversBowled));
            }else{
                holder.tv_overs.setText(String.format("%s.%s", bowlers.oversBowled, bowlers.ballsBowled));
            }
            holder.tv_maidens.setText(String.format("%s", bowlers.maidensBowled));
            holder.tv_runs.setText(String.format("%s", bowlers.runsConceded));
            holder.tv_wickets.setText(String.format("%s", bowlers.wicketsTaken));
            holder.tv_er.setText(String.format("%s", bowlers.economy));
            JSONArray jsonArray = new JSONObject(mmatchdata).getJSONArray("players");
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                int playerid = jsonObject.getInt("id");
                if (playerid == bowlers.playerId) {
                    holder.tv_bowler.setText(jsonObject.getString("displayName"));
                    Glide.with(acontext).load(jsonObject.getString("imageUrl")).placeholder(R.drawable.manplaceholder).into(holder.playerimg);
                }
            }
            holder.itemView.setOnClickListener(view -> {
                Intent intent = new Intent(acontext, PlayerInfoActivity.class);
                intent.putExtra("MATCHDATA", mmatchdata);
                intent.putExtra("PLAYER_ID", bowlers.playerId);
                intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                acontext.startActivity(intent);
            });
        } catch (Exception exception) {
            exception.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return batsmanArrayList.size();
    }


    public static class MyViewHolder extends RecyclerView.ViewHolder {

        public final TextView tv_bowler;
        public final TextView tv_overs;
        public final TextView tv_maidens;
        public final TextView tv_runs;
        public final TextView tv_wickets;
        public final TextView tv_er;
        public final ImageView playerimg;


        public MyViewHolder(View v) {
            super(v);
            playerimg = v.findViewById(R.id.playerimg);
            tv_bowler = v.findViewById(R.id.tv_batsman);
            tv_overs = v.findViewById(R.id.tv_run);
            tv_maidens = v.findViewById(R.id.tv_balls);
            tv_runs = v.findViewById(R.id.tv_fours);
            tv_wickets = v.findViewById(R.id.tv_sixs);
            tv_er = v.findViewById(R.id.tv_srate);
        }
    }

}