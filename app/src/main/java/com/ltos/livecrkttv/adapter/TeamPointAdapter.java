package com.ltos.livecrkttv.adapter;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.ltos.livecrkttv.model.Datum;
import com.ltos.livecrkttv.R;
import com.ltos.livecrkttv.activity.PointsTableActivity;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class TeamPointAdapter extends RecyclerView.Adapter<TeamPointAdapter.DataHolder> {
    final ArrayList<Datum> pointData;
    final PointsTableActivity pointsTable_activity;

    public TeamPointAdapter(PointsTableActivity pointsTable_Activity, ArrayList<Datum> arrayList) {
        this.pointData = arrayList;
        this.pointsTable_activity = pointsTable_Activity;
    }

    @NonNull
    public DataHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new DataHolder(LayoutInflater.from(this.pointsTable_activity).inflate(R.layout.point_table_item, viewGroup, false));
    }

    @SuppressLint("SetTextI18n")
    public void onBindViewHolder(DataHolder dataHolder, int i) {
        TextView textView = dataHolder.txt_TeamName;
        textView.setText("" + this.pointData.get(0).getContents().get(i).getTeam());
        TextView textView2 = dataHolder.txt_pld;
        textView2.setText("" + this.pointData.get(0).getContents().get(i).getPld());
        TextView textView3 = dataHolder.txt_NetRR;
        textView3.setText("" + this.pointData.get(0).getContents().get(i).getNetrr());
        TextView textView4 = dataHolder.txt_PTS;
        textView4.setText("" + this.pointData.get(0).getContents().get(i).getPts());
        TextView textView5 = dataHolder.txt_Won;
        textView5.setText("" + this.pointData.get(0).getContents().get(i).getWon());
        TextView textView6 = dataHolder.txt_Lost;
        textView6.setText("" + this.pointData.get(0).getContents().get(i).getLost());
        Glide.with(this.pointsTable_activity).load(this.pointData.get(0).getContents().get(i).getRoundSmall()).into(dataHolder.imgTeam_logo);
    }

    public int getItemCount() {
        return this.pointData.get(0).getContents().size();
    }

    public static class DataHolder extends RecyclerView.ViewHolder {
        final CircleImageView imgTeam_logo;
        final TextView txt_Lost;
        final TextView txt_NetRR;
        final TextView txt_PTS;
        final TextView txt_TeamName;
        final TextView txt_Won;
        final TextView txt_pld;


        public DataHolder(View view) {
            super(view);
            this.imgTeam_logo = view.findViewById(R.id.imgTeam_logo);
            this.txt_TeamName = view.findViewById(R.id.txt_TeamName);
            this.txt_pld = view.findViewById(R.id.txt_pld);
            this.txt_NetRR = view.findViewById(R.id.txt_NetRR);
            this.txt_PTS = view.findViewById(R.id.txt_PTS);
            this.txt_Won = view.findViewById(R.id.txt_Won);
            this.txt_Lost = view.findViewById(R.id.txt_Lost);
        }
    }
}
