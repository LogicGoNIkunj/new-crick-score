package com.ltos.livecrkttv.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ltos.livecrkttv.R;
import com.ltos.livecrkttv.model.CommentaryModel;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;



public class CommentaryAdapter extends RecyclerView.Adapter<CommentaryAdapter.MyViewHolder> {
    private final Context acontext;
    final ArrayList<CommentaryModel.Innings.Overs> oversArrayList;
    ArrayList<CommentaryModel.Innings.Overs.Balls> ballsArrayList;
    final int mtype;

    public CommentaryAdapter(Context context, ArrayList<CommentaryModel.Innings.Overs> arrayList, int type) {
        acontext = context;
        oversArrayList = arrayList;
        mtype = type;
    }

    @NotNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (mtype == 1) {
            return new MyViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_commentary, parent, false));
        } else {
            return new MyViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_commentary_highlight, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(@NotNull MyViewHolder holder, int position) {
        try {
            CommentaryModel.Innings.Overs overs = oversArrayList.get(position);
            if (mtype == 1) {
                holder.tv_overs_runs.setText(String.format("%s OVERS    %s Runs", overs.getOverNumber(), overs.getTotalRuns()));
                holder.tv_total_run.setText(String.format("%s - %s", overs.getTotalInningRuns(), overs.getTotalInningWickets()));
                holder.tv_run_rate.setText(String.format("CRR %s", overs.getRunrate()));
            }
            LinearLayoutManager linearLayoutManager = new GridLayoutManager(acontext, 1);
            holder.rv_commentry.setLayoutManager(linearLayoutManager);
            ballsArrayList = oversArrayList.get(position).getBalls();
            CommentaryDetailsAdapter matchAdapter = new CommentaryDetailsAdapter(acontext, ballsArrayList, overs.getOverNumber());
            holder.rv_commentry.setAdapter(matchAdapter);
            holder.itemView.setOnClickListener(view -> {

            });
        } catch (Exception exception) {
            exception.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return oversArrayList.size();
    }


    public static class MyViewHolder extends RecyclerView.ViewHolder {

        public final TextView tv_overs_runs;
        public final TextView tv_total_run;
        public final TextView tv_run_rate;
        public final RecyclerView rv_commentry;


        public MyViewHolder(View v) {
            super(v);
            tv_overs_runs = v.findViewById(R.id.tv_overs_runs);
            tv_total_run = v.findViewById(R.id.tv_total_run);
            tv_run_rate = v.findViewById(R.id.tv_run_rate);
            rv_commentry = v.findViewById(R.id.rv_commentry);
        }
    }

}