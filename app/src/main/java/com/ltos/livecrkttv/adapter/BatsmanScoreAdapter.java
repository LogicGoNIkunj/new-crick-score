package com.ltos.livecrkttv.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.ltos.livecrkttv.R;
import com.ltos.livecrkttv.activity.PlayerInfoActivity;
import com.ltos.livecrkttv.model.Scorecard;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;


public class BatsmanScoreAdapter extends RecyclerView.Adapter<BatsmanScoreAdapter.MyViewHolder> {
    private final Context acontext;
    final ArrayList<Scorecard.Fixture.Innings.Batsman> batsmanArrayList;
    final String mmatchdata;

    public BatsmanScoreAdapter(Context context, ArrayList<Scorecard.Fixture.Innings.Batsman> arrayList, String matchdata) {
        acontext = context;
        batsmanArrayList = arrayList;
        mmatchdata = matchdata;
    }

    @NotNull
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_batsman, parent, false);
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NotNull MyViewHolder holder, int position) {
        try {
            Scorecard.Fixture.Innings.Batsman batsman = batsmanArrayList.get(position);
            holder.tv_out_details.setText(String.format("%s", batsman.dismissalText));
            holder.tv_run.setText(String.format("%s", batsman.runsScored));
            holder.tv_balls.setText(String.format("%s", batsman.ballsFaced));
            holder.tv_fours.setText(String.format("%s", batsman.foursScored));
            holder.tv_sixs.setText(String.format("%s", batsman.sixesScored));
            holder.tv_srate.setText(String.format("%s", batsman.strikeRate));


            if (batsman.dismissalText.equals("not out")){
                holder.linearLayout5.setBackgroundColor(Color.parseColor("#0A01AEFE"));
            }else {
                holder.linearLayout5.setBackgroundColor(0);
            }

            JSONArray jsonArray = new JSONObject(mmatchdata).getJSONArray("players");
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                int playerid = jsonObject.getInt("id");
                if (playerid == batsman.playerId) {
                    holder.tv_batsman.setText(jsonObject.getString("displayName"));
                    Glide.with(acontext).load(jsonObject.getString("imageUrl")).placeholder(R.drawable.manplaceholder).into(holder.playerimg);
                }
            }


            holder.itemView.setOnClickListener(view -> {
                Intent intent = new Intent(acontext, PlayerInfoActivity.class);
                intent.putExtra("MATCHDATA", mmatchdata);
                intent.putExtra("PLAYER_ID", batsman.playerId);
                intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                acontext.startActivity(intent);
            });
        } catch (Exception exception) {
            exception.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return batsmanArrayList.size();
    }


    public static class MyViewHolder extends RecyclerView.ViewHolder {

        public final TextView tv_batsman;
        public final TextView tv_run;
        public final TextView tv_balls;
        public final TextView tv_fours;
        public final TextView tv_sixs;
        public final TextView tv_srate;
        public final ImageView playerimg;
        public final TextView tv_out_details;
        public final LinearLayout linearLayout5;


        public MyViewHolder(View v) {
            super(v);
            tv_batsman = v.findViewById(R.id.tv_batsman);
            playerimg = v.findViewById(R.id.playerimg);
            tv_out_details = v.findViewById(R.id.tv_out_details);
            tv_run = v.findViewById(R.id.tv_run);
            tv_balls = v.findViewById(R.id.tv_balls);
            tv_fours = v.findViewById(R.id.tv_fours);
            tv_sixs = v.findViewById(R.id.tv_sixs);
            tv_srate = v.findViewById(R.id.tv_srate);
            linearLayout5 = v.findViewById(R.id.linearLayout5);
        }
    }

}