package com.ltos.livecrkttv.adapter;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ltos.livecrkttv.R;
import com.ltos.livecrkttv.activity.PointsTableActivity;
import com.ltos.livecrkttv.model.Pointtablenew;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class TeamPointAdapter2022 extends RecyclerView.Adapter<TeamPointAdapter2022.DataHolder> {
    final ArrayList<Pointtablenew.Data.GetPointsTable.Standing.Team> pointData;
    final PointsTableActivity pointsTable_activity;

    public TeamPointAdapter2022(PointsTableActivity pointsTable_Activity, ArrayList<Pointtablenew.Data.GetPointsTable.Standing.Team> arrayList) {
        this.pointData = arrayList;
        this.pointsTable_activity = pointsTable_Activity;
    }

    @NonNull
    public DataHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new DataHolder(LayoutInflater.from(this.pointsTable_activity).inflate(R.layout.point_table_item, viewGroup, false));
    }

    @SuppressLint({"SetTextI18n", "UseCompatLoadingForDrawables"})
    public void onBindViewHolder(DataHolder dataHolder, int i) {
        dataHolder.txt_TeamName.setText(pointData.get(i).getTeamShortName());
        TextView textView2 = dataHolder.txt_pld;
        textView2.setText(pointData.get(i).getAll());
        TextView textView3 = dataHolder.txt_NetRR;
        textView3.setText(pointData.get(i).getNrr());
        TextView textView4 = dataHolder.txt_PTS;
        textView4.setText(pointData.get(i).getPoints());
        TextView textView5 = dataHolder.txt_Won;
        textView5.setText(pointData.get(i).getWins());
        TextView textView6 = dataHolder.txt_Lost;
        textView6.setText(pointData.get(i).getLost());

        switch (pointData.get(i).getTeamShortName()) {
            case "DC":
                dataHolder.imgTeam_logo.setImageDrawable(pointsTable_activity.getDrawable(R.drawable.dcicon));
                break;
            case "PBKS":
                dataHolder.imgTeam_logo.setImageDrawable(pointsTable_activity.getDrawable(R.drawable.kxiiicon));
                break;
            case "KKR":
                dataHolder.imgTeam_logo.setImageDrawable(pointsTable_activity.getDrawable(R.drawable.kkricon));
                break;
            case "GT":
                dataHolder.imgTeam_logo.setImageDrawable(pointsTable_activity.getDrawable(R.drawable.gticon));
                break;
            case "RR":
                dataHolder.imgTeam_logo.setImageDrawable(pointsTable_activity.getDrawable(R.drawable.rricon));
                break;
            case "SRH":
                dataHolder.imgTeam_logo.setImageDrawable(pointsTable_activity.getDrawable(R.drawable.srhicon));
                break;
            case "LSG":
                dataHolder.imgTeam_logo.setImageDrawable(pointsTable_activity.getDrawable(R.drawable.lsgicon));
                break;
            case "CSK":
                dataHolder.imgTeam_logo.setImageDrawable(pointsTable_activity.getDrawable(R.drawable.cskicon));
                break;
            case "RCB":
                dataHolder.imgTeam_logo.setImageDrawable(pointsTable_activity.getDrawable(R.drawable.rcbion));
                break;
            case "MI":
                dataHolder.imgTeam_logo.setImageDrawable(pointsTable_activity.getDrawable(R.drawable.micon));
                break;
        }
    }

    public int getItemCount() {
        return this.pointData.size();
    }

    public static class DataHolder extends RecyclerView.ViewHolder {
        final CircleImageView imgTeam_logo;
        final TextView txt_Lost;
        final TextView txt_NetRR;
        final TextView txt_PTS;
        final TextView txt_TeamName;
        final TextView txt_Won;
        final TextView txt_pld;


        public DataHolder(View view) {
            super(view);
            this.imgTeam_logo = view.findViewById(R.id.imgTeam_logo);
            this.txt_TeamName = view.findViewById(R.id.txt_TeamName);
            this.txt_pld = view.findViewById(R.id.txt_pld);
            this.txt_NetRR = view.findViewById(R.id.txt_NetRR);
            this.txt_PTS = view.findViewById(R.id.txt_PTS);
            this.txt_Won = view.findViewById(R.id.txt_Won);
            this.txt_Lost = view.findViewById(R.id.txt_Lost);
        }
    }
}
