package com.ltos.livecrkttv.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ltos.livecrkttv.R;
import com.ltos.livecrkttv.model.CommentaryModel;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Collections;



public class OverCommentaryAdapter extends RecyclerView.Adapter<OverCommentaryAdapter.MyViewHolder> {
    private final Context acontext;
    final ArrayList<CommentaryModel.Innings.Overs> oversArrayList;
    ArrayList<CommentaryModel.Innings.Overs.Balls> ballsArrayList;

    public OverCommentaryAdapter(Context context, ArrayList<CommentaryModel.Innings.Overs> arrayList) {
        acontext = context;
        oversArrayList = arrayList;
    }

    @NotNull
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_over_commentary, parent, false);
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NotNull MyViewHolder holder, int position) {
        try {
            CommentaryModel.Innings.Overs overs = oversArrayList.get(position);

            holder.tv_overs_runs.setText(String.format("Ov %s\n\n%s runs", overs.getOverNumber(), overs.getTotalRuns()));


            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(acontext, RecyclerView.HORIZONTAL,false);
            holder.rv_commentry.setLayoutManager(linearLayoutManager);

            ballsArrayList=oversArrayList.get(position).getBalls();
            try {
                if(ballsArrayList.size()!=0){
                    String message =ballsArrayList.get((ballsArrayList.size()-1)).getComments().get(ballsArrayList.get((ballsArrayList.size()-1)).getComments().size()-1).getMessage();
                    String comment = message.substring(0, message.indexOf("."));

                    holder.tv_comment.setText(comment);
                }
            } catch (Exception exception) {
                exception.printStackTrace();
            }
            try {
                Collections.sort(ballsArrayList, (obj1, obj2) -> Integer.valueOf(obj1.getBallNumber()).compareTo(obj2.getBallNumber()));
            } catch (Exception exception) {
                exception.printStackTrace();
            }

            OverballsAdapter matchAdapter = new OverballsAdapter(acontext, ballsArrayList,overs.getOverNumber());
            holder.rv_commentry.setAdapter(matchAdapter);

        } catch (Exception exception) {
            exception.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return oversArrayList.size();
    }
    @Override
    public long getItemId(int position) {
        return oversArrayList == null ? 0 : oversArrayList.get(position).getId();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        public final TextView tv_overs_runs;
        public final TextView tv_comment;
        public final RecyclerView rv_commentry;


        public MyViewHolder(View v) {
            super(v);
            tv_overs_runs = v.findViewById(R.id.tv_overs_runs);
            tv_comment = v.findViewById(R.id.tv_comment);
            rv_commentry = v.findViewById(R.id.rv_commentry);
        }
    }

}