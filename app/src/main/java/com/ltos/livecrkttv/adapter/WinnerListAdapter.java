package com.ltos.livecrkttv.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.ConnectivityManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.ltos.livecrkttv.R;
import com.ltos.livecrkttv.model.WinnerListModel;
import com.ltos.livecrkttv.utils.MyApplication;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdLoader;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.nativead.NativeAd;
import com.google.android.gms.ads.nativead.NativeAdView;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Objects;


public class WinnerListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private final Context acontext;
    final ArrayList<WinnerListModel> winnerListModelArrayList;

    public static final int ITEM_TYPE_NORMAL = 0;
    public static final int ITEM_TYPE_NATIVE_AD = 1;

    public WinnerListAdapter(Context context, ArrayList<WinnerListModel> arrayList) {
        acontext = context;
        winnerListModelArrayList = arrayList;
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType) {
            case ITEM_TYPE_NATIVE_AD:
                View view = LayoutInflater.from(acontext).inflate(R.layout.item_native_adapter, parent, false);
                return new AdViewHolder(view);
            case ITEM_TYPE_NORMAL:
                View view1 = LayoutInflater.from(acontext).inflate(R.layout.winnerlist_item, parent, false);
                return new MyViewHolder(view1);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(@NotNull RecyclerView.ViewHolder holder, int position) {

        switch (holder.getItemViewType()) {
            case ITEM_TYPE_NATIVE_AD:
                ((AdViewHolder) holder).main_card.setLayerType(View.LAYER_TYPE_SOFTWARE,null);
                break;
            case ITEM_TYPE_NORMAL:
                try {
                    MyViewHolder myViewHolder = (MyViewHolder) holder;
                    myViewHolder.WinnerTeamPic.setImageResource(winnerListModelArrayList.get(position).getPicture());
                    myViewHolder.runnerUpID.setText(winnerListModelArrayList.get(position).getRunnerUp());
                    myViewHolder.winnerID.setText(winnerListModelArrayList.get(position).getWinner());
                    myViewHolder.yearID.setText(winnerListModelArrayList.get(position).getYear());
                    myViewHolder.orangecapID.setText(winnerListModelArrayList.get(position).getOrangeCap());
                    myViewHolder.purplecapID.setText(winnerListModelArrayList.get(position).getPurpleCap());
                    myViewHolder.manofthematchID.setText(winnerListModelArrayList.get(position).getManoftheMatch());
                    myViewHolder.playerofthematchID.setText(winnerListModelArrayList.get(position).getPlayeroftheTournament());
                    myViewHolder.winnerlist_cardView.setLayerType(View.LAYER_TYPE_SOFTWARE,null);
                } catch (Exception exception) {
                    exception.printStackTrace();
                }
                break;
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (winnerListModelArrayList.get(position).getAd()) {
            return ITEM_TYPE_NATIVE_AD;
        }
        return ITEM_TYPE_NORMAL;
    }

    public class AdViewHolder extends RecyclerView.ViewHolder {
        final FrameLayout admob_native_container;
        final LinearLayout native_card;
        final TextView text_ads_banner;
        final CardView main_card;

        public AdViewHolder(@NonNull View itemView) {
            super(itemView);
            admob_native_container = itemView.findViewById(R.id.admob_native_container);
            native_card = itemView.findViewById(R.id.native_card);
            text_ads_banner = itemView.findViewById(R.id.text_ads_banner);
            main_card = itemView.findViewById(R.id.main_card);
            if (isNetworkConnected()) {
                RefreshAd();
            } else {
                native_card.setVisibility(View.GONE);
                text_ads_banner.setVisibility(View.GONE);
            }
        }

        private void RefreshAd() {

            AdLoader.Builder builder = new AdLoader.Builder(acontext, MyApplication.get_Admob_native_Id())
                    .forNativeAd(nativeAd -> {
                        @SuppressLint("InflateParams") NativeAdView adView = (NativeAdView) LayoutInflater.from(acontext)
                                .inflate(R.layout.unifiednativead, null);
                        populateUnifiedNativeAdView(nativeAd, adView);
                        admob_native_container.removeAllViews();
                        admob_native_container.addView(adView);
                        native_card.setVisibility(View.VISIBLE);
                        text_ads_banner.setVisibility(View.GONE);
                    });
            AdLoader adLoader = builder.withAdListener(new AdListener() {
                @Override
                public void onAdFailedToLoad(@NonNull @NotNull LoadAdError loadAdError) {
                    super.onAdFailedToLoad(loadAdError);
                    native_card.setVisibility(View.GONE);
                    text_ads_banner.setVisibility(View.GONE);
                }
            }).build();

            adLoader.loadAd(new AdRequest.Builder().build());


        }

        void populateUnifiedNativeAdView(NativeAd nativeAd, NativeAdView adView) {
            com.google.android.gms.ads.nativead.MediaView mediaView = adView.findViewById(R.id.ad_media);
            adView.setMediaView(mediaView);
            adView.setHeadlineView(adView.findViewById(R.id.ad_headline));
            adView.setBodyView(adView.findViewById(R.id.ad_body));
            adView.setCallToActionView(adView.findViewById(R.id.ad_call_to_action));
            adView.setIconView(adView.findViewById(R.id.ad_app_icon));
            ((TextView) Objects.requireNonNull(adView.getHeadlineView())).setText(nativeAd.getHeadline());
            if (nativeAd.getBody() == null) {
                Objects.requireNonNull(adView.getBodyView()).setVisibility(View.INVISIBLE);
            } else {
                Objects.requireNonNull(adView.getBodyView()).setVisibility(View.VISIBLE);
                ((TextView) adView.getBodyView()).setText(nativeAd.getBody());
            }
            if (nativeAd.getCallToAction() == null) {
                Objects.requireNonNull(adView.getCallToActionView()).setVisibility(View.INVISIBLE);
            } else {
                Objects.requireNonNull(adView.getCallToActionView()).setVisibility(View.VISIBLE);
                ((TextView) adView.getCallToActionView()).setText(nativeAd.getCallToAction());
            }
            if (nativeAd.getIcon() == null) {
                Objects.requireNonNull(adView.getIconView()).setVisibility(View.GONE);
            } else {
                ((ImageView) Objects.requireNonNull(adView.getIconView())).setImageDrawable(
                        nativeAd.getIcon().getDrawable());
                adView.getIconView().setVisibility(View.VISIBLE);
            }
            adView.setNativeAd(nativeAd);
        }
    }

    @Override
    public int getItemCount() {
        return winnerListModelArrayList.size();
    }


    public static class MyViewHolder extends RecyclerView.ViewHolder {

        public final ImageView WinnerTeamPic;
        public final TextView winnerID;
        public final TextView yearID;
        public final TextView manofthematchID;
        public final TextView orangecapID;
        public final TextView playerofthematchID;
        public final TextView purplecapID;
        public final TextView runnerUpID;
        public final CardView winnerlist_cardView;
        public MyViewHolder(View view) {
            super(view);
            this.WinnerTeamPic = view.findViewById(R.id.WinnerTeamPic);
            this.yearID = view.findViewById(R.id.yearID);
            this.winnerID = view.findViewById(R.id.winnerID);
            this.runnerUpID = view.findViewById(R.id.runnerUpID);
            this.orangecapID = view.findViewById(R.id.orangecapID);
            this.purplecapID = view.findViewById(R.id.purplecapID);
            this.manofthematchID = view.findViewById(R.id.manofthematchID);
            this.playerofthematchID = view.findViewById(R.id.playerofthematchID);
            winnerlist_cardView = view.findViewById(R.id.winnerlist_cardView);
        }
    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) acontext.getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnected();
    }
}