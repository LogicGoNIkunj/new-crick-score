package com.ltos.livecrkttv.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.ltos.livecrkttv.R;
import com.ltos.livecrkttv.model.CommentaryModel;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;


public class OverballsAdapter extends RecyclerView.Adapter<OverballsAdapter.MyViewHolder> {
    final ArrayList<CommentaryModel.Innings.Overs.Balls> ballsArrayList;
    ArrayList<CommentaryModel.Innings.Overs.Balls.Comments> commentsArrayList;
    final int moverNumber;

    public OverballsAdapter(Context context, ArrayList<CommentaryModel.Innings.Overs.Balls> arrayList, int overNumber) {
        ballsArrayList = arrayList;
        moverNumber = overNumber;
    }

    @NotNull
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_over_balls, parent, false);
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NotNull MyViewHolder holder, int position) {
        try {
            commentsArrayList = ballsArrayList.get(position).getComments();
            String commentstype = commentsArrayList.get((commentsArrayList.size() - 1)).getCommentTypeId();
            if ("Four".equals(commentstype)) {
                holder.tv_balls.setText("4");
                holder.tv_balls.setBackgroundResource(R.drawable.circle_blue);
            } else if ("Six".equals(commentstype)) {
                holder.tv_balls.setText("6");
                holder.tv_balls.setBackgroundResource(R.drawable.circle_purple);
            } else if ("Wicket".equals(commentstype) || "Catch".equals(commentstype)) {
                holder.tv_balls.setText("W");
                holder.tv_balls.setBackgroundResource(R.drawable.circle_red);
            } else if ("DotBall".equals(commentstype) || "Normal".equals(commentstype)) {
                holder.tv_balls.setText("0");
                holder.tv_balls.setBackgroundResource(R.drawable.circle_gray);
            } else if ("One".equals(commentstype)) {
                holder.tv_balls.setText("1");
                holder.tv_balls.setBackgroundResource(R.drawable.circle_green);
            } else if ("Two".equals(commentstype)) {
                holder.tv_balls.setText("2");
                holder.tv_balls.setBackgroundResource(R.drawable.circle_green);
            } else if ("Three".equals(commentstype)) {
                holder.tv_balls.setText("3");
                holder.tv_balls.setBackgroundResource(R.drawable.circle_green);
            } else if ("Wide".equals(commentstype)) {
                holder.tv_balls.setText(String.format("Wd%s", ballsArrayList.get(position).getExtras()));
                holder.tv_balls.setBackgroundResource(R.drawable.circle_yellow);
            } else if ("NoBall".equals(commentstype)) {
                holder.tv_balls.setText(String.format("Nb%s", ballsArrayList.get(position).getExtras()));
                holder.tv_balls.setBackgroundResource(R.drawable.circle_yellow);
            } else if ("LegBye".equals(commentstype)) {
                holder.tv_balls.setText(String.format("L%s", ballsArrayList.get(position).getExtras()));
                holder.tv_balls.setBackgroundResource(R.drawable.circle_yellow);
            } else if ("Bye".equals(commentstype)) {
                holder.tv_balls.setText(String.format("B%s", ballsArrayList.get(position).getExtras()));
                holder.tv_balls.setBackgroundResource(R.drawable.circle_gray);
            }else if ("RunOut".equals(commentstype)){
                holder.tv_balls.setText(String.format("W%s", ballsArrayList.get(position).getRuns()));
                holder.tv_balls.setBackgroundResource(R.drawable.circle_red);
            }
            holder.itemView.setOnClickListener(view -> {

            });
        } catch (Exception exception) {
            exception.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return ballsArrayList.size();
    }


    public static class MyViewHolder extends RecyclerView.ViewHolder {
        public final TextView tv_balls;


        public MyViewHolder(View v) {
            super(v);
            tv_balls = v.findViewById(R.id.tv_balls);
        }
    }

}