package com.ltos.livecrkttv.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ltos.livecrkttv.R;
import com.ltos.livecrkttv.model.Scorecard;

import org.jetbrains.annotations.NotNull;
import org.json.JSONObject;

import java.util.ArrayList;


public class ScorecardAdapter extends RecyclerView.Adapter<ScorecardAdapter.MyViewHolder> {
    private final Context acontext;
    static ArrayList<Scorecard.Fixture.Innings> inningsArrayList = new ArrayList<>();
    final String matchdata;
    JSONObject homejsonObject;
    JSONObject awayjsonObject;

    public ScorecardAdapter(Context context, ArrayList<Scorecard.Fixture.Innings> arrayList, String smatchdata) {
        acontext = context;
        inningsArrayList = arrayList;
        matchdata = smatchdata;
    }

    @NotNull
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_scoreboard, parent, false);
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NotNull MyViewHolder holder, int position) {
        try {
            Scorecard.Fixture.Innings innings = inningsArrayList.get(position);

            JSONObject jsonObject = new JSONObject(matchdata).getJSONObject("fixture");
            homejsonObject = jsonObject.getJSONObject("homeTeam");
            awayjsonObject = jsonObject.getJSONObject("awayTeam");

            int battingId1 = innings.getBattingTeamId();

            try {
                if (battingId1 == homejsonObject.getInt("id")) {
                    holder.tv_country.setText((homejsonObject.getString("name")).replace("Men", ""));
                } else {
                    holder.tv_country.setText((awayjsonObject.getString("name")).replace("Men", ""));
                }

            } catch (Exception exception) {
                exception.printStackTrace();
            }

            holder.tv_score.setText(String.format("%s-%s (%s)", innings.getRunsScored(), innings.getNumberOfWicketsFallen(), innings.getOversBowled()));

            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(acontext);
            LinearLayoutManager linearLayoutManager1 = new LinearLayoutManager(acontext);
            LinearLayoutManager linearLayoutManager2 = new LinearLayoutManager(acontext);
            holder.rv_players_scores.setLayoutManager(linearLayoutManager);
            holder.rv_bowlers.setLayoutManager(linearLayoutManager1);
            holder.rv_fall_wicket.setLayoutManager(linearLayoutManager2);

            BatsmanScoreAdapter matchAdapter = new BatsmanScoreAdapter(acontext, innings.getBatsmen(), matchdata);
            holder.rv_players_scores.setAdapter(matchAdapter);

            BowlerAdapter bowlerAdapter = new BowlerAdapter(acontext, innings.getBowlers(), matchdata);
            holder.rv_bowlers.setAdapter(bowlerAdapter);

            FallWickersAdapter fallWickersAdapter = new FallWickersAdapter(acontext, innings.getWickets(), matchdata);
            holder.rv_fall_wicket.setAdapter(fallWickersAdapter);

            holder.tv_total_extras.setText(String.format("%s", innings.getTotalExtras()));
            holder.tv_extras.setText(String.format("b %s, lb %s, w %s, nb %s, p %s", innings.getByesRuns(), innings.getLegByesRuns(), innings.getWideBalls(), innings.getNoBalls(), innings.getPenalties()));

            holder.tv_total_score.setText(String.format("%s-%s", innings.getRunsScored(), innings.getNumberOfWicketsFallen()));

            try {
                float overbowled = Float.parseFloat(innings.getOversBowled());
                double totalbowl = (int) (overbowled * 6);
                double crr = (innings.getRunsScored() * 6) / totalbowl;
                holder.tv_crr.setText(String.format("CRR %s", crr));
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }


        } catch (Exception exception) {
            exception.printStackTrace();
        }
        holder.rl_country_score.setOnClickListener(view -> {
            if (holder.country_cl.getVisibility() == View.VISIBLE) {
                holder.iv_updown.setImageResource(R.drawable.ic_up_collapse);
                holder.country_cl.setVisibility(View.GONE);
            } else {
                holder.iv_updown.setImageResource(R.drawable.ic_down_extend);
                holder.country_cl.setVisibility(View.VISIBLE);
            }

        });
    }

    @Override
    public int getItemCount() {
        return inningsArrayList.size();
    }


    public static class MyViewHolder extends RecyclerView.ViewHolder {

        public final TextView tv_country;
        public final TextView tv_score;
        public final TextView tv_total_extras;
        public final TextView tv_extras;
        public final TextView tv_total_score;
        public final TextView tv_crr;
        public final ImageView iv_updown;
        public final RecyclerView rv_players_scores;
        public final RecyclerView rv_bowlers;
        public final RecyclerView rv_fall_wicket;
        public final RelativeLayout rl_country_score;
        public final ConstraintLayout country_cl;


        public MyViewHolder(View v) {
            super(v);
            country_cl = v.findViewById(R.id.country_cl);
            rl_country_score = v.findViewById(R.id.rl_country_score);
            tv_country = v.findViewById(R.id.tv_country);
            tv_score = v.findViewById(R.id.tv_score);
            tv_total_extras = v.findViewById(R.id.tv_total_extras);
            tv_extras = v.findViewById(R.id.tv_extras);
            tv_total_score = v.findViewById(R.id.tv_total_score);
            tv_crr = v.findViewById(R.id.tv_crr);
            iv_updown = v.findViewById(R.id.iv_updown);
            rv_players_scores = v.findViewById(R.id.rv_players_scores);
            rv_bowlers = v.findViewById(R.id.rv_bowlers);
            rv_fall_wicket = v.findViewById(R.id.rv_fall_wicket);
        }
    }

}