package com.ltos.livecrkttv.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.ltos.livecrkttv.R;
import com.ltos.livecrkttv.activity.PlayerInfoActivity;
import com.ltos.livecrkttv.model.Scorecard;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;



public class FallWickersAdapter extends RecyclerView.Adapter<FallWickersAdapter.MyViewHolder> {
    private final Context acontext;
    final ArrayList<Scorecard.Fixture.Innings.Wickets> batsmanArrayList;
    final String mmatchdata;

    public FallWickersAdapter(Context context, ArrayList<Scorecard.Fixture.Innings.Wickets> arrayList, String matchdata) {
        acontext = context;
        batsmanArrayList = arrayList;
        mmatchdata = matchdata;
    }

    @NotNull
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_fallofwicket, parent, false);
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NotNull MyViewHolder holder, int position) {
        try {
            Scorecard.Fixture.Innings.Wickets wickets = batsmanArrayList.get(position);
            holder.tv_batsman.setText(String.format("%s", wickets.playerId));
            holder.tv_score.setText(String.format("%s-%s", wickets.runs, wickets.order));
            holder.tv_overs.setText(String.format("%s", wickets.overBallDisplay));

            JSONArray jsonArray = new JSONObject(mmatchdata).getJSONArray("players");
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                int playerid = jsonObject.getInt("id");
                if (playerid == wickets.playerId) {
                    holder.tv_batsman.setText(jsonObject.getString("displayName"));
                }
            }

            holder.itemView.setOnClickListener(view -> {
                Intent intent = new Intent(acontext, PlayerInfoActivity.class);
                intent.putExtra("MATCHDATA", mmatchdata);
                intent.putExtra("PLAYER_ID", wickets.playerId);
                intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                acontext.startActivity(intent);
            });
        } catch (Exception exception) {
            exception.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return batsmanArrayList.size();
    }


    public static class MyViewHolder extends RecyclerView.ViewHolder {

        public final TextView tv_batsman;
        public final TextView tv_score;
        public final TextView tv_overs;


        public MyViewHolder(View v) {
            super(v);
            tv_batsman = v.findViewById(R.id.tv_batsman);
            tv_score = v.findViewById(R.id.tv_score);
            tv_overs = v.findViewById(R.id.tv_overs);
        }
    }

}