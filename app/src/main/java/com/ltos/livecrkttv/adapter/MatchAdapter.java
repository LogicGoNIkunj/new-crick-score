package com.ltos.livecrkttv.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.ltos.livecrkttv.R;
import com.ltos.livecrkttv.model.MatchesModel;

import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.TimeZone;




public class MatchAdapter extends RecyclerView.Adapter<MatchAdapter.MyViewHolder> {
    private final Context acontext;
    final ArrayList<MatchesModel.Datas> matchesModelArrayList;
    final String amatchdata;
    final int type;

    public MatchAdapter(Context context, ArrayList<MatchesModel.Datas> arrayList, String matchdata, int i) {
        acontext = context;
        matchesModelArrayList = arrayList;
        amatchdata = matchdata;
        type = i;
    }

    @NotNull
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_matches, parent, false));
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NotNull MyViewHolder holder, int position) {
        try {
            MatchesModel.Datas datas = matchesModelArrayList.get(position);
            holder.match_vanue_type.setText(datas.getGameType());
            holder.match_vanue.setText(datas.getVenue().getName());
            holder.country1_name.setText(datas.getHomeTeam().getShortName());

            Glide.with(acontext)
                    .load(datas.getHomeTeam().getLogoUrl())
                    .into(holder.country1_flag);

            Glide.with(acontext)
                    .load(datas.getAwayTeam().getLogoUrl())
                    .into(holder.country2_flag);

            if (datas.getInnings().size() == 1) {
                if (datas.getInnings().get(0).getBattingTeamId() == datas.getHomeTeam().getId()) {
                    holder.country1_score1.setText(String.format("%s-%s", datas.getInnings().get(0).getRunsScored(), datas.getInnings().get(0).getNumberOfWicketsFallen()));
                } else if (datas.getInnings().get(0).getBattingTeamId() == datas.getAwayTeam().getId()) {
                    holder.country2_score1.setText(String.format("%s-%s", datas.getInnings().get(0).getRunsScored(), datas.getInnings().get(0).getNumberOfWicketsFallen()));
                }
            } else if (datas.getInnings().size() == 2) {
                if (datas.getInnings().get(0).getBattingTeamId() == datas.getHomeTeam().getId()) {
                    holder.country1_score1.setText(String.format("%s-%s", datas.getInnings().get(0).getRunsScored(), datas.getInnings().get(0).getNumberOfWicketsFallen()));
                } else if (datas.getInnings().get(0).getBattingTeamId() == datas.getAwayTeam().getId()) {
                    holder.country2_score1.setText(String.format("%s-%s", datas.getInnings().get(0).getRunsScored(), datas.getInnings().get(0).getNumberOfWicketsFallen()));
                }
                if (datas.getInnings().get(1).getBattingTeamId() == datas.getHomeTeam().getId()) {
                    holder.country1_score1.setText(String.format("%s-%s", datas.getInnings().get(1).getRunsScored(), datas.getInnings().get(1).getNumberOfWicketsFallen()));
                } else if (datas.getInnings().get(1).getBattingTeamId() == datas.getAwayTeam().getId()) {
                    holder.country2_score1.setText(String.format("%s-%s", datas.getInnings().get(1).getRunsScored(), datas.getInnings().get(1).getNumberOfWicketsFallen()));
                }
            } else if (datas.getInnings().size() == 3) {
                if (datas.getInnings().get(0).getBattingTeamId() == datas.getHomeTeam().getId()) {
                    holder.country1_score1.setText(String.format("%s-%s", datas.getInnings().get(0).getRunsScored(), datas.getInnings().get(0).getNumberOfWicketsFallen()));
                } else if (datas.getInnings().get(0).getBattingTeamId() == datas.getAwayTeam().getId()) {
                    holder.country2_score1.setText(String.format("%s-%s", datas.getInnings().get(0).getRunsScored(), datas.getInnings().get(0).getNumberOfWicketsFallen()));
                }
                if (datas.getInnings().get(1).getBattingTeamId() == datas.getHomeTeam().getId()) {
                    holder.country1_score1.setText(String.format("%s-%s", datas.getInnings().get(1).getRunsScored(), datas.getInnings().get(1).getNumberOfWicketsFallen()));
                } else if (datas.getInnings().get(1).getBattingTeamId() == datas.getAwayTeam().getId()) {
                    holder.country2_score1.setText(String.format("%s-%s", datas.getInnings().get(1).getRunsScored(), datas.getInnings().get(1).getNumberOfWicketsFallen()));
                }
                if (datas.getInnings().get(2).getBattingTeamId() == datas.getHomeTeam().getId()) {
                    holder.country1_score2.setVisibility(View.VISIBLE);
                    holder.country1_score2.setText(String.format("%s-%s", datas.getInnings().get(2).getRunsScored(), datas.getInnings().get(2).getNumberOfWicketsFallen()));
                } else if (datas.getInnings().get(2).getBattingTeamId() == datas.getAwayTeam().getId()) {
                    holder.country2_score2.setVisibility(View.VISIBLE);
                    holder.country2_score2.setText(String.format("%s-%s", datas.getInnings().get(2).getRunsScored(), datas.getInnings().get(2).getNumberOfWicketsFallen()));
                }
            } else if (datas.getInnings().size() == 4) {
                if (datas.getInnings().get(0).getBattingTeamId() == datas.getHomeTeam().getId()) {
                    holder.country1_score1.setText(String.format("%s-%s", datas.getInnings().get(0).getRunsScored(), datas.getInnings().get(0).getNumberOfWicketsFallen()));
                } else if (datas.getInnings().get(0).getBattingTeamId() == datas.getAwayTeam().getId()) {
                    holder.country2_score1.setText(String.format("%s-%s", datas.getInnings().get(0).getRunsScored(), datas.getInnings().get(0).getNumberOfWicketsFallen()));
                }
                if (datas.getInnings().get(1).getBattingTeamId() == datas.getHomeTeam().getId()) {
                    holder.country1_score1.setText(String.format("%s-%s", datas.getInnings().get(1).getRunsScored(), datas.getInnings().get(1).getNumberOfWicketsFallen()));
                } else if (datas.getInnings().get(1).getBattingTeamId() == datas.getAwayTeam().getId()) {
                    holder.country2_score1.setText(String.format("%s-%s", datas.getInnings().get(1).getRunsScored(), datas.getInnings().get(1).getNumberOfWicketsFallen()));
                }
                if (datas.getInnings().get(2).getBattingTeamId() == datas.getHomeTeam().getId()) {
                    holder.country1_score2.setVisibility(View.VISIBLE);
                    holder.country1_score2.setText(String.format("%s-%s", datas.getInnings().get(2).getRunsScored(), datas.getInnings().get(2).getNumberOfWicketsFallen()));
                } else if (datas.getInnings().get(2).getBattingTeamId() == datas.getAwayTeam().getId()) {
                    holder.country2_score2.setVisibility(View.VISIBLE);
                    holder.country2_score2.setText(String.format("%s-%s", datas.getInnings().get(2).getRunsScored(), datas.getInnings().get(2).getNumberOfWicketsFallen()));
                }
                if (datas.getInnings().get(3).getBattingTeamId() == datas.getHomeTeam().getId()) {
                    holder.country1_score2.setVisibility(View.VISIBLE);
                    holder.country1_score2.setText(String.format("%s-%s", datas.getInnings().get(3).getRunsScored(), datas.getInnings().get(3).getNumberOfWicketsFallen()));
                } else if (datas.getInnings().get(3).getBattingTeamId() == datas.getAwayTeam().getId()) {
                    holder.country2_score2.setVisibility(View.VISIBLE);
                    holder.country2_score2.setText(String.format("%s-%s", datas.getInnings().get(3).getRunsScored(), datas.getInnings().get(3).getNumberOfWicketsFallen()));
                }
            }
            try {
                if (datas.getGameStatus() != null) {
                    if (!datas.getGameStatus().equals("")) {
                        holder.gamestatus.setText(datas.getGameStatus());
                        holder.rr_live.setVisibility(View.VISIBLE);
                        holder.llupcoming.setVisibility(View.GONE);

                        if (datas.getGameStatus().contains("In Play")) {
                            holder.gamestatus.setText("Live");
                            holder.rr_live.setVisibility(View.VISIBLE);
                            holder.islive.setVisibility(View.VISIBLE);
                            holder.llupcoming.setVisibility(View.GONE);
                        } else if (datas.getGameStatus().contains("Prematch")) {
                            holder.rr_live.setVisibility(View.GONE);
                            holder.llupcoming.setVisibility(View.VISIBLE);

                            @SuppressLint("SimpleDateFormat") SimpleDateFormat parser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
                            parser.setTimeZone(TimeZone.getTimeZone("UTC"));
                            Date parsed = parser.parse(datas.getStartDateTime());

                            String removeGMT = String.valueOf(parsed).replace("GMT+05:30 ", "");
                            String removeYear = StringUtils.substringBeforeLast(removeGMT, " ");

                            holder.time_cont.setText(StringUtils.substringBeforeLast(removeYear, " "));
                            holder.time.setText(Convert24to12(StringUtils.substringAfterLast(removeYear, " ")));
                        } else {
                            holder.islive.setVisibility(View.GONE);
                        }
                    } else {
                        holder.rr_live.setVisibility(View.GONE);
                        holder.llupcoming.setVisibility(View.GONE);
                        holder.gamestatus.setVisibility(View.GONE);
                    }
                } else {
                    holder.rr_live.setVisibility(View.GONE);
                    holder.llupcoming.setVisibility(View.VISIBLE);

                    @SuppressLint("SimpleDateFormat") SimpleDateFormat parser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
                    parser.setTimeZone(TimeZone.getTimeZone("UTC"));
                    Date parsed = parser.parse(datas.getCompetition().getStartDateTime());

                    String removeGMT = String.valueOf(parsed).replace("GMT+05:30 ", "");
                    String removeYear = StringUtils.substringBeforeLast(removeGMT, " ");

                    holder.time_cont.setText(StringUtils.substringBeforeLast(removeYear, " "));
                    holder.time.setText(Convert24to12(StringUtils.substringAfterLast(removeYear, " ")));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            holder.country2_name.setText(datas.getAwayTeam().getShortName());
            holder.match_result.setText(datas.getResultText());
            holder.series_name.setText(String.format("%s %s", datas.getCompetition().getName(), datas.getName()));
        } catch (Exception exception) {
            exception.printStackTrace();
        }

    }

    public static String Convert24to12(String time) {
        String convertedTime = "";
        try {
            @SuppressLint("SimpleDateFormat") SimpleDateFormat displayFormat = new SimpleDateFormat("hh:mm a");
            @SuppressLint("SimpleDateFormat") SimpleDateFormat parseFormat = new SimpleDateFormat("HH:mm:ss");
            Date date = parseFormat.parse(time);
            if (date != null) {
                convertedTime = displayFormat.format(date);
            }
            System.out.println("convertedTime : " + convertedTime);
        } catch (final ParseException e) {
            e.printStackTrace();
        }
        return convertedTime;
        //Output will be 10:23 PM
    }

    @Override
    public int getItemCount() {
        return matchesModelArrayList.size();
    }

    @Override
    public long getItemId(int position) {
        return matchesModelArrayList == null ? 0 : matchesModelArrayList.get(position).getId();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        public final TextView match_vanue_type;
        public final TextView series_name;
        public final TextView match_vanue;
        public final TextView gamestatus;
        public final TextView time_cont;
        public final TextView time;
        public final TextView country1_name;
        public final TextView country1_score1;
        public final TextView country1_score2;
        public final TextView country2_name;
        public final TextView country2_score1;
        public final TextView country2_score2;
        public final TextView match_result;
        public final ImageView country1_flag;
        public final ImageView country2_flag;
        public final ImageView islive;
        final RelativeLayout rr_live;
        final LinearLayout llupcoming;


        public MyViewHolder(View v) {
            super(v);
            match_vanue_type = v.findViewById(R.id.match_vanue_type);
            series_name = v.findViewById(R.id.series_name);
            match_vanue = v.findViewById(R.id.match_vanue);
            gamestatus = v.findViewById(R.id.gamestatus);
            time_cont = v.findViewById(R.id.time_cont);
            time = v.findViewById(R.id.time);
            country1_name = v.findViewById(R.id.country1_name);
            country1_score1 = v.findViewById(R.id.country1_score1);
            country1_score2 = v.findViewById(R.id.country1_score2);
            country2_name = v.findViewById(R.id.country2_name);
            country2_score1 = v.findViewById(R.id.country2_score1);
            country2_score2 = v.findViewById(R.id.country2_score2);
            match_result = v.findViewById(R.id.match_result);
            country1_flag = v.findViewById(R.id.country1_flag);
            country2_flag = v.findViewById(R.id.country2_flag);
            islive = v.findViewById(R.id.islive);
            rr_live = v.findViewById(R.id.rr_live);
            llupcoming = v.findViewById(R.id.llupcoming);
        }
    }
}