package com.ltos.livecrkttv.interfaces;


import com.ltos.livecrkttv.model.CommentaryModel;
import com.ltos.livecrkttv.model.MatchesModel;
import com.ltos.livecrkttv.model.Playdata;
import com.ltos.livecrkttv.model.Scorecard;
import com.ltos.livecrkttv.model.UrlModel;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Query;

public interface APIService {
    @GET("views/fixtures")
    Call<MatchesModel> getCategories(@Query("CompletedFixturesCount") int complete,
                                     @Query("InProgressFixturesCount") int inprogress,
                                     @Query("UpcomingFixturesCount") int upcoming);

    @GET("views/comments")
    Call<CommentaryModel> getComments(@Query("FixtureId") int fixtureid,
                                      @Query("jsconfig") String jsonconfig,
                                      @Query("OverLimit") int overlimit,
                                      @Query("lastOverNumber") String lastovernumber,
                                      @Query("IncludeVideoReplays") boolean reply,
                                      @Query("InningNumber") int inningnumber,
                                      @Query("CommentType") String commentType);

    @GET("views/scorecard")
    Call<Scorecard> getScorecard(@Query("FixtureId") int fixtureid,
                                 @Query("jsconfig") String jsonconfig);

    @GET("qureka-ad")
    Call<Playdata> GetData();

    @GET("cric-api")
        Call<UrlModel> GetCricApi(@Header("AuthorizationKey") String Auth);

}
