package com.ltos.livecrkttv.activity;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.browser.customtabs.CustomTabsIntent;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.ltos.livecrkttv.R;
import com.ltos.livecrkttv.interfaces.APIService;
import com.ltos.livecrkttv.model.Playdata;
import com.ltos.livecrkttv.utils.CustomTabActivityHelper;
import com.ltos.livecrkttv.utils.MyApplication;
import com.ltos.livecrkttv.utils.NetworkCheck;
import com.ltos.livecrkttv.utils.WebviewFallback;
import com.google.android.gms.ads.AdError;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.FullScreenContentCallback;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.interstitial.InterstitialAd;
import com.google.android.gms.ads.interstitial.InterstitialAdLoadCallback;

import org.jetbrains.annotations.NotNull;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class TeamListActivity extends AppCompatActivity {

    InterstitialAd mInterstitialAd;
    boolean isActivityLeft;
    private Retrofit retrofit;
    String qurekaLink = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_team_list);
        getWindow().setBackgroundDrawable(null);
        isActivityLeft = false;
        LoadAdInterstitial();
        GetData();
        click();
    }


    public void click() {
        findViewById(R.id.back_btn).setOnClickListener(view -> onBackPressed());
        findViewById(R.id.qureka).setOnClickListener(view -> {
            MyApplication.appOpenManager.isAdShow = true;
            if (!qurekaLink.equals("")) {
                try {
                    CustomTabsIntent customTabsIntent = new CustomTabsIntent.Builder().build();
                    CustomTabActivityHelper.openCustomTab(TeamListActivity.this, customTabsIntent, Uri.parse(qurekaLink), new WebviewFallback());
                } catch (Exception e) {
                    CustomTabsIntent.Builder builder = new CustomTabsIntent.Builder();
                    CustomTabsIntent customTabsIntent = builder.build();
                    customTabsIntent.launchUrl(TeamListActivity.this, Uri.parse(qurekaLink));
                }
            }
        });
        findViewById(R.id.card_cks).setOnClickListener(view -> {

            Intent intent = new Intent(TeamListActivity.this, SquadsListActivity.class);
            intent.putExtra("team", "CSK");
            startActivity(intent);

            try {
                if (mInterstitialAd != null && !isActivityLeft) {
                    mInterstitialAd.show(TeamListActivity.this);
                    MyApplication.appOpenManager.isAdShow = true;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        });
        findViewById(R.id.card_mi).setOnClickListener(view -> {

            Intent intent = new Intent(TeamListActivity.this, SquadsListActivity.class);
            intent.putExtra("team", "MI");
            startActivity(intent);
            try {
                if (mInterstitialAd != null && !isActivityLeft) {
                    mInterstitialAd.show(TeamListActivity.this);
                    MyApplication.appOpenManager.isAdShow = true;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        findViewById(R.id.card_kkr).setOnClickListener(view -> {
            Intent intent = new Intent(TeamListActivity.this, SquadsListActivity.class);
            intent.putExtra("team", "KKR");
            startActivity(intent);
            try {
                if (mInterstitialAd != null && !isActivityLeft) {
                    mInterstitialAd.show(TeamListActivity.this);
                    MyApplication.appOpenManager.isAdShow = true;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        findViewById(R.id.card_srh).setOnClickListener(view -> {
            Intent intent = new Intent(TeamListActivity.this, SquadsListActivity.class);
            intent.putExtra("team", "SRH");
            startActivity(intent);
            try {
                if (mInterstitialAd != null && !isActivityLeft) {
                    mInterstitialAd.show(TeamListActivity.this);
                    MyApplication.appOpenManager.isAdShow = true;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        findViewById(R.id.card_rcb).setOnClickListener(view -> {
            Intent intent = new Intent(TeamListActivity.this, SquadsListActivity.class);
            intent.putExtra("team", "RCB");
            startActivity(intent);
            try {
                if (mInterstitialAd != null && !isActivityLeft) {
                    mInterstitialAd.show(TeamListActivity.this);
                    MyApplication.appOpenManager.isAdShow = true;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        findViewById(R.id.card_dc).setOnClickListener(view -> {
            Intent intent = new Intent(TeamListActivity.this, SquadsListActivity.class);
            intent.putExtra("team", "DC");
            startActivity(intent);
            try {
                if (mInterstitialAd != null && !isActivityLeft) {
                    mInterstitialAd.show(TeamListActivity.this);
                    MyApplication.appOpenManager.isAdShow = true;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        findViewById(R.id.card_pbks).setOnClickListener(view -> {
            Intent intent = new Intent(TeamListActivity.this, SquadsListActivity.class);
            intent.putExtra("team", "PBKS");
            startActivity(intent);
            try {
                if (mInterstitialAd != null && !isActivityLeft) {
                    mInterstitialAd.show(TeamListActivity.this);
                    MyApplication.appOpenManager.isAdShow = true;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        findViewById(R.id.card_rr).setOnClickListener(view -> {
            Intent intent = new Intent(TeamListActivity.this, SquadsListActivity.class);
            intent.putExtra("team", "RR");
            startActivity(intent);
            try {
                if (mInterstitialAd != null && !isActivityLeft) {
                    mInterstitialAd.show(TeamListActivity.this);
                    MyApplication.appOpenManager.isAdShow = true;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        });
        findViewById(R.id.card_gt).setOnClickListener(view -> {
            Intent intent = new Intent(TeamListActivity.this, SquadsListActivity.class);
            intent.putExtra("team", "GT");
            startActivity(intent);
            try {
                if (mInterstitialAd != null && !isActivityLeft) {
                    mInterstitialAd.show(TeamListActivity.this);
                    MyApplication.appOpenManager.isAdShow = true;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        findViewById(R.id.card_lsb).setOnClickListener(view -> {
            Intent intent = new Intent(TeamListActivity.this, SquadsListActivity.class);
            intent.putExtra("team", "LSG");
            startActivity(intent);
            try {
                if (mInterstitialAd != null && !isActivityLeft) {
                    mInterstitialAd.show(TeamListActivity.this);
                    MyApplication.appOpenManager.isAdShow = true;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

    }


    @Override
    public void onBackPressed() {
        try {
            if (mInterstitialAd != null && !isActivityLeft) {
                mInterstitialAd.show(TeamListActivity.this);
                MyApplication.appOpenManager.isAdShow = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        finish();
    }

    public void onPause() {
        super.onPause();
        this.isActivityLeft = true;
    }

    public void onResume() {
        super.onResume();
        this.isActivityLeft = false;
    }

    protected void onStop() {
        super.onStop();
        this.isActivityLeft = true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        isActivityLeft = true;
    }

    public void LoadAdInterstitial() {
        AdRequest adRequest = new AdRequest.Builder().build();

        InterstitialAd.load(this, MyApplication.get_Admob_interstitial_Id(), adRequest, new InterstitialAdLoadCallback() {
            @Override
            public void onAdLoaded(@NonNull InterstitialAd interstitialAd) {
                mInterstitialAd = interstitialAd;
                mInterstitialAd.setFullScreenContentCallback(new FullScreenContentCallback() {
                    @Override
                    public void onAdDismissedFullScreenContent() {
                        MyApplication.appOpenManager.isAdShow = false;
                        mInterstitialAd = null;
                        LoadAdInterstitial();
                    }

                    @Override
                    public void onAdFailedToShowFullScreenContent(@NonNull AdError adError) {
                        MyApplication.appOpenManager.isAdShow = false;
                    }

                    @Override
                    public void onAdShowedFullScreenContent() {
                        mInterstitialAd = null;
                        MyApplication.appOpenManager.isAdShow = true;
                    }
                });
            }

            @Override
            public void onAdFailedToLoad(@NonNull LoadAdError loadAdError) {
                mInterstitialAd = null;
                MyApplication.appOpenManager.isAdShow = false;

            }
        });
    }

    private void GetData() {
        try {
            if (NetworkCheck.isNetworkAvailable(TeamListActivity.this)) {
                if (retrofit == null) {
                    retrofit = new Retrofit.Builder().baseUrl("https://smartadz.in/api/").client(new OkHttpClient.Builder().addInterceptor(chain -> chain.proceed(chain.request().newBuilder().build())).connectTimeout(100, TimeUnit.SECONDS).readTimeout(100, TimeUnit.SECONDS).build()).addConverterFactory(GsonConverterFactory.create()).build();
                }
                APIService nativeClient = retrofit.create(APIService.class);
                nativeClient.GetData().enqueue(new Callback<Playdata>() {
                    @Override
                    public void onResponse(@NotNull Call<Playdata> call, @NotNull retrofit2.Response<Playdata> response) {
                        try {
                            if (response.body() != null) {
                                if (response.body().isStatus()) {
                                    if (response.body().getData().isFlage()) {
                                        qurekaLink = response.body().getData().getUrl();
                                        findViewById(R.id.qurekaads).setVisibility(View.VISIBLE);
                                        Glide.with(TeamListActivity.this).load(response.body().getData().getImage()).listener(new RequestListener<Drawable>() {
                                            @Override
                                            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                                return false;
                                            }

                                            @Override
                                            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                                return false;
                                            }
                                        }).into((ImageView) findViewById(R.id.img));
                                    }
                                }
                            }

                        } catch (Exception exception) {
                            exception.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<Playdata> call, @NonNull Throwable t) {
                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}