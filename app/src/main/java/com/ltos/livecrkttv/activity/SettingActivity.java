package com.ltos.livecrkttv.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.Window;
import android.widget.RatingBar;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.ltos.livecrkttv.BuildConfig;
import com.ltos.livecrkttv.R;

public class SettingActivity extends AppCompatActivity {

    SharedPreferences sharedPreferences;


    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_setting);
        getWindow().setBackgroundDrawable(null);
        sharedPreferences = getSharedPreferences("rating", Context.MODE_PRIVATE);
        click();
    }


    public void click() {
        findViewById(R.id.back_btn).setOnClickListener(view -> SettingActivity.this.onBackPressed());


        findViewById(R.id.share_setting).setOnClickListener(view -> {
            Intent intent = new Intent();
            intent.setAction("android.intent.action.SEND");
            String sb = "" +
                    getString(R.string.app_name);
            intent.putExtra("android.intent.extra.SUBJECT", sb);
            String sb2 = "Its awesome,Check out this app! \n\n" +
                    getString(R.string.app_name) +
                    "\n\n Download it for free at \n\nhttps://play.google.com/store/apps/details?id=" +
                    getPackageName();
            intent.putExtra("android.intent.extra.TEXT", sb2);
            intent.setType("text/plain");
            startActivity(intent);
        });

        findViewById(R.id.feedback_setting).setOnClickListener(view -> {
            Intent intent = new Intent(SettingActivity.this, FeedbackActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            startActivity(intent);
        });

        findViewById(R.id.rate_setting).setOnClickListener(view -> {

            if (sharedPreferences.getBoolean("ratePref", true)) {
                rate();
            } else {
                Toast.makeText(SettingActivity.this, "You have already rate this application!!", Toast.LENGTH_SHORT).show();
            }

        });
        findViewById(R.id.privacypolicy_setting).setOnClickListener(view -> {
            try {
                Uri uri = Uri.parse("https://crickylivecricket.blogspot.com/2021/09/cricky-live-cricket-score.html"); // missing 'http://' will cause crashed
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
            } catch (Exception e) {
                Toast.makeText(this, "Something went wrong", Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }
        });
    }


    public void rate() {
        final Dialog dialog = new Dialog(SettingActivity.this);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.rateus_dialog);
        RatingBar ratingbar = dialog.findViewById(R.id.ratingbar);

        dialog.findViewById(R.id.txt_notnow).setOnClickListener(v -> dialog.dismiss());

        dialog.findViewById(R.id.txt_submit).setOnClickListener(v -> {
            dialog.dismiss();
            if (ratingbar.getRating() <= 3) {
                Intent intent = new Intent(SettingActivity.this, FeedbackActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(intent);
            } else {
                startActivity(new Intent(Intent.ACTION_VIEW).setData(Uri.parse("https://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID)));
                SharedPreferences.Editor editor1;
                editor1 = sharedPreferences.edit();
                editor1.putBoolean("ratePref", false);
                editor1.apply();
            }
        });
        dialog.show();
    }


}
