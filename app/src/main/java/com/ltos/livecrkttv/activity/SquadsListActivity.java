package com.ltos.livecrkttv.activity;

import android.annotation.SuppressLint;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.browser.customtabs.CustomTabsIntent;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.ltos.livecrkttv.R;
import com.ltos.livecrkttv.adapter.SquadsAdapter;
import com.ltos.livecrkttv.interfaces.APIService;
import com.ltos.livecrkttv.model.Playdata;
import com.ltos.livecrkttv.model.SquadsModel;
import com.ltos.livecrkttv.utils.CustomTabActivityHelper;
import com.ltos.livecrkttv.utils.MyApplication;
import com.ltos.livecrkttv.utils.NetworkCheck;
import com.ltos.livecrkttv.utils.WebviewFallback;
import com.google.android.gms.ads.AdError;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.FullScreenContentCallback;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.interstitial.InterstitialAd;
import com.google.android.gms.ads.interstitial.InterstitialAdLoadCallback;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class SquadsListActivity extends AppCompatActivity {

    final ArrayList<SquadsModel> squadsModelArrayList = new ArrayList<>();
    ImageView icon_team;
    TextView team_name;
    TextView owner_name;
    TextView coach_name;
    TextView home_venue;
    LinearLayout background_team;
    RecyclerView recyclerView;
    TextView textView;
    InterstitialAd mInterstitialAd;
    boolean isActivityLeft;
    private Retrofit retrofit;
    String qurekaLink = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_squads_list);
        getWindow().setBackgroundDrawable(null);
        isActivityLeft = false;
        LoadAdInterstitial();
        GetData();
        find_id();
    }



    public void find_id() {
        icon_team = findViewById(R.id.teamicon);
        team_name = findViewById(R.id.teamname);
        owner_name = findViewById(R.id.teamowner);
        coach_name = findViewById(R.id.teamcoach);
        home_venue = findViewById(R.id.teamvanue);
        textView = findViewById(R.id.title_toolbar);
        background_team = findViewById(R.id.bgid);
        recyclerView = findViewById(R.id.recyclerview);
        findViewById(R.id.back_btn).setOnClickListener(view -> onBackPressed());
        findViewById(R.id.qureka).setOnClickListener(view -> {
            MyApplication.appOpenManager.isAdShow = true;
            if (!qurekaLink.equals("")) {
                try {
                    CustomTabsIntent customTabsIntent = new CustomTabsIntent.Builder().build();
                    CustomTabActivityHelper.openCustomTab(SquadsListActivity.this, customTabsIntent, Uri.parse(qurekaLink), new WebviewFallback());
                } catch (Exception e) {
                    CustomTabsIntent.Builder builder = new CustomTabsIntent.Builder();
                    CustomTabsIntent customTabsIntent = builder.build();
                    customTabsIntent.launchUrl(SquadsListActivity.this, Uri.parse(qurekaLink));
                }
            }
        });
        String name_get = getIntent().getStringExtra("team");
        List_squads(name_get);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(new SquadsAdapter(this, squadsModelArrayList));
    }

    @SuppressLint({"SetTextI18n", "UseCompatLoadingForDrawables"})
    public void List_squads(String str) {
        switch (str) {
            case "CSK":
                textView.setText("CSK");
                this.background_team.setBackground(getResources().getDrawable(R.drawable.bg_csk));
                this.icon_team.setImageDrawable(getResources().getDrawable(R.drawable.cskicon));
                this.team_name.setText("Chennai Super Kings");
                this.owner_name.setText("Owner : Chennai Super Kings Cricket ltd.");
                this.coach_name.setText("Coach : Stephen Fleming");
                this.home_venue.setText("Home Venue : M. A. Chidambaram Stadium");
                squadsModelArrayList.add(new SquadsModel(R.drawable.csk1, "MS Dhoni", "Price : ₹12 Cr", R.drawable.captain, "Wicketkeeper batsman  ", "Right-handed", "Right-arm medium  ", "Indian", R.color.csk));
                squadsModelArrayList.add(new SquadsModel(R.drawable.csk16, "Ruturaj Gaikwad", "Price : ₹6 Cr", R.drawable.blankbg, "Batsman", "Right-handed", "Right-arm offbreak  ", "Indian", R.color.csk));
                squadsModelArrayList.add(new SquadsModel(R.drawable.csk15, "Robin Uthappa", "Price : ₹2 Cr", R.drawable.blankbg, "Wicketkeeper, Batsman", "Right-handed", "", "Indian", R.color.csk));
                squadsModelArrayList.add(new SquadsModel(R.drawable.empty_profile, "Devon Conway", "Price : ₹1 Cr", R.drawable.blankbg, "Wicketkeeper Batsman", "Left Handed Bat", "Right-arm medium", "New Zealand", R.color.csk));
                squadsModelArrayList.add(new SquadsModel(R.drawable.csk20, "Moeen Ali", "Price : ₹8 Cr", R.drawable.blankbg, "Batting Allrounder", "Left-handed Bat", "Right-arm offbreak  ", "Indian", R.color.csk));
                squadsModelArrayList.add(new SquadsModel(R.drawable.csk2, "Ambati Rayudu", "Price : ₹6.75 Cr", R.drawable.blankbg, "Wicketkeeper batsman  ", "Right-handed", "Right-arm off-spin  ", "Indian", R.color.csk));
                squadsModelArrayList.add(new SquadsModel(R.drawable.empty_profile, "Subhranshu Senapati", "Price : ₹20 L", R.drawable.blankbg, "Batsman", "Right Handed Bat", "Right-arm medium", "Indian", R.color.csk));
                squadsModelArrayList.add(new SquadsModel(R.drawable.csk8, "Narayan Jagadeesan", "Price : ₹20 L", R.drawable.blankbg, "Batsman", "Right Handed Bat", "Right-arm medium", "Indian", R.color.csk));
                squadsModelArrayList.add(new SquadsModel(R.drawable.csk14, "Ravindra Jadeja", "Price : ₹16 Cr`", R.drawable.blankbg, "All-rounder", "Left-handed", "Left-arm orthodox spin  ", "Indian", R.color.csk));
                squadsModelArrayList.add(new SquadsModel(R.drawable.empty_profile, "Shivam Dube", "Price : ₹4 Cr", R.drawable.blankbg, "Bowling Allrounder", "Left Handed Bat", "Right-arm medium", "Indian", R.color.csk));
                squadsModelArrayList.add(new SquadsModel(R.drawable.csk4, "Dwayne Bravo", "Price : ₹4.4 Cr", R.drawable.plane, "All-rounder", "Right-handed", "Right-arm medium fast  ", "West Indian", R.color.csk));
                squadsModelArrayList.add(new SquadsModel(R.drawable.empty_profile, "Rajvardhan Hangargekar", "Price : ₹1.5 Cr", R.drawable.blankbg, "Bowler", "Right Handed Bat", "Right-arm fast-medium", "Indian", R.color.csk));
                squadsModelArrayList.add(new SquadsModel(R.drawable.empty_profile, "Dwaine Pretorius", "Price : ₹50 L", R.drawable.blankbg, "Bowling Allrounder", "Right Handed Bat", "Right-arm fast-medium", "South Africa", R.color.csk));
                squadsModelArrayList.add(new SquadsModel(R.drawable.csk12, "Mitchell Santner", "Price : ₹1.9 Cr", R.drawable.plane, "Bowling all-rounder", " Left-handed", "Left-arm orthodox  ", "New Zealander", R.color.csk));
                squadsModelArrayList.add(new SquadsModel(R.drawable.empty_profile, "Adam Milne", "Price : ₹1.9 Cr", R.drawable.plane, "Bowler", "Right-handed", " Right-arm Fast  ", "new zealande", R.color.csk));
                squadsModelArrayList.add(new SquadsModel(R.drawable.empty_profile, "Chris Jordan", "Price : 3.6 Cr", R.drawable.plane, "Bowler", "Right-handed", "  Right-arm medium fast  ", "English", R.color.csk));
                squadsModelArrayList.add(new SquadsModel(R.drawable.csk5, "Deepak Chahar", "Price : ₹14 Cr", R.drawable.blankbg, "All-rounder", "Right-handed", "Right-arm medium  ", "Indian", R.color.csk));
                squadsModelArrayList.add(new SquadsModel(R.drawable.empty_profile, "Maheesh Theekshana", "Price : ₹70 L", R.drawable.blankbg, "Bowler", "Right-handed", "Right-arm offbreak ", " Sri Lankan ", R.color.csk));
                squadsModelArrayList.add(new SquadsModel(R.drawable.csk3, "KM Asif", "Price : ₹20 L", R.drawable.blankbg, "All-rounder", "Right-handed", "Right-arm off-spin  ", "Indian", R.color.csk));
                squadsModelArrayList.add(new SquadsModel(R.drawable.empty_profile, "Tushar Deshpande", "Price : ₹20 L", R.drawable.blankbg, "Bowler", "Left Handed Bat", "Right-arm medium", "Indian", R.color.csk));
                squadsModelArrayList.add(new SquadsModel(R.drawable.empty_profile, "Mukesh Choudhary", "Price : ₹20 L", R.drawable.blankbg, "All-rounder", "Left Handed Bat", "Left-arm fast-medium", "Indian", R.color.csk));
                squadsModelArrayList.add(new SquadsModel(R.drawable.empty_profile, "C.Hari Nishaanth", "Price : ₹20 L", R.drawable.blankbg, "Batsman", "Left Handed Bat", "Right-arm off break", "Indian", R.color.csk));
                squadsModelArrayList.add(new SquadsModel(R.drawable.csk25, "Bhagath Varma", "Price : ₹20 L", R.drawable.blankbg, "Batsman", "Right-handed", "Right-arm offbreak", "Indian", R.color.csk));
                squadsModelArrayList.add(new SquadsModel(R.drawable.csk25, "Simarjeet Singh", "Price : ₹20 L", R.drawable.blankbg, "Bowler", "Right Handed Bat", "Right-arm fast-medium", "Indian", R.color.csk));
                squadsModelArrayList.add(new SquadsModel(R.drawable.csk25, "Prashant Solanki", "Price : ₹1.2 Cr", R.drawable.blankbg, "Bowler", "Right Handed Bat", "Right-arm legbreak", "Indian", R.color.csk));
                break;
            case "MI":
                textView.setText("MI");
                this.background_team.setBackground(getResources().getDrawable(R.drawable.bg_mi));
                this.icon_team.setImageDrawable(getResources().getDrawable(R.drawable.micon));
                this.team_name.setText("Mumbai Indians");
                this.owner_name.setText("Owner : Indiawin Sports Pvt. Ltd");
                this.coach_name.setText("Coach : Mahela Jayawardene");
                this.home_venue.setText("Home Venue : Wankhede Stadium");
                squadsModelArrayList.add(new SquadsModel(R.drawable.mi1, "Rohit Sharma", "Price : ₹16 Cr", R.drawable.captain, " Batsman", "Right-handed ", "Right-arm off-spin  ", "Indian", R.color.mi));
                squadsModelArrayList.add(new SquadsModel(R.drawable.mi8, "Ishan Kishan", "Price : ₹15.25 Cr", R.drawable.blankbg, "Batsman", "Left-handed", "Right-arm medium  ", "Indian", R.color.mi));
                squadsModelArrayList.add(new SquadsModel(R.drawable.mi17, "Suryakumar Yadav", "Price : ₹8 Cr", R.drawable.blankbg, "Batsman", "Right-handed", "Right-arm medium  ", "Indian", R.color.mi));
                squadsModelArrayList.add(new SquadsModel(R.drawable.empty_profile, "Dewald Brewis", "Price : ₹3 Cr", R.drawable.blankbg, "All-Rounder", "Right-handed", "Right-arm medium  ", "South African", R.color.mi));
                squadsModelArrayList.add(new SquadsModel(R.drawable.mi4, "Anmolpreet Singh", "Price : ₹20 L", R.drawable.blankbg, "Batsman", "Right-handed", "Right-arm off-break  ", "Indian", R.color.mi));
                squadsModelArrayList.add(new SquadsModel(R.drawable.mi11, "Kieron Pollard", "Price : ₹6 Cr", R.drawable.plane, "All-rounder", "Right-handed", "Right-arm medium  ", "West Indian", R.color.mi));
                squadsModelArrayList.add(new SquadsModel(R.drawable.empty_profile, "Tilak Varmad", "Price : ₹1.7 Cr", R.drawable.blankbg, "Batsman", "Left-Handed", "Right-arm offbreak", "Indian", R.color.mi));
                squadsModelArrayList.add(new SquadsModel(R.drawable.empty_profile, "Ramandeep Singh", "Price : ₹20 L", R.drawable.blankbg, "Batsman", "Right Handed Bat", "Right-arm medium", "Indian", R.color.mi));
                squadsModelArrayList.add(new SquadsModel(R.drawable.empty_profile, "Sanjay Yadav", "Price : ₹50 L", R.drawable.blankbg, "Bowling Allrounder", "Left Handed Bat", "Left-arm orthodox", "Indian", R.color.mi));
                squadsModelArrayList.add(new SquadsModel(R.drawable.empty_profile, "Tim David", "Price : ₹8.25 Cr", R.drawable.blankbg, "Batting Allrounder", "Right Handed Bat", "Right-arm medium", "Singapore", R.color.mi));
                squadsModelArrayList.add(new SquadsModel(R.drawable.empty_profile, "Fabian Allen", "Price : ₹75 L", R.drawable.plane, "All-rounder", "Left-handed", "  Right-arm medium  ", "New Zealander", R.color.mi));
                squadsModelArrayList.add(new SquadsModel(R.drawable.empty_profile, "Daniel Sams", "Price : ₹2.6 Cr", R.drawable.plane, "All rounder", "Right-handed", "  Medium Speed  ", "Indian", R.color.mi));
                squadsModelArrayList.add(new SquadsModel(R.drawable.empty_profile, "Aryan Juyal", "Price : ₹20 L", R.drawable.plane, "WK-Batsman", "Right Handed Bat", "", "Indian", R.color.mi));
                squadsModelArrayList.add(new SquadsModel(R.drawable.xip12, "Murugan Ashwin", "Price : ₹1.6 Cr", R.drawable.blankbg, "Bowler", "   Right-handed   ", "Leg-break", "Indian", R.color.mi));
                squadsModelArrayList.add(new SquadsModel(R.drawable.rr7, "Jofra Archer", "Price : ₹8 Cr", R.drawable.plane, "Bowler", "Right-handed", "  Right-arm fast  ", "English", R.color.mi));
                squadsModelArrayList.add(new SquadsModel(R.drawable.mi9, "Jasprit Bumrah", "Price : ₹12 Cr", R.drawable.blankbg, "Bowler", "Right-handed", "Right-arm medium  ", "Indian", R.color.mi));
                squadsModelArrayList.add(new SquadsModel(R.drawable.srh4, "Basil Thampi", "Price : ₹20 L", R.drawable.blankbg, "Bowler", "Right-handed", "  Right-arm fast medium  ", "Indian", R.color.mi));
                squadsModelArrayList.add(new SquadsModel(R.drawable.rr6, "Jaydev Unadkat", "Price : ₹1.3 Cr", R.drawable.blankbg, "Bowler", "Right-handed", "  Left-arm medium fast  ", "Indian", R.color.mi));
                squadsModelArrayList.add(new SquadsModel(R.drawable.empty_profile, "Rahul Buddhi", "Price : ₹20 L", R.drawable.blankbg, "Batsman", "Left-Handed Batsman", "", "Indian", R.color.mi));
                squadsModelArrayList.add(new SquadsModel(R.drawable.rr12, "Mayank Markande", "Price : ₹65 L", R.drawable.blankbg, "Bowler", "Right-handed", "  Right-arm leg break  ", "Indian", R.color.mi));
                squadsModelArrayList.add(new SquadsModel(R.drawable.empty_profile, "Tymal Mills", "Price : ₹1.5 Cr", R.drawable.plane, "Bowler", "Right Handed Bat", "Left-arm fast-medium", "English", R.color.mi));
                squadsModelArrayList.add(new SquadsModel(R.drawable.empty_profile, "Riley Meredith", "Price : ₹1 Cr", R.drawable.plane, "Bowler", "Right Handed Bat", "Right-arm fast-medium", "Australia", R.color.mi));
                squadsModelArrayList.add(new SquadsModel(R.drawable.empty_profile, "Mohd. Arshad Khan", "Price : ₹20 L", R.drawable.blankbg, "Batsman", "Left-Handed Batsman", " ", "Indian", R.color.mi));
                squadsModelArrayList.add(new SquadsModel(R.drawable.empty_profile, "Hrithik Shokeen", "Price : ₹20 L", R.drawable.blankbg, "Bowling Allrounder", "Right Handed Bat", "Right-arm offbreak ", "Indian", R.color.mi));
                squadsModelArrayList.add(new SquadsModel(R.drawable.empty_profile, "Arjun Tendulkar", "Price : ₹30 L", R.drawable.blankbg, "All-rounder", "Left-handed", " Right-arm medium fast  ", "Indian", R.color.mi));
                break;
            case "KKR":
                textView.setText("KKR");
                this.background_team.setBackground(getResources().getDrawable(R.drawable.bg_kkr));
                this.icon_team.setImageDrawable(getResources().getDrawable(R.drawable.kkricon));
                this.team_name.setText("Kolkata Knight Riders");
                this.owner_name.setText("Owner : Knight Riders Sports Private Ltd");
                this.coach_name.setText("Coach : Jacques Kallis");
                this.home_venue.setText("Home Venue : Eden Gardens");
                squadsModelArrayList.add(new SquadsModel(R.drawable.dc1, "Shreyas Iyer", "Price : ₹12.25 Cr", R.drawable.captain, "Batsman", "Right-handed", "Right-arm off-break  ", "Indian", R.color.kkr));
                squadsModelArrayList.add(new SquadsModel(R.drawable.empty_profile, "Alex Hales", "Price : ₹1.5 Cr", R.drawable.plane, "Batsman", "Right Handed Bat", "Right-arm medium", "England", R.color.kkr));
                squadsModelArrayList.add(new SquadsModel(R.drawable.kkrno, " Venkatesh lyer", "Price : ₹8 Cr", R.drawable.blankbg, "All Rounder  ", "Left-handed", "Right-arm medium", "Indian", R.color.kkr));
                squadsModelArrayList.add(new SquadsModel(R.drawable.kkr7, "Nitish Rana", "Price : ₹8 Cr", R.drawable.blankbg, "Batsman", "Left-handed", "Right-arm off-break  ", "Indian", R.color.kkr));
                squadsModelArrayList.add(new SquadsModel(R.drawable.empty_profile, "Sam Billings", "Price : ₹2 Cr", R.drawable.plane, "Batsman", "   Right-handed   ", "N/A", "England", R.color.kkr));
                squadsModelArrayList.add(new SquadsModel(R.drawable.kkr12, "Rinku Singh", "Price : ₹55 L", R.drawable.blankbg, "Batsman", "Right-handed", "Right-arm off break  ", "Indian", R.color.kkr));
                squadsModelArrayList.add(new SquadsModel(R.drawable.kkrno, "Sheldon Jackson", "Price : ₹60 L", R.drawable.blankbg, "Wicket Keeper", " Right Handed   ", " N/A ", "India", R.color.kkr));
                squadsModelArrayList.add(new SquadsModel(R.drawable.empty_profile, "Baba Indrajith", "Price : ₹20 L", R.drawable.blankbg, "Batsman", " Right Handed Bat   ", "Right-arm legbreak ", "India", R.color.kkr));
                squadsModelArrayList.add(new SquadsModel(R.drawable.empty_profile, "Abhijeet Tomar", "Price : ₹40 L", R.drawable.blankbg, "Batsman", " Right Handed Bat   ", "Right-arm offbreak ", "India", R.color.kkr));
                squadsModelArrayList.add(new SquadsModel(R.drawable.empty_profile, "Pratham Singh", "Price : ₹20 L", R.drawable.blankbg, "Batsman", " Left Handed Bat  ", "Right-arm offbreak ", "India", R.color.kkr));
                squadsModelArrayList.add(new SquadsModel(R.drawable.kkr2, "Andre Russell", "Price : ₹12 Cr", R.drawable.plane, "All-rounder", "Right-handed", "Right-arm fast   ", "West Indian", R.color.kkr));
                squadsModelArrayList.add(new SquadsModel(R.drawable.srh11, "Mohammad Nabi", "Price : ₹1 Cr", R.drawable.plane, "All-rounder", "Right-handed", "Right-arm off break  ", "Afghan", R.color.kkr));
                squadsModelArrayList.add(new SquadsModel(R.drawable.mi3, "Anukul Roy", "Price : ₹20 L", R.drawable.blankbg, "All-rounder", "Left-handed", "Slow left-arm orthodox  ", "Indian", R.color.kkr));
                squadsModelArrayList.add(new SquadsModel(R.drawable.empty_profile, "Chamika Karunaratne", "Price : ₹50 L", R.drawable.plane, "Bowler", "Right Handed Bat", "Right-arm fast-medium ", "Sri Lanka", R.color.kkr));
                squadsModelArrayList.add(new SquadsModel(R.drawable.kkr15, "Sunil Narine", "Price : ₹6 Cr", R.drawable.plane, "Bowler", "Left-handed", "Right-arm off-spin  ", "West Indian", R.color.kkr));
                squadsModelArrayList.add(new SquadsModel(R.drawable.kkr8, "Pat Cummins", "Price : ₹7.25 Cr", R.drawable.plane, "Bowler", "Right-handed", "Right-arm fast  ", "Australian", R.color.kkr));
                squadsModelArrayList.add(new SquadsModel(R.drawable.empty_profile, "Tim Southee", "Price : ₹1.5 Cr", R.drawable.plane, "Bowler", "Right Handed Bat", "Right-arm fast-medium ", "New Zealand", R.color.kkr));
                squadsModelArrayList.add(new SquadsModel(R.drawable.kkr13, "Shivam Mavi", "Price : ₹7.25 Cr", R.drawable.blankbg, "All-rounder", "Right-handed", "Right-arm fast  ", "Indian", R.color.kkr));
                squadsModelArrayList.add(new SquadsModel(R.drawable.kkr17, "Varun Chakravarthy", "Price : ₹8 Cr", R.drawable.blankbg, "Bowler", "Right-handed   ", "Leg-break", "Indian", R.color.kkr));
                squadsModelArrayList.add(new SquadsModel(R.drawable.empty_profile, "Rasikh Dar", "Price : ₹20 L", R.drawable.blankbg, "Bowler", "Right-handed   ", "Right-arm fast medium", "Indian", R.color.kkr));
                squadsModelArrayList.add(new SquadsModel(R.drawable.empty_profile, "Ashok Sharma", "Price : ₹55 L", R.drawable.blankbg, "Bowler", "Right-handed   ", "Right-arm fast medium", "Indian", R.color.kkr));
                squadsModelArrayList.add(new SquadsModel(R.drawable.empty_profile, "Ramesh Kumar", "Price : ₹20 L", R.drawable.blankbg, "Bowler", "Right-handed   ", "Right-arm fast medium", "Indian", R.color.kkr));
                squadsModelArrayList.add(new SquadsModel(R.drawable.empty_profile, "Umesh Yadav", "Price : ₹2 Cr", R.drawable.blankbg, "Bowler", "  Right-handed   ", " Right-arm fast", "Indian", R.color.kkr));
                squadsModelArrayList.add(new SquadsModel(R.drawable.empty_profile, "Aman Khan", "Price : ₹20 L", R.drawable.blankbg, "Batting Allrounder", " Right Handed Bat", " Right-arm fast", "Indian", R.color.kkr));
                break;
            case "SRH":
                textView.setText("SRH");
                this.background_team.setBackground(getResources().getDrawable(R.drawable.bg_srh));
                this.icon_team.setImageDrawable(getResources().getDrawable(R.drawable.srhicon));
                this.team_name.setText("Sunrisers Hyderabad");
                this.owner_name.setText("Owner : SUN TV Network");
                this.coach_name.setText("Coach : Tom Moody");
                this.home_venue.setText("Home Venue : Rajiv Gandhi Intl. Cricket Stadium");
                squadsModelArrayList.add(new SquadsModel(R.drawable.srh8, "Kane Williamson", "Price : ₹14 Cr", R.drawable.captain, "Batsman", "Right-handed", "Right-arm off-spin  ", "New Zealander", R.color.srh));
                squadsModelArrayList.add(new SquadsModel(R.drawable.kkr10, "Rahul Tripathi", "Price : ₹8.5 Cr", R.drawable.blankbg, "Batsman", "Right-handed   ", "", "Indian", R.color.srh));
                squadsModelArrayList.add(new SquadsModel(R.drawable.empty_profile, "Aiden Markram", "Price : ₹2.6 Cr", R.drawable.plane, "Batsman", "Right-handed   ", "Right-arm off-spin", "South Afrcia", R.color.srh));
                squadsModelArrayList.add(new SquadsModel(R.drawable.xip13, "Nicholas Pooran", "Price : ₹10.75 Cr", R.drawable.plane, "Batsman", "   Left-handed   ", "", "West Indian", R.color.srh));
                squadsModelArrayList.add(new SquadsModel(R.drawable.empty_profile, "Romario Shepherd ", "Price : ₹7.75 Cr", R.drawable.plane, "Bowler", "Right Handed Bat", "Right-arm fast-medium", "West Indian", R.color.srh));
                squadsModelArrayList.add(new SquadsModel(R.drawable.empty_profile, "Glenn Phillips", "Price : ₹1.5 Cr", R.drawable.plane, "WK-Batsman", "Right Handed Bat", "Right-arm offbreak", "New Zealand", R.color.srh));
                squadsModelArrayList.add(new SquadsModel(R.drawable.srh12, "Priyam Garg", "Price : ₹20 L", R.drawable.blankbg, "Batsman", "   Right-handed   ", "", "Indian", R.color.srh));
                squadsModelArrayList.add(new SquadsModel(R.drawable.srh2, "Abdul Samad", "Price : ₹4 Cr", R.drawable.blankbg, "", "  Right-handed  ", "Legbreak", "Indian", R.color.srh));
                squadsModelArrayList.add(new SquadsModel(R.drawable.empty_profile, "R Samarth", "Price : ₹20 L", R.drawable.blankbg, "Batsman", "Right Handed Bat", "Right-arm offbreak", "Indian", R.color.srh));
                squadsModelArrayList.add(new SquadsModel(R.drawable.empty_profile, "Vishnu Vinod", "Price : ₹50 L", R.drawable.blankbg, "Wicket keeper", "  Right-handed   ", " N/A", "Indian", R.color.srh));
                squadsModelArrayList.add(new SquadsModel(R.drawable.rcb11, "Washington Sundar", "Price : ₹8.75 Cr", R.drawable.blankbg, "All-rounder", "Left-handed", "Right-arm off-break   ", "Indian", R.color.srh));
                squadsModelArrayList.add(new SquadsModel(R.drawable.srh3, "Abhishek Sharma", "Price : ₹6.5 Cfr", R.drawable.blankbg, "Batsman", "Left-handed", "  Left-arm orthodox  ", "Indian", R.color.srh));
                squadsModelArrayList.add(new SquadsModel(R.drawable.rr15, "Shreyas Gopal", "Price : ₹75 L", R.drawable.blankbg, "All-rounder", "Right-handed", "  Right-arm leg spin  ", "Indian", R.color.srh));
                squadsModelArrayList.add(new SquadsModel(R.drawable.empty_profile, "Shashank Singh", "Price : ₹20 L", R.drawable.blankbg, "Batsman", "Right Handed Bat", "Right-arm offbreak", "Indian", R.color.srh));
                squadsModelArrayList.add(new SquadsModel(R.drawable.empty_profile, "Marco Jansen", "Price : ₹4.2 Cr", R.drawable.plane, "Bowler", "Right-handed", "Left-arm fast  ", "South African", R.color.srh));
                squadsModelArrayList.add(new SquadsModel(R.drawable.empty_profile, "Sean Abbott ", "Price : ₹2.4 Cr", R.drawable.plane, "Bowling Allrounder", "Right Handed Bat", "Right-arm medium", "Australia", R.color.srh));
                squadsModelArrayList.add(new SquadsModel(R.drawable.srh5, "Bhuvneshwar Kumar", "Price : ₹4.2 Cr", R.drawable.blankbg, "Bowler", "Right-handed", "   Right-arm medium   ", "Indian", R.color.srh));
                squadsModelArrayList.add(new SquadsModel(R.drawable.empty_profile, "Umran Malik ", "Price : ₹4 Cr", R.drawable.blankbg, "Bowler", "Right handed", "Right arm fast", "Indian", R.color.srh));
                squadsModelArrayList.add(new SquadsModel(R.drawable.srh19, "T Natarajan", "Price : ₹4 Cr", R.drawable.blankbg, "Bowler", "Left-handed", "  Left-arm medium-fast  ", "Indian", R.color.srh));
                squadsModelArrayList.add(new SquadsModel(R.drawable.rr9, "Kartik Tyagi", "Price : ₹4 Cr", R.drawable.blankbg, "Bowler", "Right-handed", "  Right-arm Medium Fast  ", "Indian", R.color.srh));
                squadsModelArrayList.add(new SquadsModel(R.drawable.empty_profile, "Jagadeesha suchita ", "Price : ₹20 L", R.drawable.blankbg, "Bowler", "Left-handed", "   Right-arm fast   ", "Indian", R.color.srh));
                squadsModelArrayList.add(new SquadsModel(R.drawable.empty_profile, "Saurabh Kumar", "Price : ₹20 L", R.drawable.blankbg, "Bowler", "Left-handed", "  Slow left-arm orthodox  ", "Indian", R.color.srh));
                squadsModelArrayList.add(new SquadsModel(R.drawable.empty_profile, "Fazalhaq Farooqi", "Price : ₹50 L", R.drawable.blankbg, "Bowler", "Left-handed", "Left-Arm Medium Bowler", "Afghanistan", R.color.srh));
                break;
            case "RCB":
                textView.setText("RCB");
                this.background_team.setBackground(getResources().getDrawable(R.drawable.bg_rcb));
                this.icon_team.setImageDrawable(getResources().getDrawable(R.drawable.rcbion));
                this.team_name.setText("Royal Challengers Bangalore");
                this.owner_name.setText("Owner : Royal Challengers Sports Private Ltd");
                this.coach_name.setText("Coach : Daniel Vettori");
                this.home_venue.setText("Home Venue : M. Chinnaswamy Stadium");
                squadsModelArrayList.add(new SquadsModel(R.drawable.csk6, "Faf du Plessis", "Price : ₹7 Cr", R.drawable.captain, "Batsman", "Right-handed", "Right-arm leg spin  ", "South African", R.color.rcb));
                squadsModelArrayList.add(new SquadsModel(R.drawable.rcb1, "Virat Kohli", "Price : ₹15 Cr", R.drawable.blankbg, "Batsman", "Right-handed", "Right-arm medium  ", "Indian", R.color.rcb));
                squadsModelArrayList.add(new SquadsModel(R.drawable.rcb15, "Glenn Maxwell", "Price : ₹11 Cr", R.drawable.plane, "  All Rounder  ", "Right Handed", "Spin", "Australian", R.color.rcb));
                squadsModelArrayList.add(new SquadsModel(R.drawable.empty_profile, " Sherfane Rutherford", "Price : ₹1 Cr", R.drawable.plane, "Batting Allrounder", "Left Handed Bat", "Right-arm fast-medium", "West Indies", R.color.rcb));
                squadsModelArrayList.add(new SquadsModel(R.drawable.kkr3, "Dinesh Karthik", "Price : ₹5.5 Cr", R.drawable.blankbg, "  Wicketkeeper batsman  ", "Right-handed", "", "Indian", R.color.rcb));
                squadsModelArrayList.add(new SquadsModel(R.drawable.rr3, "Anuj Rawat", "Price : ₹3.4 Cr", R.drawable.blankbg, "  Wicketkeeper batsman  ", "Left-handed", "", "Indian", R.color.rcb));
                squadsModelArrayList.add(new SquadsModel(R.drawable.empty_profile, "Wanindu Hasaranga", "Price : ₹10.75 Cr", R.drawable.plane, "Bowling Allrounder", "Right Handed Bat", "Right-arm legbreak", "Sri Lanka", R.color.rcb));
                squadsModelArrayList.add(new SquadsModel(R.drawable.empty_profile, "Finn Allen", "Price : ₹80 L", R.drawable.plane, "WK-Batsman", "Right Handed Bat", "", "New Zealand", R.color.rcb));
                squadsModelArrayList.add(new SquadsModel(R.drawable.rr10, "Mahipal Lomror", "Price : ₹95 L", R.drawable.blankbg, "All-rounder", "Left-handed", "  Left-arm spin  ", "Indian", R.color.rcb));
                squadsModelArrayList.add(new SquadsModel(R.drawable.rcb10, "Shahbaz Ahmed", "Price : ₹2.4 Cr", R.drawable.blankbg, "", "Left-handed", "Left-arm orthodox  ", "Indian", R.color.rcb));
                squadsModelArrayList.add(new SquadsModel(R.drawable.empty_profile, "Aneeshwar Gautam", "Price : ₹20 L", R.drawable.blankbg, "", "Left hand bat", "Slow left arm orthodox  ", "Indian", R.color.rcb));
                squadsModelArrayList.add(new SquadsModel(R.drawable.csk10, "Karn Sharma", "Price : ₹50 L", R.drawable.blankbg, " All-rounder", "Left-handed", "  Right-arm leg spin  ", "Indian", R.color.rcb));
                squadsModelArrayList.add(new SquadsModel(R.drawable.rcb14, "Harshal Patel", "Price : ₹10.75 Cr", R.drawable.blankbg, "Batting all-rounder", "Left-handed", "Right-arm off-break  ", "English", R.color.rcb));
                squadsModelArrayList.add(new SquadsModel(R.drawable.rcb7, "Mohammed Siraj", "Price : ₹7 Cr", R.drawable.blankbg, "Bowler", "Right-handed", "Right-arm medium-fast  ", "Indian", R.color.rcb));
                squadsModelArrayList.add(new SquadsModel(R.drawable.csk9, "Josh Hazlewood", "Price : ₹7.75 Cr", R.drawable.plane, "Bowler", "Left-handed", "Right-arm fast-medium  ", "Australian", R.color.rcb));
                squadsModelArrayList.add(new SquadsModel(R.drawable.empty_profile, "Jason Behrendorff", "Price : ₹75 L", R.drawable.plane, "Bowler", "Right Handed Bat", "Left-arm fast-medium", "Australian", R.color.rcb));
                squadsModelArrayList.add(new SquadsModel(R.drawable.empty_profile, "Akash Deep", "Price : ₹20 L", R.drawable.blankbg, "Bowler", "Right Handed Bat", "Right-arm medium", "Indian", R.color.rcb));
                squadsModelArrayList.add(new SquadsModel(R.drawable.rcb21, "Suyash Prabhudessai", "Price : ₹30 L", R.drawable.blankbg, "Batsman", "Right-handed", "Medium Speed", "Indian", R.color.rcb));
                squadsModelArrayList.add(new SquadsModel(R.drawable.empty_profile, "Chama Milind", "Price : ₹25 L", R.drawable.blankbg, "Batsman", "Left Handed Bat", "Left-arm medium", "Indian", R.color.rcb));
                squadsModelArrayList.add(new SquadsModel(R.drawable.srh17, "Siddarth Kaul", "Price : ₹75 L", R.drawable.blankbg, "Bowler", "Right-handed", "  Right-arm medium fast  ", "Indian", R.color.rcb));
                squadsModelArrayList.add(new SquadsModel(R.drawable.empty_profile, "Luvnith Sisodia", "Price : ₹20 L", R.drawable.blankbg, "WK-Batsman", "Left Handed Bat", "Left-arm medium", "Indian", R.color.rcb));
                squadsModelArrayList.add(new SquadsModel(R.drawable.empty_profile, "David Willey", "Price : ₹2 Cr", R.drawable.blankbg, "Bowling Allrounder", "Left Handed Bat", "Left-arm fast-medium", "England", R.color.rcb));
                break;
            case "DC":
                textView.setText("DC");
                this.background_team.setBackground(getResources().getDrawable(R.drawable.bg_dc));
                this.icon_team.setImageDrawable(getResources().getDrawable(R.drawable.dcicon));
                this.team_name.setText("Delhi Capitals");
                this.owner_name.setText("Owner : GMR Sports Pvt .Ltd & JSW Sports Pvt Ltd");
                this.coach_name.setText("Coach : Ricky Ponting");
                this.home_venue.setText("Home Venue : Feroz Shah Kotla Ground");
                squadsModelArrayList.add(new SquadsModel(R.drawable.dc15, "Rishabh Pant", "Price : ₹16 Cr", R.drawable.captain, "  Wicketkeeper-batsman  ", "Left-handed", "", "Indian", R.color.dc));
                squadsModelArrayList.add(new SquadsModel(R.drawable.srh1, "David Warner", "Price : ₹6.25 Cr", R.drawable.plane, "Batsman", "Left-handed", "  Right-arm leg spin  ", "Australian", R.color.dc));
                squadsModelArrayList.add(new SquadsModel(R.drawable.dc13, "Prithvi Shaw", "Price : ₹7.5 Cr", R.drawable.blankbg, "Batsman", "   Right-handed   ", "", "Indian", R.color.dc));
                squadsModelArrayList.add(new SquadsModel(R.drawable.xip9, "Mandeep Singh", "Price : ₹1.1 Cr", R.drawable.blankbg, "All-rounder", "Right-handed", "  Right-arm medium  ", "Indian", R.color.dc));
                squadsModelArrayList.add(new SquadsModel(R.drawable.empty_profile, "Rovman Powell", "Price : ₹2.8 Cr", R.drawable.plane, "Batsman", "Right Handed Bat", "  Right-arm medium  ", "West Indies", R.color.dc));
                squadsModelArrayList.add(new SquadsModel(R.drawable.kkr16, "Tim Seifert", "Price : ₹50 L", R.drawable.plane, "Wicket-keeper, Batsman", "Right-handed", " N/A ", "New Zealand", R.color.dc));
                squadsModelArrayList.add(new SquadsModel(R.drawable.empty_profile, " Ashwin Hebbar", "Price : ₹20 L", R.drawable.blankbg, "Batsman", "Right-handed", "Right-arm medium", "India", R.color.dc));
                squadsModelArrayList.add(new SquadsModel(R.drawable.xip16, "Sarfaraz Khan", "Price : ₹20 L", R.drawable.blankbg, "Batsman", "   Right-handed   ", "Legbreak", "Indian", R.color.dc));
                squadsModelArrayList.add(new SquadsModel(R.drawable.empty_profile, "Yash Dhull", "Price : ₹50 L", R.drawable.blankbg, "Batsman", "   Right-handed   ", "Right arm offbreak", "Indian", R.color.dc));
                squadsModelArrayList.add(new SquadsModel(R.drawable.rcb22, "Kona Srikar Bharat", "Price : ₹2 Cr", R.drawable.blankbg, "Wicket Keeper", "Right-handed", "N/A", "India", R.color.dc));
                squadsModelArrayList.add(new SquadsModel(R.drawable.empty_profile, "Ripal Patel", "Price : ₹20 L", R.drawable.blankbg, "Bowler", "  Right-handed   ", " Right-arm medium fast", "Indian", R.color.dc));
                squadsModelArrayList.add(new SquadsModel(R.drawable.srh10, "Mitchell Marsh", "Price : ₹6.5 Cr", R.drawable.plane, "All-rounder", "Right-handed", "   Right-arm medium   ", "Australian", R.color.dc));
                squadsModelArrayList.add(new SquadsModel(R.drawable.dc6, "Axar Patel", "Price : ₹9 Cr", R.drawable.blankbg, "All-rounder", "Left-handed", "  Left-arm orthodox spin  ", "Indian", R.color.dc));
                squadsModelArrayList.add(new SquadsModel(R.drawable.csk18, "Shardul Thakur", "Price : ₹10.75 Cr", R.drawable.blankbg, "Bowler", "Right-handed", "Right-arm medium  ", "Indian", R.color.dc));
                squadsModelArrayList.add(new SquadsModel(R.drawable.empty_profile, "Lalith Yadav", "Price : ₹65 L", R.drawable.blankbg, "All-rounder", "Right-handed", "Right-arm off-break  ", "Indian", R.color.dc));
                squadsModelArrayList.add(new SquadsModel(R.drawable.kkr5, "Kuldeep Yadav", "Price : ₹2 Cr", R.drawable.blankbg, "Bowler", "Left-handed", "Left-arm orthodox spin  ", "Indian", R.color.dc));
                squadsModelArrayList.add(new SquadsModel(R.drawable.kkr4, "Kamlesh Nagarkoti", "Price : ₹1.1 Cr", R.drawable.blankbg, "Bowler", "Right-handed", "Right-arm fast  ", "Indian", R.color.dc));
                squadsModelArrayList.add(new SquadsModel(R.drawable.dc12, "Pravin Dubey", "Price : ₹50 L", R.drawable.blankbg, "Bowler", "Right-handed", " Lwg Spin ", "Indian", R.color.dc));
                squadsModelArrayList.add(new SquadsModel(R.drawable.dc4, "Anrich Nortje", "Price : ₹6.5 Cr", R.drawable.plane, "Bowler", "Right-handed", "  Fast Seam", "South African", R.color.dc));
                squadsModelArrayList.add(new SquadsModel(R.drawable.rr19, " Cetan Sakariya ", "Price : ₹4.2 Cr", R.drawable.blankbg, "Bowler", "Left-handed", "  Left arm fast medium  ", "Indian", R.color.dc));
                squadsModelArrayList.add(new SquadsModel(R.drawable.srh18, "Khaleel Ahmed", "Price : ₹5.25 Cr", R.drawable.blankbg, "Bowler", "Right-handed", "  Left-arm medium pace  ", "Indian", R.color.dc));
                squadsModelArrayList.add(new SquadsModel(R.drawable.rr18, "Mustafizur Rahman", "Price : ₹2 Cr", R.drawable.blankbg, "bowler", "  Left-handed  ", "Left arm fast medium", "Indian", R.color.dc));
                squadsModelArrayList.add(new SquadsModel(R.drawable.csk11, "Lungi Ngidi", "Price : ₹50 L", R.drawable.plane, " Bowler", "Right-handed", "Right-arm fast  ", "South African", R.color.dc));
                squadsModelArrayList.add(new SquadsModel(R.drawable.empty_profile, "Vicky Ostwal", "Price : ₹20 L", R.drawable.blankbg, " Bowler", "Right hand bat", "Left-arm orthodox", "Indian", R.color.dc));

                break;
            case "PBKS":
                textView.setText("PBKS");
                this.background_team.setBackground(getResources().getDrawable(R.drawable.bg_kxip));
                this.icon_team.setImageDrawable(getResources().getDrawable(R.drawable.kxiiicon));
                this.team_name.setText("Punjab Kings");
                this.owner_name.setText("Owner : KPH Dream Cricket Private Limited");
                this.coach_name.setText("Coach : Anil Kumble");
                this.home_venue.setText("Home Venue : IS Bindra Stadium");
                squadsModelArrayList.add(new SquadsModel(R.drawable.xip10, "Mayank Agarwal", "Price : ₹14 Cr", R.drawable.captain, "Batsman", "   Right-handed   ", "", "Indian", R.color.kxip));
                squadsModelArrayList.add(new SquadsModel(R.drawable.dc16, "Shikhar Dhawan", "Price : ₹8.25 Cr", R.drawable.blankbg, "Batsman", "   Left-handed   ", "", "Indian", R.color.kxip));
                squadsModelArrayList.add(new SquadsModel(R.drawable.srh7, "Jonny Bairstow", "Price : ₹6.75 Cr", R.drawable.plane, "  Wicketkeeper Batsman  ", "Right-handed", "", "English", R.color.kxip));
                squadsModelArrayList.add(new SquadsModel(R.drawable.empty_profile, " Shahrukh Khan ", "Price : ₹9 Cr", R.drawable.blankbg, "Batsman", "Right-handed", "Right-arm offbreak  ", "Indian", R.color.kxip));
                squadsModelArrayList.add(new SquadsModel(R.drawable.empty_profile, "Prabhsimran Singh", "Price : ₹60 L", R.drawable.blankbg, "Wicketkeeper", "Right-handed", "N/A", "Indian", R.color.kxip));
                squadsModelArrayList.add(new SquadsModel(R.drawable.empty_profile, "Jitesh Sharma", "Price : ₹20 L", R.drawable.blankbg, "Wicketkeeper", "Right-handed", "N/A", "Indian", R.color.kxip));
                squadsModelArrayList.add(new SquadsModel(R.drawable.empty_profile, "Atharva Taide", "Price : ₹20 L", R.drawable.blankbg, "Batsman", "Left-Handed Batsman", "Left-arm Orthodox", "Indian", R.color.kxip));
                squadsModelArrayList.add(new SquadsModel(R.drawable.rr21, "Liam Livingstone", "Price : ₹11.5 Cr", R.drawable.plane, "All Rounder", "Right-handed", "  Right-arm offbreak  ", "English", R.color.kxip));
                squadsModelArrayList.add(new SquadsModel(R.drawable.empty_profile, "Rishi Dhawan", "Price : ₹55 L", R.drawable.blankbg, "Bowling Allrounder", "Right Handed Bat", "Right-arm fast-medium", "Indian", R.color.kxip));
                squadsModelArrayList.add(new SquadsModel(R.drawable.empty_profile, "Odean Smith", "Price : ₹6 Cr", R.drawable.plane, "Bowler", "Right Handed Bat", "Right-arm medium", "West Indies", R.color.kxip));
                squadsModelArrayList.add(new SquadsModel(R.drawable.xip7, "Harpreet Brar", "Price : ₹3.8 Cr", R.drawable.blankbg, "Bowler", "Left-handed", "  Left-arm orthodox  ", "Indian", R.color.kxip));
                squadsModelArrayList.add(new SquadsModel(R.drawable.empty_profile, "Writtick Chatterjee", "Price : ₹20 L", R.drawable.blankbg, "Bowler", "Right Handed Bat", "Right-arm offbreak", "Indian", R.color.kxip));
                squadsModelArrayList.add(new SquadsModel(R.drawable.empty_profile, "Bhanuka Rajapaksa", "Price : ₹50 L", R.drawable.plane, "Batsman", "Left Handed Bat", "Right-arm medium", "Sri Lanka", R.color.kxip));
                squadsModelArrayList.add(new SquadsModel(R.drawable.empty_profile, "Nathan Ellis", "Price : ₹75 L", R.drawable.plane, "Bowler", "Right Handed Bat", "Right-arm fast-medium", "Australia", R.color.kxip));
                squadsModelArrayList.add(new SquadsModel(R.drawable.empty_profile, "Benny Howell", "Price : ₹40 L", R.drawable.plane, "Batting Allrounder", "Right Handed Bat", "Right-arm medium", "England", R.color.kxip));
                squadsModelArrayList.add(new SquadsModel(R.drawable.dc9, "Kagiso Rabada", "Price : ₹9.25 Cr", R.drawable.plane, "Bowler", "Right-handed", "  Right-arm fast  ", "South African", R.color.kxip));
                squadsModelArrayList.add(new SquadsModel(R.drawable.xip2, "Arshdeep Singh", "Price : ₹4 Cr", R.drawable.blankbg, "Bowler", "Left-handed", "  Left-arm medium-fast  ", "Indian", R.color.kxip));
                squadsModelArrayList.add(new SquadsModel(R.drawable.srh14, "Sandeep Sharma", "Price : ₹50 L", R.drawable.blankbg, "Bowler", "Right-handed", "  Right-arm medium  ", "Indian", R.color.kxip));
                squadsModelArrayList.add(new SquadsModel(R.drawable.mi15, "Rahul Chahar", "Price : ₹5.25 Cr", R.drawable.blankbg, "Bowler", "Right-handed", "Leg break googly  ", "Indian", R.color.kxip));
                squadsModelArrayList.add(new SquadsModel(R.drawable.empty_profile, "Ishan Porel", "Price : ₹25 L", R.drawable.blankbg, "Bowler", "Right-handed", "  Right-arm fast-medium  ", "Indian", R.color.kxip));
                squadsModelArrayList.add(new SquadsModel(R.drawable.empty_profile, "Prerak Mankad", "Price : ₹20 L", R.drawable.blankbg, "Batsman", "Right-handed", "  Right-arm medium  ", "India", R.color.kxip));
                squadsModelArrayList.add(new SquadsModel(R.drawable.kkrno, "Vaibhav Arora ", "Price : ₹2 Cr", R.drawable.blankbg, "Bowler", "Right-handed", "Right-arm fast-medium  ", "Indian", R.color.kxip));
                squadsModelArrayList.add(new SquadsModel(R.drawable.empty_profile, " Baltej Dhanda ", "Price : ₹20 L", R.drawable.blankbg, "Bowler", "Right-handed", "Right-Arm Medium Bowler ", "Indian", R.color.kxip));
                squadsModelArrayList.add(new SquadsModel(R.drawable.empty_profile, "Ansh Patel", "Price : ₹20 L", R.drawable.blankbg, "Bowler", "Right hand bat", "Slow left arm orthodox", "Indian", R.color.kxip));
                break;
            case "RR":
                textView.setText("RR");
                this.background_team.setBackground(getResources().getDrawable(R.drawable.bg_rr));
                this.icon_team.setImageDrawable(getResources().getDrawable(R.drawable.rricon));
                this.team_name.setText("Rajasthan Royals");
                this.owner_name.setText("Owner : Royal Multisport Pvt. Ltd");
                this.coach_name.setText("Coach: Rahul Dravid");
                this.home_venue.setText("Home Venue : Sawai Mansingh Stadium");
                squadsModelArrayList.add(new SquadsModel(R.drawable.rr1, "Sanju Samson", "Price : ₹14 Cr", R.drawable.captain, "  Wicketkeeper batsman  ", "Right-handed", "", "Indian", R.color.rr));
                squadsModelArrayList.add(new SquadsModel(R.drawable.rr24, "Yashasvi Jaiswal ", "Price : ₹4 Cr", R.drawable.blankbg, "Bataman", "Left-handed", "  Medium Seam  ", "Indian", R.color.rr));
                squadsModelArrayList.add(new SquadsModel(R.drawable.rcb4, "Devdutt Padikkal", "Price : ₹7.75 Cr", R.drawable.blankbg, "Batsman", "Left-handed", "Right-arm offbreak  ", "Indian", R.color.rr));
                squadsModelArrayList.add(new SquadsModel(R.drawable.rr8, "Jos Buttler", "Price : ₹10 Cr", R.drawable.plane, "  Wicketkeeper-batsman  ", "Right-handed", "", "English", R.color.rr));
                squadsModelArrayList.add(new SquadsModel(R.drawable.kkrno, "Karun Nair", "Price : ₹1.4 Cr", R.drawable.blankbg, "Batsman", "Right-handed", "Off break  ", "Indian", R.color.rr));
                squadsModelArrayList.add(new SquadsModel(R.drawable.dc17, "Shimron Hetmyer", "Price : ₹8.5 Cr", R.drawable.plane, "Batsman", "   Left-handed   ", "", "west Indian", R.color.rr));
                squadsModelArrayList.add(new SquadsModel(R.drawable.empty_profile, "Rassie Van Der Dussen", "Price : ₹1 Cr", R.drawable.plane, "Batsman", "Right Handed Bat", "Right-arm legbreak", "South Africa", R.color.rr));
                squadsModelArrayList.add(new SquadsModel(R.drawable.empty_profile, "Daryl Mitchell", "Price : ₹75 L", R.drawable.plane, "Bowling Allrounder", "Right Handed Bat", "Right-arm medium", "New Zealand", R.color.rr));
                squadsModelArrayList.add(new SquadsModel(R.drawable.empty_profile, "James Neesham", "Price : ₹1.5 Cr", R.drawable.plane, "Bowling Allrounder", "Left Handed Bat", "Right-arm medium", "New Zealand", R.color.rr));
                squadsModelArrayList.add(new SquadsModel(R.drawable.rr14, "Riyan Parag", "Price : ₹3.8 Cr", R.drawable.blankbg, "Batsman", "   Right-handed   ", "Leg break", "Indian", R.color.rr));
                squadsModelArrayList.add(new SquadsModel(R.drawable.empty_profile, " Dhruv Jurel", "Price : ₹20 L", R.drawable.blankbg, "WK-Batsman", "Right Handed Bat", "", "India", R.color.rr));
                squadsModelArrayList.add(new SquadsModel(R.drawable.dc14, "Ravichandran Ashwin", "Price : ₹5 Cr", R.drawable.blankbg, "Bowling all-rounder", "Right-handed", "Right-arm off-spin  ", "Indian", R.color.rr));
                squadsModelArrayList.add(new SquadsModel(R.drawable.mi18, "Trent Boult", "Price : ₹8` Cr", R.drawable.plane, "Bowler", "Right-handed", "Left-arm fast-medium  ", "New Zealande", R.color.rr));
                squadsModelArrayList.add(new SquadsModel(R.drawable.kkr9, "Prasidh Krishna", "Price : ₹10 Cr", R.drawable.blankbg, "Bowler", "Right-handed", "Right-arm fast   ", "Indian", R.color.rr));
                squadsModelArrayList.add(new SquadsModel(R.drawable.rcb12, "Yuzvendra Chahal", "Price : ₹6.5 Cr", R.drawable.blankbg, "Bowler", "Right-handed", "Right-arm leg spin  ", "Indian", R.color.rr));
                squadsModelArrayList.add(new SquadsModel(R.drawable.rr20, " KC Cariappa ", "Price : ₹30 L", R.drawable.blankbg, "Bowler", "Left-handed", "  Left arm fast medium  ", "Indian", R.color.rr));
                squadsModelArrayList.add(new SquadsModel(R.drawable.rcb8, "Navdeep Saini", "Price : ₹2.6 Cr", R.drawable.blankbg, "Bowler", "Right-handed", "Right-arm fast  ", "Indian", R.color.rr));
                squadsModelArrayList.add(new SquadsModel(R.drawable.empty_profile, "Obed Mccoy", "Price : ₹75 L", R.drawable.plane, "Bowler", "Left Handed Bat", "Left-arm fast-medium", "West Indies", R.color.rr));
                squadsModelArrayList.add(new SquadsModel(R.drawable.empty_profile, "Anunay Singh", "Price : ₹20 L", R.drawable.blankbg, "Bowling Allrounder", "Right Handed Batv", "Right-arm medium", "India", R.color.rr));
                squadsModelArrayList.add(new SquadsModel(R.drawable.empty_profile, "Kuldeep Sen", "Price : ₹20 L", R.drawable.blankbg, "Bowler", "Right Handed Bat", "Right-arm fast-medium", "Indian", R.color.rr));
                squadsModelArrayList.add(new SquadsModel(R.drawable.empty_profile, "Tejas Baroka", "Price : ₹20 L", R.drawable.blankbg, "Bowler", "Right Handed Bat", "Right-arm fast-medium", "Indian", R.color.rr));
                squadsModelArrayList.add(new SquadsModel(R.drawable.empty_profile, "Kuldip Yadav", "Price : ₹20 L", R.drawable.blankbg, "Bowler", "Left Handed Bat", "Left-arm fast-medium", "Indian", R.color.rr));
                squadsModelArrayList.add(new SquadsModel(R.drawable.mi20, "Nathan Coulter-Nile", "Price : ₹2 Cr", R.drawable.plane, "Bowler", "Right-handed", "Right-arm fast  ", "Australian", R.color.rr));

                break;
            case "GT":
                textView.setText("GT");
                this.background_team.setBackground(getResources().getDrawable(R.drawable.bg_gt));
                this.icon_team.setImageDrawable(getResources().getDrawable(R.drawable.gticon));
                this.team_name.setText("Gujarat Titans");
                this.owner_name.setText("Owner : CVC Capital Partners group");
                this.coach_name.setText("Coach : Ashish Nehra");
                this.home_venue.setText("Home Venue : Narendra Modi Stadium");
                squadsModelArrayList.add(new SquadsModel(R.drawable.mi7, "Hardik Pandya", "Price : ₹15 Cr", R.drawable.captain, "All-rounder", "Right-handed", "Right-arm medium fast  ", "Indian", R.color.gt));
                squadsModelArrayList.add(new SquadsModel(R.drawable.kkr14, "Shubman Gill", "Price : ₹8 Cr", R.drawable.blankbg, "Batsman", "Right-handed   ", "", "Indian", R.color.gt));
                squadsModelArrayList.add(new SquadsModel(R.drawable.empty_profile, "Jason Roy", "Price : ₹2 Cr", R.drawable.plane, "Batsman", "Right Handed Bat", "", "England", R.color.gt));
                squadsModelArrayList.add(new SquadsModel(R.drawable.empty_profile, "Abhinav Sadarangani", "Price : ₹2.6 Cr", R.drawable.blankbg, "Batsman", "Right Handed Bat", "", "Indian", R.color.gt));
                squadsModelArrayList.add(new SquadsModel(R.drawable.rr5, "David Miller", "Price : ₹3 Cr", R.drawable.plane, "Batsman", "Left-handed", "Right-arm off-spin  ", "South African", R.color.gt));
                squadsModelArrayList.add(new SquadsModel(R.drawable.empty_profile, "Matthew Wade", "Price : ₹2.4 Cr", R.drawable.plane, "WK-Batsman", "Left Handed Bat", "", "Australia", R.color.gt));
                squadsModelArrayList.add(new SquadsModel(R.drawable.srh22, "Wriddhiman Saha", "Price : ₹1.9 Cr", R.drawable.blankbg, "  Wicketkeeper batsman  ", "Right-handed", "", "Indian", R.color.gt));
                squadsModelArrayList.add(new SquadsModel(R.drawable.empty_profile, "Gurkeerat Singh", "Price : ₹50 L", R.drawable.blankbg, "Batsman", "Right Handed Bat", "Right-arm offbreak", "Indian", R.color.gt));
                squadsModelArrayList.add(new SquadsModel(R.drawable.rr13, "Rahul Tewatia", "Price : ₹9 Cr", R.drawable.blankbg, "Bowler", "   Right-handed   ", "Leg-spinner", "Indian", R.color.gt));
                squadsModelArrayList.add(new SquadsModel(R.drawable.srh20, "Vijay Shankar", "Price : ₹1.5 Cr", R.drawable.blankbg, "All-rounder", "Right-handed", "Right-arm off-break  ", "Indian", R.color.gt));
                squadsModelArrayList.add(new SquadsModel(R.drawable.mi10, "Jayant Yadav", "Price : ₹1.7 Cr", R.drawable.blankbg, "Bowler", "Right-handed", "Right-arm off-spin  ", "Indian", R.color.gt));
                squadsModelArrayList.add(new SquadsModel(R.drawable.srh13, "Rashid Khan", "Price : ₹15 Cr", R.drawable.plane, "All-rounder", "Right-handed", "  Right-arm leg spin  ", "Afghan", R.color.gt));
                squadsModelArrayList.add(new SquadsModel(R.drawable.xip11, "Mohammad Shami", "Price : ₹6.25 Cr", R.drawable.blankbg, "Bowler", "Right-handed", "  Right-arm medium  ", "Indian", R.color.gt));
                squadsModelArrayList.add(new SquadsModel(R.drawable.kkr6, "Lockie Ferguson", "Price : ₹10 Cr", R.drawable.plane, "Bowler", "Right-handed", "Right-arm fast   ", "New Zealander", R.color.gt));
                squadsModelArrayList.add(new SquadsModel(R.drawable.empty_profile, "Noor Ahmad", "Price : ₹30 L", R.drawable.plane, "Bowler", "Right Handed Bat", "Left-arm chinaman ", "Afghanistan", R.color.gt));
                squadsModelArrayList.add(new SquadsModel(R.drawable.csk13, "R Sai Kishore", "Price : ₹3 Cr", R.drawable.blankbg, "Bowler", "Left-handed", "Left-arm Leg Spin  ", "Indian", R.color.gt));
                squadsModelArrayList.add(new SquadsModel(R.drawable.empty_profile, "Dominic Drakes", "Price : ₹1.1 Cr", R.drawable.plane, "Bowler", "Left Handed Bat", "Left-arm fast-medium", "West Indies", R.color.gt));
                squadsModelArrayList.add(new SquadsModel(R.drawable.xip5, "Darshan Nalkande", "Price : ₹20 L", R.drawable.blankbg, "Bowler", "Right-handed", "  Right-arm fast-medium  ", "Indian", R.color.gt));
                squadsModelArrayList.add(new SquadsModel(R.drawable.empty_profile, "Yash Dayal", "Price : ₹3.2 Cr", R.drawable.blankbg, "Bowler", "Left Handed Bat", "Left-arm fast-medium", "Indian", R.color.gt));
                squadsModelArrayList.add(new SquadsModel(R.drawable.empty_profile, "Alzarri Joseph", "Price : ₹2.4 Cr", R.drawable.blankbg, "Bowler", "Right Handed Bat", "Right-arm fast-medium", "West Indies", R.color.gt));
                squadsModelArrayList.add(new SquadsModel(R.drawable.empty_profile, "Pradeep Sangwan", "Price : ₹20 L", R.drawable.blankbg, "Bowler", "Right Handed Bat", "Left-arm fast-medium", "Indian", R.color.gt));
                squadsModelArrayList.add(new SquadsModel(R.drawable.empty_profile, "Varun Aaron", "Price : ₹50 L", R.drawable.blankbg, "Bowler", "Right Handed Bat", "Right-arm fast", "Indian", R.color.gt));
                squadsModelArrayList.add(new SquadsModel(R.drawable.empty_profile, "B. Sai Sudharsan", "Price : ₹20 L", R.drawable.blankbg, "Batsman", "Left Handed Bat", "Right-arm legbreak", "Indian", R.color.gt));

                break;
            case "LSG":
                textView.setText("LSG");
                this.background_team.setBackground(getResources().getDrawable(R.drawable.bg_lsg));
                this.icon_team.setImageDrawable(getResources().getDrawable(R.drawable.lsgicon));
                this.team_name.setText("Lucknow Supergiants");
                this.owner_name.setText("Owner : Sanjiv Goenka");
                this.coach_name.setText("Coach : Andy Flower");
                this.home_venue.setText("Home Venue : BRSABV Ekana Cricket Stadium");
                squadsModelArrayList.add(new SquadsModel(R.drawable.xip1, "KL Rahul", "Price : ₹17 Cr", R.drawable.captain, "  Wicketkeeper batsman  ", "Right-handed", "", "Indian", R.color.lsg));
                squadsModelArrayList.add(new SquadsModel(R.drawable.mi14, "Quinton de Kock", "Price : ₹6.75 Cr", R.drawable.plane, "Wicketkeeper batsman  ", "Left-handed", "", "South African", R.color.lsg));
                squadsModelArrayList.add(new SquadsModel(R.drawable.empty_profile, "Evin Lewis", "Price : ₹2 Cr", R.drawable.plane, "Batsman", "Left Handed Bat", "", "West Indies", R.color.lsg));
                squadsModelArrayList.add(new SquadsModel(R.drawable.rr11, "Manan Vohra", "Price : ₹20 L", R.drawable.blankbg, "Batsman", "Right-handed", "  Right-arm medium  ", "Indian", R.color.lsg));
                squadsModelArrayList.add(new SquadsModel(R.drawable.empty_profile, "Kyle Mayers", "Price : ₹50 L", R.drawable.blankbg, "Batting Allrounder", "Left Handed Bat", "Right-arm medium", "West Indies", R.color.lsg));
                squadsModelArrayList.add(new SquadsModel(R.drawable.srh9, "Manish Pandey", "Price : ₹4.6 Cr", R.drawable.blankbg, "Batsman", "Right-handed", "Right-arm off-spin  ", "Indian", R.color.lsg));
                squadsModelArrayList.add(new SquadsModel(R.drawable.empty_profile, "Deepak Hooda", "Price : ₹5.75 Cr", R.drawable.blankbg, "Bowling All-rounder", "Right-handed", "Right-arm off-spin  ", "Indian", R.color.lsg));
                squadsModelArrayList.add(new SquadsModel(R.drawable.dc11, "Marcus Stoinis", "Price : ₹9.2 Cr", R.drawable.plane, "All-rounder", "Right-handed", "  Right-arm medium-fast  ", "Australian", R.color.lsg));
                squadsModelArrayList.add(new SquadsModel(R.drawable.srh6, "Jason Holder", "Price : ₹8.75", R.drawable.plane, "All rounder", "Right-handed", "   Medium Sean   ", "West Indian", R.color.lsg));
                squadsModelArrayList.add(new SquadsModel(R.drawable.mi12, "Krunal Pandya", "Price : ₹8.25 Cr", R.drawable.blankbg, "All-rounder", "Left-handed", "Left-arm orthodox  ", "Indian", R.color.lsg));
                squadsModelArrayList.add(new SquadsModel(R.drawable.csk21, "Krishnappa Gowtham", "Price : ₹90 L", R.drawable.blankbg, "Batting Allrounder", "Right-handed", "Right-arm offbreak  ", "Indian", R.color.lsg));
                squadsModelArrayList.add(new SquadsModel(R.drawable.empty_profile, "Ayush Badoni", "Price : ₹20 L", R.drawable.blankbg, "Bowling Allrounder", "Right Handed Bat", "Right-arm offbreak", "Indian", R.color.lsg));
                squadsModelArrayList.add(new SquadsModel(R.drawable.empty_profile, "Karan Sharma", "Price : ₹20 L", R.drawable.blankbg, "Batsman", "Right Handed Bat", "Right-arm legbreak", "Indian", R.color.lsg));
                squadsModelArrayList.add(new SquadsModel(R.drawable.empty_profile, "Dushmanta Chameera", "Price : ₹2 Cr", R.drawable.blankbg, "Bowler", "Right Handed Bat", "Right-arm fast", "Indian", R.color.lsg));
                squadsModelArrayList.add(new SquadsModel(R.drawable.empty_profile, "Ravi Bishnoi", "Price : ₹4 Cr", R.drawable.blankbg, "Bowler", "Right-handed", "  Right-arm leg spin  ", "Indian", R.color.lsg));
                squadsModelArrayList.add(new SquadsModel(R.drawable.srh15, "Shahbaz Nadeem", "Price : ₹50 L", R.drawable.blankbg, "Bowler", "Right-handed", "  Left-arm orthodox spin  ", "Indian", R.color.lsg));
                squadsModelArrayList.add(new SquadsModel(R.drawable.empty_profile, "Mark Wood", "Price : ₹7.5 Cr", R.drawable.plane, "Bowler", "Right Handed Bat", "Right-arm fast", "England", R.color.lsg));
                squadsModelArrayList.add(new SquadsModel(R.drawable.dc5, "Avesh Khan", "Price : ₹10 Cr", R.drawable.blankbg, "Bowler", "Right-handed", "  Right-arm Medium Fast  ", "Indian", R.color.lsg));
                squadsModelArrayList.add(new SquadsModel(R.drawable.empty_profile, "Ankit Rajpoot ", "Price : ₹50 L", R.drawable.blankbg, "Bowler", "Right Handed Bat", "Right-arm fast-medium", "Indian", R.color.lsg));
                squadsModelArrayList.add(new SquadsModel(R.drawable.mi13, "Mohsin Khan", "Price : ₹20 L", R.drawable.blankbg, "Batsman", "Left-handed", "Left-arm medium-fast  ", "Indian", R.color.lsg));
                squadsModelArrayList.add(new SquadsModel(R.drawable.empty_profile, "Mayank Yadav", "Price : ₹20 L", R.drawable.blankbg, "Batsman", "Right hand bat", "Right arm medium fast", "Indian", R.color.lsg));

                break;
        }
    }

    @Override
    public void onBackPressed() {
        try {
            if (mInterstitialAd != null && !isActivityLeft) {
                mInterstitialAd.show(SquadsListActivity.this);
                MyApplication.appOpenManager.isAdShow = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        finish();
    }

    public void onPause() {
        super.onPause();
        this.isActivityLeft = true;
    }

    public void onResume() {
        super.onResume();
        this.isActivityLeft = false;
    }

    protected void onStop() {
        super.onStop();
        this.isActivityLeft = true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        isActivityLeft = true;
    }

    public void LoadAdInterstitial() {
        AdRequest adRequest = new AdRequest.Builder().build();

        InterstitialAd.load(this, MyApplication.get_Admob_interstitial_Id(), adRequest, new InterstitialAdLoadCallback() {
            @Override
            public void onAdLoaded(@NonNull InterstitialAd interstitialAd) {
                mInterstitialAd = interstitialAd;
                mInterstitialAd.setFullScreenContentCallback(new FullScreenContentCallback() {
                    @Override
                    public void onAdDismissedFullScreenContent() {
                        MyApplication.appOpenManager.isAdShow = false;
                        mInterstitialAd = null;
                    }

                    @Override
                    public void onAdFailedToShowFullScreenContent(@NonNull AdError adError) {
                        MyApplication.appOpenManager.isAdShow = false;
                    }

                    @Override
                    public void onAdShowedFullScreenContent() {
                        mInterstitialAd = null;
                        MyApplication.appOpenManager.isAdShow = true;
                    }
                });
            }

            @Override
            public void onAdFailedToLoad(@NonNull LoadAdError loadAdError) {
                mInterstitialAd = null;
                MyApplication.appOpenManager.isAdShow = false;

            }
        });
    }

    private void GetData() {
        try {
            if (NetworkCheck.isNetworkAvailable(SquadsListActivity.this)) {
                if (retrofit == null) {
                    retrofit = new Retrofit.Builder().baseUrl("https://smartadz.in/api/").client(new OkHttpClient.Builder().addInterceptor(chain -> chain.proceed(chain.request().newBuilder().build())).connectTimeout(100, TimeUnit.SECONDS).readTimeout(100, TimeUnit.SECONDS).build()).addConverterFactory(GsonConverterFactory.create()).build();
                }
                APIService nativeClient = retrofit.create(APIService.class);
                nativeClient.GetData().enqueue(new Callback<Playdata>() {
                    @Override
                    public void onResponse(@NotNull Call<Playdata> call, @NotNull retrofit2.Response<Playdata> response) {
                        try {
                            if (response.body() != null) {
                                if (response.body().isStatus()) {
                                    if (response.body().getData().isFlage()) {
                                        qurekaLink = response.body().getData().getUrl();
                                        findViewById(R.id.qurekaads).setVisibility(View.VISIBLE);
                                        Glide.with(SquadsListActivity.this).load(response.body().getData().getImage()).listener(new RequestListener<Drawable>() {
                                            @Override
                                            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                                return false;
                                            }

                                            @Override
                                            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                                return false;
                                            }
                                        }).into((ImageView) findViewById(R.id.img));
                                    }
                                }
                            }

                        } catch (Exception exception) {
                            exception.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<Playdata> call, @NonNull Throwable t) {
                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}