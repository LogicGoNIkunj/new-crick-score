package com.ltos.livecrkttv.activity;

import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.browser.customtabs.CustomTabsIntent;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.ltos.livecrkttv.R;
import com.ltos.livecrkttv.fragments.LiveMatchFragment;
import com.ltos.livecrkttv.fragments.RecentFragment;
import com.ltos.livecrkttv.fragments.UpcomingFragment;
import com.ltos.livecrkttv.interfaces.APIService;
import com.ltos.livecrkttv.model.Playdata;
import com.ltos.livecrkttv.utils.CustomTabActivityHelper;
import com.ltos.livecrkttv.utils.MyApplication;
import com.ltos.livecrkttv.utils.NetworkCheck;
import com.ltos.livecrkttv.utils.WebviewFallback;
import com.google.android.gms.ads.AdError;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.FullScreenContentCallback;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.interstitial.InterstitialAd;
import com.google.android.gms.ads.interstitial.InterstitialAdLoadCallback;
import com.google.android.material.tabs.TabLayout;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class LiveCricketActivity extends AppCompatActivity {
    ViewPager vp;
    TabLayout tab;
    private Retrofit retrofit;
    String qurekaLink = "";
    public static int click;
    InterstitialAd mInterstitialAd;
    boolean isActivityLeft;
    TextView ads_space;
    FrameLayout adContainerView;
    AdView adView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_livecricket);
        isActivityLeft = false;
        LoadAdInterstitial();
        Analytics();
        initids();
        Banner();
        setupViewPager(vp);
        GetData();

        findViewById(R.id.qureka).setOnClickListener(view -> {
            MyApplication.appOpenManager.isAdShow = true;
            if (!qurekaLink.equals("")) {
                try {
                    CustomTabsIntent customTabsIntent = new CustomTabsIntent.Builder().build();
                    CustomTabActivityHelper.openCustomTab(LiveCricketActivity.this, customTabsIntent, Uri.parse(qurekaLink), new WebviewFallback());
                } catch (Exception e) {
                    CustomTabsIntent.Builder builder = new CustomTabsIntent.Builder();
                    CustomTabsIntent customTabsIntent = builder.build();
                    customTabsIntent.launchUrl(LiveCricketActivity.this, Uri.parse(qurekaLink));
                }
            }
        });

        findViewById(R.id.back_btn).setOnClickListener(view -> onBackPressed());
    }

    private void initids() {
        vp = findViewById(R.id.vp);
        tab = findViewById(R.id.tab);
        ads_space = findViewById(R.id.bottom_space);
        adContainerView = findViewById(R.id.ad_view_container);
    }


    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new LiveMatchFragment(), "Live");
        adapter.addFragment(new UpcomingFragment(), "Upcoming");
        adapter.addFragment(new RecentFragment(), "Recent");
        viewPager.setAdapter(adapter);
        int limit = (adapter.getCount() > 1 ? adapter.getCount() - 1 : 1);
        viewPager.setOffscreenPageLimit(limit);
        tab.setupWithViewPager(viewPager);
    }

    public static class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @NonNull
        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }


    private void Analytics() {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }
    public void Banner() {
        adView = new AdView(this);
        adView.setAdUnitId(MyApplication.get_Admob_banner_Id());
        adContainerView.addView(adView);
        loadBanner();
    }

    private void loadBanner() {
        AdRequest adRequest =
                new AdRequest.Builder().build();

        AdSize adSize = getAdSize();
        adView.setAdSize(adSize);
        adView.setAdListener(new AdListener() {
            @Override
            public void onAdFailedToLoad(@NonNull LoadAdError loadAdError) {
                super.onAdFailedToLoad(loadAdError);
                ads_space.setVisibility(View.GONE);
            }

            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
                ads_space.setVisibility(View.GONE);
            }
        });
        adView.loadAd(adRequest);
    }

    private AdSize getAdSize() {
        Display display = getWindowManager().getDefaultDisplay();
        DisplayMetrics outMetrics = new DisplayMetrics();
        display.getMetrics(outMetrics);

        float widthPixels = outMetrics.widthPixels;
        float density = outMetrics.density;

        int adWidth = (int) (widthPixels / density);
        return AdSize.getCurrentOrientationAnchoredAdaptiveBannerAdSize(this, adWidth);
    }

    private void GetData() {
        try {
            if (NetworkCheck.isNetworkAvailable(LiveCricketActivity.this)) {
                if (retrofit == null) {
                    retrofit = new Retrofit.Builder().baseUrl("https://smartadz.in/api/").client(new OkHttpClient.Builder().addInterceptor(chain -> chain.proceed(chain.request().newBuilder().build())).connectTimeout(100, TimeUnit.SECONDS).readTimeout(100, TimeUnit.SECONDS).build()).addConverterFactory(GsonConverterFactory.create()).build();
                }
                APIService nativeClient = retrofit.create(APIService.class);
                nativeClient.GetData().enqueue(new Callback<Playdata>() {
                    @Override
                    public void onResponse(@NotNull Call<Playdata> call, @NotNull retrofit2.Response<Playdata> response) {
                        try {
                            if (response.body() != null) {
                                if (response.body().isStatus()) {
                                    if (response.body().getData().isFlage()) {
                                        qurekaLink = response.body().getData().getUrl();
                                        findViewById(R.id.qurekaads).setVisibility(View.VISIBLE);
                                        Glide.with(LiveCricketActivity.this).load(response.body().getData().getImage()).listener(new RequestListener<Drawable>() {
                                            @Override
                                            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                                return false;
                                            }

                                            @Override
                                            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                                return false;
                                            }
                                        }).into((ImageView) findViewById(R.id.img));
                                    }
                                }
                            }

                        } catch (Exception exception) {
                            exception.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<Playdata> call, @NonNull Throwable t) {
                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onBackPressed() {
        try {
            if (mInterstitialAd != null && !isActivityLeft) {
                mInterstitialAd.show(LiveCricketActivity.this);
                MyApplication.appOpenManager.isAdShow = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        finish();
    }

    public void onPause() {
        super.onPause();
        this.isActivityLeft = true;
    }

    public void onResume() {
        super.onResume();
        this.isActivityLeft = false;
    }

    protected void onStop() {
        super.onStop();
        this.isActivityLeft = true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        isActivityLeft = true;
    }

    public void LoadAdInterstitial() {
        AdRequest adRequest = new AdRequest.Builder().build();

        InterstitialAd.load(this, MyApplication.get_Admob_interstitial_Id(), adRequest, new InterstitialAdLoadCallback() {
            @Override
            public void onAdLoaded(@NonNull InterstitialAd interstitialAd) {
                mInterstitialAd = interstitialAd;
                mInterstitialAd.setFullScreenContentCallback(new FullScreenContentCallback() {
                    @Override
                    public void onAdDismissedFullScreenContent() {
                        MyApplication.appOpenManager.isAdShow = false;
                        mInterstitialAd = null;
                    }

                    @Override
                    public void onAdFailedToShowFullScreenContent(@NonNull AdError adError) {
                        MyApplication.appOpenManager.isAdShow = false;
                    }

                    @Override
                    public void onAdShowedFullScreenContent() {
                        mInterstitialAd = null;
                        MyApplication.appOpenManager.isAdShow = true;
                    }
                });
            }

            @Override
            public void onAdFailedToLoad(@NonNull LoadAdError loadAdError) {
                mInterstitialAd = null;
                MyApplication.appOpenManager.isAdShow = false;

            }
        });
    }
}
