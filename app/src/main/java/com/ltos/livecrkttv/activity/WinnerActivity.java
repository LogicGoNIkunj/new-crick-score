package com.ltos.livecrkttv.activity;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.browser.customtabs.CustomTabsIntent;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.ltos.livecrkttv.R;
import com.ltos.livecrkttv.adapter.WinnerListAdapter;
import com.ltos.livecrkttv.interfaces.APIService;
import com.ltos.livecrkttv.model.Playdata;
import com.ltos.livecrkttv.model.WinnerListModel;
import com.ltos.livecrkttv.utils.CustomTabActivityHelper;
import com.ltos.livecrkttv.utils.MyApplication;
import com.ltos.livecrkttv.utils.NetworkCheck;
import com.ltos.livecrkttv.utils.WebviewFallback;
import com.google.android.gms.ads.AdError;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.FullScreenContentCallback;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.interstitial.InterstitialAd;
import com.google.android.gms.ads.interstitial.InterstitialAdLoadCallback;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class WinnerActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    final ArrayList<WinnerListModel> winnerListModelArrayList = new ArrayList<>();
    private Retrofit retrofit;
    String qurekaLink = "";
    InterstitialAd mInterstitialAd;
    boolean isActivityLeft;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_winner);
        getWindow().setBackgroundDrawable(null);
        isActivityLeft = false;
        LoadAdInterstitial();
        GetData();

        findViewById(R.id.back_btn).setOnClickListener(view -> onBackPressed());

        findViewById(R.id.qureka).setOnClickListener(view -> {
            MyApplication.appOpenManager.isAdShow = true;
            if (!qurekaLink.equals("")) {
                try {
                    CustomTabsIntent customTabsIntent = new CustomTabsIntent.Builder().build();
                    CustomTabActivityHelper.openCustomTab(WinnerActivity.this, customTabsIntent, Uri.parse(qurekaLink), new WebviewFallback());
                } catch (Exception e) {
                    CustomTabsIntent.Builder builder = new CustomTabsIntent.Builder();
                    CustomTabsIntent customTabsIntent = builder.build();
                    customTabsIntent.launchUrl(WinnerActivity.this, Uri.parse(qurekaLink));
                }
            }
        });

        recyclerView = findViewById(R.id.recyclerview);
        winnerListModelArrayList.add(new WinnerListModel(R.drawable.iplm21, "2021", "Chennai Super Kings", "Kolkata Knight Riders", "Ruturaj Gaikwad", "Harshal Patel(RCB)", "Faf du Plessis(CSK)", "Harshal Patel(RCB)",false));
        winnerListModelArrayList.add(new WinnerListModel(R.drawable.iplm20, "2020", "Mumbai Indians", "Delhi Capitals", "KL Rahul(KXIP)", "Kagiso Rabada(DC)", "Trent Boult(MI)", "Jofra Archer(RR)",false));
        winnerListModelArrayList.add(new WinnerListModel(0, "", "", "", "", "", "", "",true));
        winnerListModelArrayList.add(new WinnerListModel(R.drawable.champion2019, "2019", "Mumbai Indians", "Chennai Super Kings", "David Warner(SRH)", "Imran Tahir(CSK)", "Jasprit Bumrah(MI)", "Andre Russell(KKR)",false));
        winnerListModelArrayList.add(new WinnerListModel(R.drawable.champion2018, "2018", "Chennai Super Kings", "Sunrisers Hyderabad", "Kane Williamson(SRH)", "Andrew Tye(KXIP)", "Shane Watson(CSK)", "Sunil Narine(KKR)",false));
        winnerListModelArrayList.add(new WinnerListModel(0, "", "", "", "", "", "", "",true));
        winnerListModelArrayList.add(new WinnerListModel(R.drawable.champion2017, "2017", "Mumbai Indians", "Rising Pune Supergiants", "David Warner(SRH)", "Bhuvneswar Kumar(SRH)", "Krunal Pandya(MI)", "Ben Stokes(RPS)",false));
        winnerListModelArrayList.add(new WinnerListModel(R.drawable.champion2016, "2016", "Sunrisers Hyderabad", "Royal Challengers Bangalore", "Virat Kohli(RCB)", "Bhuvneswar Kumar(SRH)", "Ben Cutting(SRH)", "Virat Kohli(RCB)",false));
        winnerListModelArrayList.add(new WinnerListModel(0, "", "", "", "", "", "", "",true));
        winnerListModelArrayList.add(new WinnerListModel(R.drawable.champion2015, "2015", "Mumbai Indians", "Chennai Super Kings", "David Warner(SRH)", "Dwayne Bravo(CSK)", "Rohit Sharma(MI)", "Andre Russell(KKR)",false));
        winnerListModelArrayList.add(new WinnerListModel(R.drawable.champion2014, "2014", "Kolkata Knight Riders", "Kings XI Punjab", "Robin Uthappa(KKR)", "Mohit Sharma(CSK)", "Manish Pandey(KKR)", "Glenn Maxwell(KXIP)",false));
        winnerListModelArrayList.add(new WinnerListModel(0, "", "", "", "", "", "", "",true));
        winnerListModelArrayList.add(new WinnerListModel(R.drawable.champion2013, "2013", "Mumbai Indians", "Chennai Super Kings", "Michael Hussey(CSK)", "Dwayone Bravo(CSK)", "Kieron Pollard(MI)", "Shane Watson(RR)",false));
        winnerListModelArrayList.add(new WinnerListModel(R.drawable.champion2012, "2012", "Kolkata Knight Riders", "Chennai Super Kings", "Chris Gayle(RCB)", "Morne Morkel(DD)", "Manvinder Bisla(KKR)", "Sunil Narine(KKR)",false));
        winnerListModelArrayList.add(new WinnerListModel(0, "", "", "", "", "", "", "",true));
        winnerListModelArrayList.add(new WinnerListModel(R.drawable.champion2011, "2011", "Chennai Super Kings", "Royal Challengers Bangalore", "Chris Gayle(RCB)", "Lasith Malinga(MI)", "Murali Vijay(CSK)", "Chris Gayle(RCB)",false));
        winnerListModelArrayList.add(new WinnerListModel(R.drawable.champion2010, "2010", "Chennai Super Kings", "Mumbai Indians", "Sachin Tendulkar(MI)", "Pragyan Ojha(DC)", "Suresh Raina(CSK)", "Sachin Tendulkar(MI)",false));
        winnerListModelArrayList.add(new WinnerListModel(0, "", "", "", "", "", "", "",true));
        winnerListModelArrayList.add(new WinnerListModel(R.drawable.champion2009, "2009", "Deccan Chargers", "Royal Challengers Bangalore", "Matthew Hayden(CSK)", "R.P Sing(DC)", "Anil Kumble(RCB)", "Adam Gilchrist(DC)",false));
        winnerListModelArrayList.add(new WinnerListModel(R.drawable.champion2008, "2008", "Rajasthan Royals", "Chennai Super Kings", "Shaun Marsh(KXIP)", "Sohail Tanvir(RR)", "Yusuf Pathan(RR)", "Shane Watson(RR)",false));
        winnerListModelArrayList.add(new WinnerListModel(0, "", "", "", "", "", "", "",true));
        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 1);
        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerView.setAdapter(new WinnerListAdapter(WinnerActivity.this,winnerListModelArrayList));

    }




    @Override
    public void onBackPressed() {
        try {
            if (mInterstitialAd != null && !isActivityLeft) {
                mInterstitialAd.show(WinnerActivity.this);
                MyApplication.appOpenManager.isAdShow = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        finish();
    }

    public void onPause() {
        super.onPause();
        this.isActivityLeft = true;
    }

    public void onResume() {
        super.onResume();
        this.isActivityLeft = false;
    }

    protected void onStop() {
        super.onStop();
        this.isActivityLeft = true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        isActivityLeft = true;
    }
    public void LoadAdInterstitial() {
        AdRequest adRequest = new AdRequest.Builder().build();

        InterstitialAd.load(this, MyApplication.get_Admob_interstitial_Id(), adRequest, new InterstitialAdLoadCallback() {
            @Override
            public void onAdLoaded(@NonNull InterstitialAd interstitialAd) {
                mInterstitialAd = interstitialAd;
                mInterstitialAd.setFullScreenContentCallback(new FullScreenContentCallback() {
                    @Override
                    public void onAdDismissedFullScreenContent() {
                        MyApplication.appOpenManager.isAdShow = false;
                        mInterstitialAd = null;
                    }

                    @Override
                    public void onAdFailedToShowFullScreenContent(@NonNull AdError adError) {
                        MyApplication.appOpenManager.isAdShow = false;
                    }

                    @Override
                    public void onAdShowedFullScreenContent() {
                        mInterstitialAd = null;
                        MyApplication.appOpenManager.isAdShow = true;
                    }
                });
            }

            @Override
            public void onAdFailedToLoad(@NonNull LoadAdError loadAdError) {
                mInterstitialAd = null;
                MyApplication.appOpenManager.isAdShow = false;

            }
        });
    }

    private void GetData() {
        try {
            if (NetworkCheck.isNetworkAvailable(WinnerActivity.this)) {
                if (retrofit == null) {
                    retrofit = new Retrofit.Builder().baseUrl("https://smartadz.in/api/").client(new OkHttpClient.Builder().addInterceptor(chain -> chain.proceed(chain.request().newBuilder().build())).connectTimeout(100, TimeUnit.SECONDS).readTimeout(100, TimeUnit.SECONDS).build()).addConverterFactory(GsonConverterFactory.create()).build();
                }
                APIService nativeClient = retrofit.create(APIService.class);
                nativeClient.GetData().enqueue(new Callback<Playdata>() {
                    @Override
                    public void onResponse(@NotNull Call<Playdata> call, @NotNull retrofit2.Response<Playdata> response) {
                        try {
                            if (response.body() != null) {
                                if (response.body().isStatus()) {
                                    if (response.body().getData().isFlage()) {
                                        qurekaLink = response.body().getData().getUrl();
                                        findViewById(R.id.qurekaads).setVisibility(View.VISIBLE);
                                        Glide.with(WinnerActivity.this).load(response.body().getData().getImage()).listener(new RequestListener<Drawable>() {
                                            @Override
                                            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                                return false;
                                            }

                                            @Override
                                            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                                return false;
                                            }
                                        }).into((ImageView) findViewById(R.id.img));
                                    }
                                }
                            }

                        } catch (Exception exception) {
                            exception.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<Playdata> call, @NonNull Throwable t) {
                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}