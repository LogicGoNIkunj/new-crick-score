package com.ltos.livecrkttv.activity;


import android.app.ProgressDialog;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.ltos.livecrkttv.R;
import com.ltos.livecrkttv.admodels.APIInterFace;
import com.ltos.livecrkttv.admodels.ApiClient;
import com.ltos.livecrkttv.admodels.Feedback_model;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class FeedbackActivity extends AppCompatActivity {
    EditText TextDescription;
    EditText TextTitle;
    boolean isFatching = false;
    ProgressDialog dialog_ll;

    public static class ToastMessage {
        public static final String DESCRIPTION_TEXT_SET_PRE_CONDITION = "Please enter a description.";
        public static final String TITLE_TEXT_SET_PRE_CONDITION = "Please enter a title.";
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback);
        initUI();
        clickListener();
        removeFocus();
        closeKeyboard();
    }

    private void initUI() {
        TextTitle = findViewById(R.id.TextTitle);
        TextDescription = findViewById(R.id.TextDescription);
        findViewById(R.id.back_btn).setOnClickListener(v -> onBackPressed());
    }

    private void clickListener() {
        findViewById(R.id.SendEmail).setOnClickListener(v -> {
            closeKeyboard();

            if (TextTitle.getText().toString().equalsIgnoreCase("")) {
                Toast.makeText(FeedbackActivity.this, ToastMessage.TITLE_TEXT_SET_PRE_CONDITION, Toast.LENGTH_SHORT).show();
            } else if (TextDescription.getText().toString().equalsIgnoreCase("")) {
                Toast.makeText(FeedbackActivity.this, ToastMessage.DESCRIPTION_TEXT_SET_PRE_CONDITION, Toast.LENGTH_SHORT).show();
            } else {
                PackageInfo packageInfo = null;
                try {
                    packageInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
                } catch (PackageManager.NameNotFoundException e) {
                    e.printStackTrace();
                }
                assert packageInfo != null;
                String appverson = packageInfo.versionName;
                String appname = getResources().getString(R.string.app_name);
                int appversoncode = Build.VERSION.SDK_INT;
                String mobilemodel = Build.MODEL;
                String packagename = getPackageName();
                PostFeedback(appverson, appname, String.valueOf(appversoncode), mobilemodel, packagename, TextTitle.getText().toString(), TextDescription.getText().toString());
            }
        });

    }

    private void removeFocus() {
        getWindow().setSoftInputMode(3);
    }

    private void closeKeyboard() {
        try {
            View view = getCurrentFocus();
            if (view != null) {
                ((InputMethodManager) getSystemService(INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void PostFeedback(String appverson, String appname, String appversoncode, String mobilemodel, String packagename, String title, String description) {

        if (!isFatching) {
            dialog_ll = new ProgressDialog(this);
            dialog_ll.setMessage("Send Feedback...");
            dialog_ll.setCancelable(false);
            dialog_ll.show();
            isFatching = true;

            APIInterFace apiinterface = ApiClient.getClient().create(APIInterFace.class);
            apiinterface.sendfeedback(appname, packagename, title, description, mobilemodel, appversoncode, appverson).enqueue(new Callback<Feedback_model>() {
                @Override
                public void onResponse(@NonNull Call<Feedback_model> call, @NonNull Response<Feedback_model> response) {

                    if (response.code() == 200) {
                        assert response.body() != null;
                        isFatching = false;
                        if (response.body().getStatus()) {
                            Toast.makeText(FeedbackActivity.this, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();
                            TextDescription.setText("");
                            TextTitle.setText("");
                            dialog_ll.dismiss();
                        }
                    }

                }

                @Override
                public void onFailure(@NonNull Call<Feedback_model> call, @NonNull Throwable t) {
                    dialog_ll.dismiss();
                    Toast.makeText(FeedbackActivity.this, "Please send feedback in playstore", Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

}