package com.ltos.livecrkttv.activity;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.bumptech.glide.Glide;
import com.ltos.livecrkttv.R;
import com.ltos.livecrkttv.fragments.HighlightsFragment;
import com.ltos.livecrkttv.fragments.LiveInfoFragment;
import com.ltos.livecrkttv.fragments.LiveMatchFragment;
import com.ltos.livecrkttv.fragments.MatchInfoFragment;
import com.ltos.livecrkttv.fragments.OversInfoFragment;
import com.ltos.livecrkttv.fragments.RecentFragment;
import com.ltos.livecrkttv.fragments.ScorecardFragment;
import com.ltos.livecrkttv.fragments.UpcomingFragment;
import com.ltos.livecrkttv.model.MatchesModel;
import com.ltos.livecrkttv.utils.MyApplication;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.material.tabs.TabLayout;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.TimeZone;


public class MatchInfoActivity extends AppCompatActivity {
    ArrayList<MatchesModel.Datas> datasArrayList;
    int position;
    JSONObject homejsonObject;
    JSONObject awayjsonObject;
    JSONObject vanuejsonObject;
    JSONArray inningjsonArray;
    JSONObject compjsonObject;
    String matchdata, homecolor, awaycolor;
    RelativeLayout banner_container;
    String check_fragment = "";
    ImageView img_team1, img_team2;
    ConstraintLayout clupandrecent;
    RelativeLayout timeforupcoming;
    TextView tv_country_inning1, tv_inning1, tv_country_name, tv_inning2, tv_inning3, tv_inning4, tv_runrate, tv_rqrunrate, tv_result;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_match_details);
        transparentStatusAndNavigation();
        Toolbar toolbar = findViewById(R.id.toolbar);
        ViewPager viewPager = findViewById(R.id.viewpager);
        TabLayout tabLayout = findViewById(R.id.tabs);
        banner_container = findViewById(R.id.rl_ads);
        timeforupcoming = findViewById(R.id.timeforupcoming);
        clupandrecent = findViewById(R.id.clupandrecent);
        tv_country_inning1 = findViewById(R.id.tv_country_inning1);
        tv_inning1 = findViewById(R.id.tv_inning1);
        tv_country_name = findViewById(R.id.tv_country_name);
        tv_inning2 = findViewById(R.id.tv_inning2);
        tv_inning3 = findViewById(R.id.tv_inning3);
        tv_inning4 = findViewById(R.id.tv_inning4);
        tv_runrate = findViewById(R.id.tv_runrate);
        tv_rqrunrate = findViewById(R.id.tv_rqrunrate);
        tv_result = findViewById(R.id.tv_result);
        img_team2 = findViewById(R.id.img_team2);
        img_team1 = findViewById(R.id.img_team1);
        banner_container.setLayerType(View.LAYER_TYPE_SOFTWARE, null);

        try {
            datasArrayList = getIntent().getParcelableArrayListExtra("MATCHDETAILS");
            position = getIntent().getExtras().getInt("POSITION");
            matchdata = getIntent().getStringExtra("MATCHDATA");
            homecolor = getIntent().getStringExtra("homecolor");
            awaycolor = getIntent().getStringExtra("awaycolor");
            check_fragment = getIntent().getStringExtra("abc");

            homejsonObject = (new JSONObject(matchdata)).getJSONObject("HomeTeam");
            awayjsonObject = (new JSONObject(matchdata)).getJSONObject("AwayTeam");
            inningjsonArray = new JSONObject(matchdata).getJSONArray("Innings");
            if (check_fragment.equals("abc")) {
                JSONObject inObject1 = inningjsonArray.getJSONObject(0);
                if (inObject1.getString("BattingTeamId").equals(homejsonObject.getString("Id"))) {
                    GradientDrawable gd = new GradientDrawable(
                            GradientDrawable.Orientation.TOP_BOTTOM,
                            new int[]{Color.parseColor(homecolor), 0xC8000000});
                    gd.setCornerRadius(0f);
                    findViewById(R.id.appBarLayout).setBackgroundDrawable(gd);
                } else {
                    GradientDrawable gd = new GradientDrawable(
                            GradientDrawable.Orientation.TOP_BOTTOM,
                            new int[]{Color.parseColor(awaycolor), 0xC8000000});
                    gd.setCornerRadius(0f);
                    findViewById(R.id.appBarLayout).setBackgroundDrawable(gd);
                }
            } else if (check_fragment.equals("xyz")) {
                if (homejsonObject.getBoolean("IsMatchWinner")) {
                    GradientDrawable gd = new GradientDrawable(
                            GradientDrawable.Orientation.TOP_BOTTOM,
                            new int[]{Color.parseColor(homecolor), 0xC8000000});
                    gd.setCornerRadius(0f);
                    findViewById(R.id.appBarLayout).setBackgroundDrawable(gd);
                } else if (awayjsonObject.getBoolean("IsMatchWinner")) {
                    GradientDrawable gd = new GradientDrawable(
                            GradientDrawable.Orientation.TOP_BOTTOM,
                            new int[]{Color.parseColor(awaycolor), 0xC8000000});
                    gd.setCornerRadius(0f);
                    findViewById(R.id.appBarLayout).setBackgroundDrawable(gd);
                }
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }

        try {
            toolbar.setTitle("" + homejsonObject.getString("ShortName") +
                    " v " + awayjsonObject.getString("ShortName"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        try {
            compjsonObject = (new JSONObject(matchdata)).getJSONObject("Competition");
            homejsonObject = (new JSONObject(matchdata)).getJSONObject("HomeTeam");
            awayjsonObject = (new JSONObject(matchdata)).getJSONObject("AwayTeam");
            vanuejsonObject = (new JSONObject(matchdata)).getJSONObject("Venue");


            Glide.with(this).load(compjsonObject.getString("ImageUrl")).into((ImageView) findViewById(R.id.imgtheme));
            if (check_fragment.equals("abc")) {
                clupandrecent.setVisibility(View.VISIBLE);
                try {
                    tv_country_inning1.setText(LiveMatchFragment.arrayList.get(position).getHomeTeam().getShortName());
                    Glide.with(this)
                            .load(LiveMatchFragment.arrayList.get(position).getHomeTeam().getLogoUrl())
                            .into(img_team1);

                    Glide.with(this)
                            .load(LiveMatchFragment.arrayList.get(position).getAwayTeam().getLogoUrl())
                            .into(img_team2);

                    if (LiveMatchFragment.arrayList.get(position).getInnings().size() == 1) {
                        if (LiveMatchFragment.arrayList.get(position).getInnings().get(0).getBattingTeamId() == LiveMatchFragment.arrayList.get(position).getHomeTeam().getId()) {
                            tv_inning1.setText(String.format("%s-%s", LiveMatchFragment.arrayList.get(position).getInnings().get(0).getRunsScored(), LiveMatchFragment.arrayList.get(position).getInnings().get(0).getNumberOfWicketsFallen()));
                        } else if (LiveMatchFragment.arrayList.get(position).getInnings().get(0).getBattingTeamId() == LiveMatchFragment.arrayList.get(position).getAwayTeam().getId()) {
                            tv_inning2.setText(String.format("%s-%s", LiveMatchFragment.arrayList.get(position).getInnings().get(0).getRunsScored(), LiveMatchFragment.arrayList.get(position).getInnings().get(0).getNumberOfWicketsFallen()));
                        }
                    } else if (LiveMatchFragment.arrayList.get(position).getInnings().size() == 2) {
                        if (LiveMatchFragment.arrayList.get(position).getInnings().get(0).getBattingTeamId() == LiveMatchFragment.arrayList.get(position).getHomeTeam().getId()) {
                            tv_inning1.setText(String.format("%s-%s", LiveMatchFragment.arrayList.get(position).getInnings().get(0).getRunsScored(), LiveMatchFragment.arrayList.get(position).getInnings().get(0).getNumberOfWicketsFallen()));
                        } else if (LiveMatchFragment.arrayList.get(position).getInnings().get(0).getBattingTeamId() == LiveMatchFragment.arrayList.get(position).getAwayTeam().getId()) {
                            tv_inning2.setText(String.format("%s-%s", LiveMatchFragment.arrayList.get(position).getInnings().get(0).getRunsScored(), LiveMatchFragment.arrayList.get(position).getInnings().get(0).getNumberOfWicketsFallen()));
                        }
                        if (LiveMatchFragment.arrayList.get(position).getInnings().get(1).getBattingTeamId() == LiveMatchFragment.arrayList.get(position).getHomeTeam().getId()) {
                            tv_inning1.setText(String.format("%s-%s", LiveMatchFragment.arrayList.get(position).getInnings().get(1).getRunsScored(), LiveMatchFragment.arrayList.get(position).getInnings().get(1).getNumberOfWicketsFallen()));
                        } else if (LiveMatchFragment.arrayList.get(position).getInnings().get(1).getBattingTeamId() == LiveMatchFragment.arrayList.get(position).getAwayTeam().getId()) {
                            tv_inning2.setText(String.format("%s-%s", LiveMatchFragment.arrayList.get(position).getInnings().get(1).getRunsScored(), LiveMatchFragment.arrayList.get(position).getInnings().get(1).getNumberOfWicketsFallen()));
                        }
                    } else if (LiveMatchFragment.arrayList.get(position).getInnings().size() == 3) {
                        if (LiveMatchFragment.arrayList.get(position).getInnings().get(0).getBattingTeamId() == LiveMatchFragment.arrayList.get(position).getHomeTeam().getId()) {
                            tv_inning1.setText(String.format("%s-%s", LiveMatchFragment.arrayList.get(position).getInnings().get(0).getRunsScored(), LiveMatchFragment.arrayList.get(position).getInnings().get(0).getNumberOfWicketsFallen()));
                        } else if (LiveMatchFragment.arrayList.get(position).getInnings().get(0).getBattingTeamId() == LiveMatchFragment.arrayList.get(position).getAwayTeam().getId()) {
                            tv_inning2.setText(String.format("%s-%s", LiveMatchFragment.arrayList.get(position).getInnings().get(0).getRunsScored(), LiveMatchFragment.arrayList.get(position).getInnings().get(0).getNumberOfWicketsFallen()));
                        }
                        if (LiveMatchFragment.arrayList.get(position).getInnings().get(1).getBattingTeamId() == LiveMatchFragment.arrayList.get(position).getHomeTeam().getId()) {
                            tv_inning1.setText(String.format("%s-%s", LiveMatchFragment.arrayList.get(position).getInnings().get(1).getRunsScored(), LiveMatchFragment.arrayList.get(position).getInnings().get(1).getNumberOfWicketsFallen()));
                        } else if (LiveMatchFragment.arrayList.get(position).getInnings().get(1).getBattingTeamId() == LiveMatchFragment.arrayList.get(position).getAwayTeam().getId()) {
                            tv_inning2.setText(String.format("%s-%s", LiveMatchFragment.arrayList.get(position).getInnings().get(1).getRunsScored(), LiveMatchFragment.arrayList.get(position).getInnings().get(1).getNumberOfWicketsFallen()));
                        }
                        if (LiveMatchFragment.arrayList.get(position).getInnings().get(2).getBattingTeamId() == LiveMatchFragment.arrayList.get(position).getHomeTeam().getId()) {
                            tv_inning3.setVisibility(View.VISIBLE);
                            tv_inning3.setText(String.format(" & %s-%s", LiveMatchFragment.arrayList.get(position).getInnings().get(2).getRunsScored(), LiveMatchFragment.arrayList.get(position).getInnings().get(2).getNumberOfWicketsFallen()));
                        } else if (LiveMatchFragment.arrayList.get(position).getInnings().get(2).getBattingTeamId() == LiveMatchFragment.arrayList.get(position).getAwayTeam().getId()) {
                            tv_inning4.setVisibility(View.VISIBLE);
                            tv_inning4.setText(String.format(" & %s-%s", LiveMatchFragment.arrayList.get(position).getInnings().get(2).getRunsScored(), LiveMatchFragment.arrayList.get(position).getInnings().get(2).getNumberOfWicketsFallen()));
                        }
                    } else if (LiveMatchFragment.arrayList.get(position).getInnings().size() == 4) {
                        if (LiveMatchFragment.arrayList.get(position).getInnings().get(0).getBattingTeamId() == LiveMatchFragment.arrayList.get(position).getHomeTeam().getId()) {
                            tv_inning1.setText(String.format("%s-%s", LiveMatchFragment.arrayList.get(position).getInnings().get(0).getRunsScored(), LiveMatchFragment.arrayList.get(position).getInnings().get(0).getNumberOfWicketsFallen()));
                        } else if (LiveMatchFragment.arrayList.get(position).getInnings().get(0).getBattingTeamId() == LiveMatchFragment.arrayList.get(position).getAwayTeam().getId()) {
                            tv_inning2.setText(String.format("%s-%s", LiveMatchFragment.arrayList.get(position).getInnings().get(0).getRunsScored(), LiveMatchFragment.arrayList.get(position).getInnings().get(0).getNumberOfWicketsFallen()));
                        }
                        if (LiveMatchFragment.arrayList.get(position).getInnings().get(1).getBattingTeamId() == LiveMatchFragment.arrayList.get(position).getHomeTeam().getId()) {
                            tv_inning1.setText(String.format("%s-%s", LiveMatchFragment.arrayList.get(position).getInnings().get(1).getRunsScored(), LiveMatchFragment.arrayList.get(position).getInnings().get(1).getNumberOfWicketsFallen()));
                        } else if (LiveMatchFragment.arrayList.get(position).getInnings().get(1).getBattingTeamId() == LiveMatchFragment.arrayList.get(position).getAwayTeam().getId()) {
                            tv_inning2.setText(String.format("%s-%s", LiveMatchFragment.arrayList.get(position).getInnings().get(1).getRunsScored(), LiveMatchFragment.arrayList.get(position).getInnings().get(1).getNumberOfWicketsFallen()));
                        }
                        if (LiveMatchFragment.arrayList.get(position).getInnings().get(2).getBattingTeamId() == LiveMatchFragment.arrayList.get(position).getHomeTeam().getId()) {
                            tv_inning3.setVisibility(View.VISIBLE);
                            tv_inning3.setText(String.format(" & %s-%s", LiveMatchFragment.arrayList.get(position).getInnings().get(2).getRunsScored(), LiveMatchFragment.arrayList.get(position).getInnings().get(2).getNumberOfWicketsFallen()));
                        } else if (LiveMatchFragment.arrayList.get(position).getInnings().get(2).getBattingTeamId() == LiveMatchFragment.arrayList.get(position).getAwayTeam().getId()) {
                            tv_inning4.setVisibility(View.VISIBLE);
                            tv_inning4.setText(String.format(" & %s-%s", LiveMatchFragment.arrayList.get(position).getInnings().get(2).getRunsScored(), LiveMatchFragment.arrayList.get(position).getInnings().get(2).getNumberOfWicketsFallen()));
                        }
                        if (LiveMatchFragment.arrayList.get(position).getInnings().get(3).getBattingTeamId() == LiveMatchFragment.arrayList.get(position).getHomeTeam().getId()) {
                            tv_inning3.setVisibility(View.VISIBLE);
                            tv_inning3.setText(String.format(" & %s-%s", LiveMatchFragment.arrayList.get(position).getInnings().get(3).getRunsScored(), LiveMatchFragment.arrayList.get(position).getInnings().get(3).getNumberOfWicketsFallen()));
                        } else if (LiveMatchFragment.arrayList.get(position).getInnings().get(3).getBattingTeamId() == LiveMatchFragment.arrayList.get(position).getAwayTeam().getId()) {
                            tv_inning4.setVisibility(View.VISIBLE);
                            tv_inning4.setText(String.format(" & %s-%s", LiveMatchFragment.arrayList.get(position).getInnings().get(3).getRunsScored(), LiveMatchFragment.arrayList.get(position).getInnings().get(3).getNumberOfWicketsFallen()));
                        }
                    }
                    tv_country_name.setText(LiveMatchFragment.arrayList.get(position).getAwayTeam().getShortName());
                    tv_result.setText(LiveMatchFragment.arrayList.get(position).getResultText());
                } catch (Exception exception) {
                    exception.printStackTrace();
                }

            } else if (check_fragment.equals("xyz")) {
                clupandrecent.setVisibility(View.VISIBLE);
                try {
                    tv_country_inning1.setText(RecentFragment.arrayList.get(position).getHomeTeam().getShortName());
                    Glide.with(this)
                            .load(RecentFragment.arrayList.get(position).getHomeTeam().getLogoUrl())
                            .into(img_team1);


                    Glide.with(this)
                            .load(RecentFragment.arrayList.get(position).getAwayTeam().getLogoUrl())
                            .into(img_team2);

                    if (RecentFragment.arrayList.get(position).getInnings().size() == 1) {
                        if (RecentFragment.arrayList.get(position).getInnings().get(0).getBattingTeamId() == RecentFragment.arrayList.get(position).getHomeTeam().getId()) {
                            tv_inning1.setText(String.format("%s-%s", RecentFragment.arrayList.get(position).getInnings().get(0).getRunsScored(), RecentFragment.arrayList.get(position).getInnings().get(0).getNumberOfWicketsFallen()));
                        } else if (RecentFragment.arrayList.get(position).getInnings().get(0).getBattingTeamId() == RecentFragment.arrayList.get(position).getAwayTeam().getId()) {
                            tv_inning2.setText(String.format("%s-%s", RecentFragment.arrayList.get(position).getInnings().get(0).getRunsScored(), RecentFragment.arrayList.get(position).getInnings().get(0).getNumberOfWicketsFallen()));
                        }
                    } else if (RecentFragment.arrayList.get(position).getInnings().size() == 2) {
                        if (RecentFragment.arrayList.get(position).getInnings().get(0).getBattingTeamId() == RecentFragment.arrayList.get(position).getHomeTeam().getId()) {
                            tv_inning1.setText(String.format("%s-%s", RecentFragment.arrayList.get(position).getInnings().get(0).getRunsScored(), RecentFragment.arrayList.get(position).getInnings().get(0).getNumberOfWicketsFallen()));
                        } else if (RecentFragment.arrayList.get(position).getInnings().get(0).getBattingTeamId() == RecentFragment.arrayList.get(position).getAwayTeam().getId()) {
                            tv_inning2.setText(String.format("%s-%s", RecentFragment.arrayList.get(position).getInnings().get(0).getRunsScored(), RecentFragment.arrayList.get(position).getInnings().get(0).getNumberOfWicketsFallen()));
                        }
                        if (RecentFragment.arrayList.get(position).getInnings().get(1).getBattingTeamId() == RecentFragment.arrayList.get(position).getHomeTeam().getId()) {
                            tv_inning1.setText(String.format("%s-%s", RecentFragment.arrayList.get(position).getInnings().get(1).getRunsScored(), RecentFragment.arrayList.get(position).getInnings().get(1).getNumberOfWicketsFallen()));
                        } else if (RecentFragment.arrayList.get(position).getInnings().get(1).getBattingTeamId() == RecentFragment.arrayList.get(position).getAwayTeam().getId()) {
                            tv_inning2.setText(String.format("%s-%s", RecentFragment.arrayList.get(position).getInnings().get(1).getRunsScored(), RecentFragment.arrayList.get(position).getInnings().get(1).getNumberOfWicketsFallen()));
                        }
                    } else if (RecentFragment.arrayList.get(position).getInnings().size() == 3) {
                        if (RecentFragment.arrayList.get(position).getInnings().get(0).getBattingTeamId() == RecentFragment.arrayList.get(position).getHomeTeam().getId()) {
                            tv_inning1.setText(String.format("%s-%s", RecentFragment.arrayList.get(position).getInnings().get(0).getRunsScored(), RecentFragment.arrayList.get(position).getInnings().get(0).getNumberOfWicketsFallen()));
                        } else if (RecentFragment.arrayList.get(position).getInnings().get(0).getBattingTeamId() == RecentFragment.arrayList.get(position).getAwayTeam().getId()) {
                            tv_inning2.setText(String.format("%s-%s", RecentFragment.arrayList.get(position).getInnings().get(0).getRunsScored(), RecentFragment.arrayList.get(position).getInnings().get(0).getNumberOfWicketsFallen()));
                        }
                        if (RecentFragment.arrayList.get(position).getInnings().get(1).getBattingTeamId() == RecentFragment.arrayList.get(position).getHomeTeam().getId()) {
                            tv_inning1.setText(String.format("%s-%s", RecentFragment.arrayList.get(position).getInnings().get(1).getRunsScored(), RecentFragment.arrayList.get(position).getInnings().get(1).getNumberOfWicketsFallen()));
                        } else if (RecentFragment.arrayList.get(position).getInnings().get(1).getBattingTeamId() == RecentFragment.arrayList.get(position).getAwayTeam().getId()) {
                            tv_inning2.setText(String.format("%s-%s", RecentFragment.arrayList.get(position).getInnings().get(1).getRunsScored(), RecentFragment.arrayList.get(position).getInnings().get(1).getNumberOfWicketsFallen()));
                        }
                        if (RecentFragment.arrayList.get(position).getInnings().get(2).getBattingTeamId() == RecentFragment.arrayList.get(position).getHomeTeam().getId()) {
                            tv_inning3.setVisibility(View.VISIBLE);
                            tv_inning3.setText(String.format(" & %s-%s", RecentFragment.arrayList.get(position).getInnings().get(2).getRunsScored(), RecentFragment.arrayList.get(position).getInnings().get(2).getNumberOfWicketsFallen()));
                        } else if (RecentFragment.arrayList.get(position).getInnings().get(2).getBattingTeamId() == RecentFragment.arrayList.get(position).getAwayTeam().getId()) {
                            tv_inning4.setVisibility(View.VISIBLE);
                            tv_inning4.setText(String.format(" & %s-%s", RecentFragment.arrayList.get(position).getInnings().get(2).getRunsScored(), RecentFragment.arrayList.get(position).getInnings().get(2).getNumberOfWicketsFallen()));
                        }
                    } else if (RecentFragment.arrayList.get(position).getInnings().size() == 4) {
                        if (RecentFragment.arrayList.get(position).getInnings().get(0).getBattingTeamId() == RecentFragment.arrayList.get(position).getHomeTeam().getId()) {
                            tv_inning1.setText(String.format("%s-%s", RecentFragment.arrayList.get(position).getInnings().get(0).getRunsScored(), RecentFragment.arrayList.get(position).getInnings().get(0).getNumberOfWicketsFallen()));
                        } else if (RecentFragment.arrayList.get(position).getInnings().get(0).getBattingTeamId() == RecentFragment.arrayList.get(position).getAwayTeam().getId()) {
                            tv_inning2.setText(String.format("%s-%s", RecentFragment.arrayList.get(position).getInnings().get(0).getRunsScored(), RecentFragment.arrayList.get(position).getInnings().get(0).getNumberOfWicketsFallen()));
                        }
                        if (RecentFragment.arrayList.get(position).getInnings().get(1).getBattingTeamId() == RecentFragment.arrayList.get(position).getHomeTeam().getId()) {
                            tv_inning1.setText(String.format("%s-%s", RecentFragment.arrayList.get(position).getInnings().get(1).getRunsScored(), RecentFragment.arrayList.get(position).getInnings().get(1).getNumberOfWicketsFallen()));
                        } else if (RecentFragment.arrayList.get(position).getInnings().get(1).getBattingTeamId() == RecentFragment.arrayList.get(position).getAwayTeam().getId()) {
                            tv_inning2.setText(String.format("%s-%s", RecentFragment.arrayList.get(position).getInnings().get(1).getRunsScored(), RecentFragment.arrayList.get(position).getInnings().get(1).getNumberOfWicketsFallen()));
                        }
                        if (RecentFragment.arrayList.get(position).getInnings().get(2).getBattingTeamId() == RecentFragment.arrayList.get(position).getHomeTeam().getId()) {
                            tv_inning3.setVisibility(View.VISIBLE);
                            tv_inning3.setText(String.format(" & %s-%s", RecentFragment.arrayList.get(position).getInnings().get(2).getRunsScored(), RecentFragment.arrayList.get(position).getInnings().get(2).getNumberOfWicketsFallen()));
                        } else if (RecentFragment.arrayList.get(position).getInnings().get(2).getBattingTeamId() == RecentFragment.arrayList.get(position).getAwayTeam().getId()) {
                            tv_inning4.setVisibility(View.VISIBLE);
                            tv_inning4.setText(String.format(" & %s-%s", RecentFragment.arrayList.get(position).getInnings().get(2).getRunsScored(), RecentFragment.arrayList.get(position).getInnings().get(2).getNumberOfWicketsFallen()));
                        }
                        if (RecentFragment.arrayList.get(position).getInnings().get(3).getBattingTeamId() == RecentFragment.arrayList.get(position).getHomeTeam().getId()) {
                            tv_inning3.setVisibility(View.VISIBLE);
                            tv_inning3.setText(String.format(" & %s-%s", RecentFragment.arrayList.get(position).getInnings().get(3).getRunsScored(), RecentFragment.arrayList.get(position).getInnings().get(3).getNumberOfWicketsFallen()));
                        } else if (RecentFragment.arrayList.get(position).getInnings().get(3).getBattingTeamId() == RecentFragment.arrayList.get(position).getAwayTeam().getId()) {
                            tv_inning4.setVisibility(View.VISIBLE);
                            tv_inning4.setText(String.format(" & %s-%s", RecentFragment.arrayList.get(position).getInnings().get(3).getRunsScored(), RecentFragment.arrayList.get(position).getInnings().get(3).getNumberOfWicketsFallen()));
                        }
                    }
                    tv_country_name.setText(RecentFragment.arrayList.get(position).getAwayTeam().getShortName());
                    tv_result.setText(RecentFragment.arrayList.get(position).getResultText());
                } catch (Exception exception) {
                    exception.printStackTrace();
                }

            } else {
                clupandrecent.setVisibility(View.GONE);
                timeforupcoming.setVisibility(View.VISIBLE);
                @SuppressLint("SimpleDateFormat") SimpleDateFormat parser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
                parser.setTimeZone(TimeZone.getTimeZone("UTC"));
                Date parsed = parser.parse(UpcomingFragment.arrayList.get(position).getStartDateTime());

                String removeGMT = String.valueOf(parsed).replace("GMT+05:30 ", "");
                String removeYear = StringUtils.substringBeforeLast(removeGMT, " ");

                ((TextView) findViewById(R.id.date)).setText(StringUtils.substringBeforeLast(removeYear, " "));
                ((TextView) findViewById(R.id.time)).setText(Convert24to12(StringUtils.substringAfterLast(removeYear, " ")));

                ((TextView) findViewById(R.id.team1)).setText(UpcomingFragment.arrayList.get(position).getHomeTeam().getShortName());
                Glide.with(this)
                        .load(UpcomingFragment.arrayList.get(position).getHomeTeam().getLogoUrl())
                        .into((ImageView) findViewById(R.id.image_team1));


                Glide.with(this)
                        .load(UpcomingFragment.arrayList.get(position).getAwayTeam().getLogoUrl())
                        .into((ImageView) findViewById(R.id.image_team2));
                ((TextView) findViewById(R.id.team2)).setText(UpcomingFragment.arrayList.get(position).getAwayTeam().getShortName());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        new Handler().postDelayed(() -> {
            setupViewPager(viewPager);
            tabLayout.setupWithViewPager(viewPager);
            viewPager.setCurrentItem(1);
        }, 10);

        admob_Banner(MatchInfoActivity.this, banner_container);
    }

    public static String Convert24to12(String time) {
        String convertedTime = "";
        try {
            @SuppressLint("SimpleDateFormat") SimpleDateFormat displayFormat = new SimpleDateFormat("hh:mm a");
            @SuppressLint("SimpleDateFormat") SimpleDateFormat parseFormat = new SimpleDateFormat("HH:mm:ss");
            Date date = parseFormat.parse(time);
            if (date != null) {
                convertedTime = displayFormat.format(date);
            }
            System.out.println("convertedTime : " + convertedTime);
        } catch (final ParseException e) {
            e.printStackTrace();
        }
        return convertedTime;
        //Output will be 10:23 PM
    }


    private void transparentStatusAndNavigation() {
        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
        );
        setWindowFlag();
        getWindow().setStatusBarColor(Color.TRANSPARENT);
    }

    private void setWindowFlag() {
        Window win = getWindow();
        WindowManager.LayoutParams winParams = win.getAttributes();
        winParams.flags &= ~WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS;
        win.setAttributes(winParams);
    }


    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager(), matchdata, datasArrayList, position);
        adapter.addFragment(new MatchInfoFragment(), "Info");
        adapter.addFragment(new LiveInfoFragment(), "Live");
        adapter.addFragment(new ScorecardFragment(), "Scorecard");
        adapter.addFragment(new OversInfoFragment(), "Overs");
        adapter.addFragment(new HighlightsFragment(), "Highlights");
        viewPager.setAdapter(adapter);
        int limit = (adapter.getCount() > 1 ? adapter.getCount() - 1 : 1);
        viewPager.setOffscreenPageLimit(limit);
    }

    static class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();
        final ArrayList<MatchesModel.Datas> datasArrayList;
        final int position;
        final String matchdata;

        public ViewPagerAdapter(FragmentManager manager, String smatchdata, ArrayList<MatchesModel.Datas> datasArrayList, int position) {
            super(manager);
            this.matchdata = smatchdata;
            this.datasArrayList = datasArrayList;
            this.position = position;
        }

        @NonNull
        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
            if (fragment instanceof MatchInfoFragment) {
                MatchInfoFragment.getInstance(matchdata, datasArrayList, position);
            } else if (fragment instanceof LiveInfoFragment) {
                LiveInfoFragment.getInstance(matchdata, datasArrayList, position);
            } else if (fragment instanceof ScorecardFragment) {
                ScorecardFragment.getInstance(datasArrayList, position);
            } else if (fragment instanceof OversInfoFragment) {
                OversInfoFragment.getInstance(matchdata, datasArrayList, position);
            } else if (fragment instanceof HighlightsFragment) {
                HighlightsFragment.getInstance(matchdata, datasArrayList, position);
            }
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    public void admob_Banner(Activity activity, RelativeLayout adcontainer) {
        AdView adView = new AdView(activity);
        adView.setAdUnitId(MyApplication.get_Admob_banner_Id());
        adcontainer.addView(adView);
        loadBanner(activity, adView);
        adView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
            }

            @Override
            public void onAdFailedToLoad(@NonNull LoadAdError adError) {
            }

            @Override
            public void onAdOpened() {
            }

            @Override
            public void onAdClicked() {
            }

            @Override
            public void onAdClosed() {
            }
        });
    }

    private void loadBanner(Activity activity, AdView adView) {
        AdRequest adRequest = new AdRequest.Builder().build();
        AdSize adSize = getAdSize(activity);
        adView.setAdSize(adSize);
        adView.loadAd(adRequest);
    }

    private AdSize getAdSize(Activity activity) {
        Display display = activity.getWindowManager().getDefaultDisplay();
        DisplayMetrics outMetrics = new DisplayMetrics();
        display.getMetrics(outMetrics);
        float widthPixels = outMetrics.widthPixels;
        float density = outMetrics.density;
        int adWidth = (int) (widthPixels / density);
        return AdSize.getCurrentOrientationAnchoredAdaptiveBannerAdSize(activity, adWidth);
    }

}