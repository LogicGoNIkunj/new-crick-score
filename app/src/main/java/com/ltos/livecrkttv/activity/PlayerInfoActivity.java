package com.ltos.livecrkttv.activity;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.ltos.livecrkttv.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Objects;

import de.hdodenhof.circleimageview.CircleImageView;


public class PlayerInfoActivity extends AppCompatActivity {
    TextView player_name, country_name, birth_date, birth_place, tv_height, tv_role, batting_style,
            bowling_style, tv_bio, didyouKnow;
    CircleImageView profile_image;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_player_details);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        toolbar.setTitle("Player Details");

        profile_image = findViewById(R.id.profile_image);
        player_name = findViewById(R.id.player_name);
        country_name = findViewById(R.id.country_name);
        birth_date = findViewById(R.id.birth_date);
        birth_place = findViewById(R.id.birth_place);
        tv_height = findViewById(R.id.tv_height);
        tv_role = findViewById(R.id.tv_role);
        batting_style = findViewById(R.id.batting_style);
        bowling_style = findViewById(R.id.bowling_style);
        tv_bio = findViewById(R.id.tv_bio);
        didyouKnow = findViewById(R.id.didyouKnow);

        try {

            JSONArray jsonArray = new JSONObject(getIntent().getExtras().getString("MATCHDATA")).getJSONArray("players");
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                int playerid = jsonObject.getInt("id");
                if (playerid == getIntent().getExtras().getInt("PLAYER_ID")) {
                    player_name.setText(jsonObject.getString("displayName"));
                    birth_date.setText(jsonObject.getString("dob"));
                    birth_place.setText(jsonObject.getString("birthPlace"));
                    tv_height.setText(jsonObject.getString("height"));
                    tv_role.setText(jsonObject.getString("type"));

                    tv_bio.setText(jsonObject.getString("bio"));
                    didyouKnow.setText(jsonObject.getString("didyouKnow"));
                    Glide.with(PlayerInfoActivity.this)
                            .load(jsonObject.getString("imageUrl"))
                            .listener(new RequestListener<Drawable>() {
                                @Override
                                public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                    return false;
                                }

                                @Override
                                public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                    return false;
                                }
                            })
                            .error(R.drawable.manplaceholder)
                            .placeholder(R.drawable.manplaceholder)
                            .into(profile_image);
                    try {
                        batting_style.setText(jsonObject.getString("battingHandId"));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    try {
                        bowling_style.setText(jsonObject.getString("bowlingType"));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }


    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}