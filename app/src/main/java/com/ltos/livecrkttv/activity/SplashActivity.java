package com.ltos.livecrkttv.activity;

import android.os.Bundle;
import android.os.Handler;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.github.ybq.android.spinkit.sprite.Sprite;
import com.github.ybq.android.spinkit.style.ThreeBounce;
import com.ltos.livecrkttv.R;
import com.ltos.livecrkttv.admodels.APIInterFace;
import com.ltos.livecrkttv.admodels.AdsModel;
import com.ltos.livecrkttv.interfaces.APIService;
import com.ltos.livecrkttv.interfaces.BaseUrl;
import com.ltos.livecrkttv.model.UrlModel;
import com.ltos.livecrkttv.utils.MyApplication;
import com.google.android.gms.ads.MobileAds;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        MobileAds.initialize(this, initializationStatus -> {
        });
        getAdsData();
        getUrl();
        runOnUiThread(this::setUpAnimation);
        new Handler().postDelayed(() -> MyApplication.appOpenManager.showAdIfAvailable(true,this), 5000);

    }

    private void setUpAnimation() {
        RelativeLayout relativeLayout = new RelativeLayout(this);
        ProgressBar progressBar = new ProgressBar(this, null, 16842874);
        Sprite doubleBounce = new ThreeBounce();
        doubleBounce.setColor(getResources().getColor(R.color.black));
        progressBar.setIndeterminateDrawable(doubleBounce);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(100, 100);
        layoutParams.addRule(14);
        layoutParams.addRule(12);
        layoutParams.setMargins(0, 0, 0, 50);
        relativeLayout.addView(progressBar, layoutParams);
        setContentView(relativeLayout);
    }

    public void getUrl() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BaseUrl.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        APIService service = retrofit.create(APIService.class);
        Call<UrlModel> call = service.GetCricApi(MyApplication.MYSECRET);
        call.enqueue(new Callback<UrlModel>() {
            @Override
            public void onResponse(@NonNull Call<UrlModel> call, @NonNull Response<UrlModel> response) {
                if (response.code() == 200) {
                    if (response.body() != null) {
                        if (response.body().isStatus()) {
                            MyApplication.set_cricApi(response.body().getData().getUrl());
                            MyApplication.set_cricApipoint(response.body().getData().getUrl2());
                            MyApplication.set_cricApipoint2022(response.body().getData().getUrl3());
                        }
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<UrlModel> call, @NonNull Throwable t) {

            }
        });
    }

    public void getAdsData() {
        Retrofit.Builder retrofit = new Retrofit.Builder().baseUrl(APIInterFace.adsAPI).addConverterFactory(GsonConverterFactory.create());
        APIInterFace service = retrofit.build().create(APIInterFace.class);
        Call<AdsModel> call = service.getads(56);
        call.enqueue(new Callback<AdsModel>() {
            @Override
            public void onResponse(@NonNull Call<AdsModel> call, @NonNull Response<AdsModel> response) {
                if (response.code() == 200) {
                    if (response.body() != null) {
                        if (response.body().isStatus()) {
                            if (response.body().getData() != null) {
                                MyApplication.set_AdsInt(response.body().getData().getPublisher_id());
                                if (response.body().getData().getPublishers() != null) {
                                    if (MyApplication.get_AdsInt() == 1) {
                                        if (response.body().getData().getPublishers().getAdmob() != null) {
                                            if (response.body().getData().getPublishers().getAdmob().getAdmob_interstitial() != null) {
                                                MyApplication.set_Admob_interstitial_Id(response.body().getData().getPublishers().getAdmob().getAdmob_interstitial().getAdmob_interstitial_id());
                                            } else {
                                                MyApplication.set_Admob_interstitial_Id(getResources().getString(R.string.interestial_add));
                                            }
                                            if (response.body().getData().getPublishers().getAdmob().getAdmob_native() != null) {
                                                MyApplication.set_Admob_native_Id(response.body().getData().getPublishers().getAdmob().getAdmob_native().getAdmob_Native_id());
                                            } else {
                                                MyApplication.set_Admob_native_Id(getResources().getString(R.string.native_big_add));
                                            }
                                            if (response.body().getData().getPublishers().getAdmob().getAdmob_banner() != null) {
                                                MyApplication.set_Admob_banner_Id(response.body().getData().getPublishers().getAdmob().getAdmob_banner().getAdmob_banner_id());
                                            } else {
                                                MyApplication.set_Admob_banner_Id(getResources().getString(R.string.banner_id));
                                            }
                                            if (response.body().getData().getPublishers().getAdmob().getAdmob_open() != null) {
                                                MyApplication.set_Admob_openapp(response.body().getData().getPublishers().getAdmob().getAdmob_open().getAdmob_Open_id());
                                            } else {
                                                MyApplication.set_Admob_openapp(getResources().getString(R.string.open_app_ad_id));
                                            }

                                        } else {
                                            MyApplication.set_Admob_banner_Id(getResources().getString(R.string.banner_id));
                                            MyApplication.set_Admob_native_Id(getResources().getString(R.string.native_big_add));
                                            MyApplication.set_Admob_interstitial_Id(getResources().getString(R.string.interestial_add));
                                            MyApplication.set_Admob_openapp(getResources().getString(R.string.open_app_ad_id));
                                        }
                                    } else {
                                        MyApplication.set_Admob_banner_Id(getResources().getString(R.string.banner_id));
                                        MyApplication.set_Admob_native_Id(getResources().getString(R.string.native_big_add));
                                        MyApplication.set_Admob_interstitial_Id(getResources().getString(R.string.interestial_add));
                                        MyApplication.set_Admob_openapp(getResources().getString(R.string.open_app_ad_id));
                                    }
                                } else {
                                    MyApplication.set_Admob_banner_Id("1234");
                                    MyApplication.set_Admob_native_Id("1234");
                                    MyApplication.set_Admob_interstitial_Id("1234");
                                    MyApplication.set_Admob_openapp("1234");
                                }
                            } else {
                                MyApplication.set_Admob_banner_Id(getResources().getString(R.string.banner_id));
                                MyApplication.set_Admob_native_Id(getResources().getString(R.string.native_big_add));
                                MyApplication.set_Admob_interstitial_Id(getResources().getString(R.string.interestial_add));
                                MyApplication.set_Admob_openapp(getResources().getString(R.string.open_app_ad_id));
                            }
                        } else {
                            MyApplication.set_Admob_banner_Id(getResources().getString(R.string.banner_id));
                            MyApplication.set_Admob_native_Id(getResources().getString(R.string.native_big_add));
                            MyApplication.set_Admob_interstitial_Id(getResources().getString(R.string.interestial_add));
                            MyApplication.set_Admob_openapp(getResources().getString(R.string.open_app_ad_id));
                        }
                    } else {
                        MyApplication.set_Admob_banner_Id(getResources().getString(R.string.banner_id));
                        MyApplication.set_Admob_native_Id(getResources().getString(R.string.native_big_add));
                        MyApplication.set_Admob_interstitial_Id(getResources().getString(R.string.interestial_add));
                        MyApplication.set_Admob_openapp(getResources().getString(R.string.open_app_ad_id));
                    }
                } else {
                    MyApplication.set_Admob_banner_Id(getResources().getString(R.string.banner_id));
                    MyApplication.set_Admob_native_Id(getResources().getString(R.string.native_big_add));
                    MyApplication.set_Admob_interstitial_Id(getResources().getString(R.string.interestial_add));
                    MyApplication.set_Admob_openapp(getResources().getString(R.string.open_app_ad_id));
                }
            }

            @Override
            public void onFailure(@NonNull Call<AdsModel> call, @NonNull Throwable t) {
                MyApplication.set_Admob_banner_Id(getResources().getString(R.string.banner_id));
                MyApplication.set_Admob_native_Id(getResources().getString(R.string.native_big_add));
                MyApplication.set_Admob_interstitial_Id(getResources().getString(R.string.interestial_add));
                MyApplication.set_Admob_openapp(getResources().getString(R.string.open_app_ad_id));
            }
        });
    }


    protected void onStop() {
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        super.onStop();
    }



    @Override
    public void onBackPressed() {
    }
}