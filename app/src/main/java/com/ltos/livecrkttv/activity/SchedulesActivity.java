package com.ltos.livecrkttv.activity;

import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.browser.customtabs.CustomTabsIntent;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.ltos.livecrkttv.R;
import com.ltos.livecrkttv.adapter.ScheduleListAdapter;
import com.ltos.livecrkttv.interfaces.APIService;
import com.ltos.livecrkttv.model.Playdata;
import com.ltos.livecrkttv.model.ScheduleListModel;
import com.ltos.livecrkttv.utils.CustomTabActivityHelper;
import com.ltos.livecrkttv.utils.MyApplication;
import com.ltos.livecrkttv.utils.NetworkCheck;
import com.ltos.livecrkttv.utils.WebviewFallback;
import com.google.android.gms.ads.AdError;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.FullScreenContentCallback;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.interstitial.InterstitialAd;
import com.google.android.gms.ads.interstitial.InterstitialAdLoadCallback;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class SchedulesActivity extends AppCompatActivity {

    final ArrayList<ScheduleListModel> scheduleListModelArrayList = new ArrayList<>();
    RecyclerView recyclerView;
    private Retrofit retrofit;
    String qurekaLink = "";
    InterstitialAd mInterstitialAd;
    boolean isActivityLeft;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_schedules);
        getWindow().setBackgroundDrawable(null);
        isActivityLeft = false;
        LoadAdInterstitial();
        GetData();

        findViewById(R.id.back_btn).setOnClickListener(view -> onBackPressed());

        findViewById(R.id.qureka).setOnClickListener(view -> {
            MyApplication.appOpenManager.isAdShow = true;
            if (!qurekaLink.equals("")) {
                try {
                    CustomTabsIntent customTabsIntent = new CustomTabsIntent.Builder().build();
                    CustomTabActivityHelper.openCustomTab(SchedulesActivity.this, customTabsIntent, Uri.parse(qurekaLink), new WebviewFallback());
                } catch (Exception e) {
                    CustomTabsIntent.Builder builder = new CustomTabsIntent.Builder();
                    CustomTabsIntent customTabsIntent = builder.build();
                    customTabsIntent.launchUrl(SchedulesActivity.this, Uri.parse(qurekaLink));
                }
            }
        });
        recyclerView = findViewById(R.id.recyclerview);


        scheduleListModelArrayList.add(new ScheduleListModel("Chennai Super Kings vs Kolkata Knight Riders", "IPL 1st T20 Match", "26 March 2022 at 07:30 PM - Sat", "CSK", "KKR", R.drawable.cskicon, R.drawable.kkricon, "Wankhede Stadium, Mumbai",false));
        scheduleListModelArrayList.add(new ScheduleListModel("Delhi Capitals vs Mumbai Indians", "IPL 2nd T20 Match", "27 March 2022 at 03:30 PM - Sun", "DC", "MI", R.drawable.dcicon, R.drawable.micon, "Brabourne Stadium, Mumbai",false));
        scheduleListModelArrayList.add(new ScheduleListModel("", "", "", "", "", 0, 0, "",true));
        scheduleListModelArrayList.add(new ScheduleListModel("Punjab Kings vs Royal Challengers Bangalore", "IPL 3rd T20 Match", "27 March 2022 at 07:30 PM - Sun", "PBKS", "RCB", R.drawable.kxiiicon, R.drawable.rcbion, "Dr DY Patil Sports Academy, Mumbai",false));
        scheduleListModelArrayList.add(new ScheduleListModel("Gujarat Titans vs Lucknow Super Giants", "IPL 4th T20 Match", "28 March 2022 at 07:30 PM - Mon", "GT", "LSG", R.drawable.gticon, R.drawable.lsgicon, "Wankhede Stadium, Mumbai",false));
        scheduleListModelArrayList.add(new ScheduleListModel("", "", "", "", "", 0, 0, "",true));
        scheduleListModelArrayList.add(new ScheduleListModel("Sunrisers Hyderabad vs Rajasthan Royals", "IPL 5th T20 Match", "29 March 2022 at 07:30 PM - Tue", "SRH", "RR", R.drawable.srhicon, R.drawable.rricon, "Maharashtra Cricket Association Stadium, Pune",false));
        scheduleListModelArrayList.add(new ScheduleListModel("Royal Challengers Bangalore vs Kolkata Knight Riders", "IPL 6th T20 Match", "30 March 2022 at 07:30 PM - Web", "RCB", "KKR", R.drawable.rcbion, R.drawable.kkricon, "Dr DY Patil Sports Academy, Mumbai",false));
        scheduleListModelArrayList.add(new ScheduleListModel("", "", "", "", "", 0, 0, "",true));
        scheduleListModelArrayList.add(new ScheduleListModel("Lucknow Super Giants vs Chennai Super Kings", "IPL 7th T20 Match", "31 March 2022 at 07:30 PM - Thu", "LSG", "CSk", R.drawable.lsgicon, R.drawable.cskicon, "Brabourne Stadium, Mumbai",false));
        scheduleListModelArrayList.add(new ScheduleListModel("Kolkata Knight Riders vs Punjab Kings", "IPL 8th T20 Match", "1 April 2022 at 07:30 PM - Fir", "KKR", "PBKS", R.drawable.kkricon, R.drawable.kxiiicon, "Wankhede Stadium, Mumbai",false));
        scheduleListModelArrayList.add(new ScheduleListModel("", "", "", "", "", 0, 0, "",true));
        scheduleListModelArrayList.add(new ScheduleListModel("Mumbai Indians vs Rajasthan Royals", "IPL 9th T20 Match", "2 April at 03:30 PM - Sat", "MI", "RR", R.drawable.micon, R.drawable.rricon, "Dr DY Patil Sports Academy, Mumbai",false));
        scheduleListModelArrayList.add(new ScheduleListModel("Gujarat Titans vs Delhi Capitals", "IPL 10th T20 Match", "2 April at 07:30 PM - Sat", "GT", "DC", R.drawable.gticon, R.drawable.dcicon, "Maharashtra Cricket Association Stadium, Pune",false));
        scheduleListModelArrayList.add(new ScheduleListModel("", "", "", "", "", 0, 0, "",true));
        scheduleListModelArrayList.add(new ScheduleListModel("Chennai Super Kings vs Punjab Kings", "IPL 11th T20 Match", "3 April at 07:30 PM - Sun", "CSK", "PBKS", R.drawable.cskicon, R.drawable.kxiiicon, "Brabourne Stadium, Mumbai",false));
        scheduleListModelArrayList.add(new ScheduleListModel("Sunrisers Hyderabad vs Lucknow Super Giants", "IPL 12th T20 Match", "4 April at 07:30 PM - Mon", "SRH", "LSG", R.drawable.srhicon, R.drawable.lsgicon, "Dr DY Patil Sports Academy, Mumbai",false));
        scheduleListModelArrayList.add(new ScheduleListModel("", "", "", "", "", 0, 0, "",true));
        scheduleListModelArrayList.add(new ScheduleListModel("Rajasthan Royals vs Royal Challengers Bangalore", "IPL 13th T20 Match", "5 April at 07:30 PM - Tue", "RR", "RCB", R.drawable.rricon, R.drawable.rcbion, "Wankhede Stadium, Mumbai",false));
        scheduleListModelArrayList.add(new ScheduleListModel("Kolkata Knight Riders vs Mumbai Indians", "IPL 14th T20 Match", "6 April at 07:30 PM - Web", "KKR", "MI", R.drawable.kkricon, R.drawable.micon, "Maharashtra Cricket Association Stadium, Pune",false));
        scheduleListModelArrayList.add(new ScheduleListModel("", "", "", "", "", 0, 0, "",true));
        scheduleListModelArrayList.add(new ScheduleListModel("Lucknow Super Giants vs Delhi Capitals", "IPL 15th T20 Match", "7 April at 07:30 PM - Thu", "LSG", "DC", R.drawable.lsgicon, R.drawable.dcicon, "Dr DY Patil Sports Academy, Mumbai",false));
        scheduleListModelArrayList.add(new ScheduleListModel("Punjab Kings vs Gujarat Titans", "IPL 16th T20 Match", "8 April at 07:30 PM - Fir", "PBKS", "GT", R.drawable.kxiiicon, R.drawable.gticon, "Brabourne Stadium, Mumbai",false));
        scheduleListModelArrayList.add(new ScheduleListModel("", "", "", "", "", 0, 0, "",true));
        scheduleListModelArrayList.add(new ScheduleListModel("Chennai Super Kings vs Sunrisers Hyderabad", "IPL 17th T20 Match", "9 April at 03:30 PM - Sat", "CSK", "SRH", R.drawable.cskicon, R.drawable.srhicon, "Dr DY Patil Sports Academy, Mumbai",false));
        scheduleListModelArrayList.add(new ScheduleListModel("Royal Challengers Bangalore vs Mumbai Indians", "IPL 18th T20 Match", "9 April at 07:30 PM - Sat", "RCB", "MI", R.drawable.rcbion, R.drawable.micon, "Maharashtra Cricket Association Stadium, Pune",false));
        scheduleListModelArrayList.add(new ScheduleListModel("", "", "", "", "", 0, 0, "",true));
        scheduleListModelArrayList.add(new ScheduleListModel("Kolkata Knight Riders vs Delhi Capitals", "IPL 19th T20 Match", "10 April at 03:30 PM - Sun", "KKR", "DC", R.drawable.kkricon, R.drawable.dcicon, "Brabourne Stadium, Mumbai",false));
        scheduleListModelArrayList.add(new ScheduleListModel("Rajasthan Royals vs Lucknow Super Giants", "IPL 20th T20 Match", "10 April at 07:30 PM - Sun", "RR", "LSG", R.drawable.rricon, R.drawable.lsgicon, "Wankhede Stadium, Mumbai",false));
        scheduleListModelArrayList.add(new ScheduleListModel("", "", "", "", "", 0, 0, "",true));
        scheduleListModelArrayList.add(new ScheduleListModel("Sunrisers Hyderabad vs Gujarat Titans", "IPL 21st T20 Match", "11 April at 07:30 PM - Mon", "SRH", "GT", R.drawable.srhicon, R.drawable.gticon, "Dr DY Patil Sports Academy, Mumbai",false));
        scheduleListModelArrayList.add(new ScheduleListModel("Chennai Super Kings vs Royal Challengers Bangalore", "IPL 22nd T20 Match", "12 April at 07:30 PM - Tue", "CSK", "RCB", R.drawable.cskicon, R.drawable.rcbion, "Dr DY Patil Sports Academy, Mumbai",false));
        scheduleListModelArrayList.add(new ScheduleListModel("", "", "", "", "", 0, 0, "",true));
        scheduleListModelArrayList.add(new ScheduleListModel("Mumbai Indians vs Punjab Kings", "IPL 23rd T20 Match", "13 April at 07:30 PM - Web", "MI", "PBKS", R.drawable.micon, R.drawable.kxiiicon, "Maharashtra Cricket Association Stadium, Pune",false));
        scheduleListModelArrayList.add(new ScheduleListModel("Rajasthan Royals vs Gujarat Titans", "IPL 24th T20 Match", "14 April at 07:30 PM - Thu", "RR", "GT", R.drawable.rricon, R.drawable.gticon, "Dr DY Patil Sports Academy, Mumbai",false));
        scheduleListModelArrayList.add(new ScheduleListModel("", "", "", "", "", 0, 0, "",true));
        scheduleListModelArrayList.add(new ScheduleListModel("Sunrisers Hyderabad vs Kolkata Knight Riders", "IPL 25th T20 Match", "15 April at 07:30 PM - Fir", "SRH", "KKR", R.drawable.srhicon, R.drawable.kkricon, "Brabourne Stadium, Mumbai",false));
        scheduleListModelArrayList.add(new ScheduleListModel("Mumbai Indians vs Lucknow Super Giants", "IPL 26th T20 Match", "16 April at 03:30 PM - Sat", "MI", "LSG", R.drawable.micon, R.drawable.lsgicon, "Brabourne Stadium, Mumbai",false));
        scheduleListModelArrayList.add(new ScheduleListModel("", "", "", "", "", 0, 0, "",true));
        scheduleListModelArrayList.add(new ScheduleListModel("Delhi Capitals vs Royal Challengers Bangalore", "IPL 27th T20 Match", "16 April at 07:30 PM - Sat", "DC", "RCB", R.drawable.dcicon, R.drawable.rcbion, "Wankhede Stadium, Mumbai",false));
        scheduleListModelArrayList.add(new ScheduleListModel("Punjab Kings vs Sunrisers Hyderabad", "IPL 28th T20 Match", "17 April at 03:30 PM - Sun", "PBKS", "SRH", R.drawable.kxiiicon, R.drawable.srhicon, "Dr DY Patil Sports Academy, Mumbai",false));
        scheduleListModelArrayList.add(new ScheduleListModel("", "", "", "", "", 0, 0, "",true));
        scheduleListModelArrayList.add(new ScheduleListModel("Gujarat Titans vs Chennai Super Kings", "IPL 29th T20 Match", "17 April at 07:30 PM - Sun", "GT", "CSK", R.drawable.gticon, R.drawable.cskicon, "Maharashtra Cricket Association Stadium, Pune",false));
        scheduleListModelArrayList.add(new ScheduleListModel("Rajasthan Royals vs Kolkata Knight Riders", "IPL 30th T20 Match", "18 April at 07:30 PM - Mon", "RR", "KKR", R.drawable.rricon, R.drawable.kkricon, "Brabourne Stadium, Mumbai",false));
        scheduleListModelArrayList.add(new ScheduleListModel("", "", "", "", "", 0, 0, "",true));
        scheduleListModelArrayList.add(new ScheduleListModel("Lucknow Super Giants vs Royal Challengers Bangalore", "IPL 31st T20 Match", "19 April at 07:30 PM - Tue", "LSG", "RCB", R.drawable.lsgicon, R.drawable.rcbion, "Dr DY Patil Sports Academy, Mumbai",false));
        scheduleListModelArrayList.add(new ScheduleListModel("Delhi Capitals vs Punjab Kings", "IPL 32nd T20 Match", "20 April at 07:30 PM - Web", "DC", "PBKS", R.drawable.dcicon, R.drawable.kxiiicon, "Maharashtra Cricket Association Stadium, Pune",false));
        scheduleListModelArrayList.add(new ScheduleListModel("", "", "", "", "", 0, 0, "",true));
        scheduleListModelArrayList.add(new ScheduleListModel("Mumbai Indians vs Chennai Super Kings", "IPL 33rd T20 Match", "21 April at 07:30 PM - Thu", "MI", "CSK", R.drawable.micon, R.drawable.cskicon, "Dr DY Patil Sports Academy, Mumbai",false));
        scheduleListModelArrayList.add(new ScheduleListModel("Delhi Capitals vs Rajasthan Royals", "IPL 34th T20 Match", "22 April at 07:30 PM - Fir", "DC", "RR", R.drawable.dcicon, R.drawable.rricon, "Maharashtra Cricket Association Stadium, Pune",false));
        scheduleListModelArrayList.add(new ScheduleListModel("", "", "", "", "", 0, 0, "",true));
        scheduleListModelArrayList.add(new ScheduleListModel("Kolkata Knight Riders vs Gujarat Titans", "IPL 35th T20 Match", "23 April at 03:30 PM - Sat", "KKR", "GT", R.drawable.kkricon, R.drawable.gticon, "Dr DY Patil Sports Academy, Mumbai",false));
        scheduleListModelArrayList.add(new ScheduleListModel("Royal Challengers Bangalore vs Sunrisers Hyderabad", "IPL 36th T20 Match", "23 April at 07:30 PM - Sat", "RCB", "SRH", R.drawable.rcbion, R.drawable.srhicon, "Brabourne Stadium, Mumbai",false));
        scheduleListModelArrayList.add(new ScheduleListModel("", "", "", "", "", 0, 0, "",true));
        scheduleListModelArrayList.add(new ScheduleListModel("Lucknow Super Giants vs Mumbai Indians", "IPL 37th T20 Match", "24 April at 07:30 PM - Sun", "LSG", "MI", R.drawable.lsgicon, R.drawable.micon, "Wankhede Stadium, Mumbai",false));
        scheduleListModelArrayList.add(new ScheduleListModel("Punjab Kings vs Chennai Super Kings", "IPL 38th T20 Match", "25 April at 07:30 PM - Mon", "PBKS", "CSK", R.drawable.kxiiicon, R.drawable.cskicon, "Wankhede Stadium, Mumbai",false));
        scheduleListModelArrayList.add(new ScheduleListModel("", "", "", "", "", 0, 0, "",true));
        scheduleListModelArrayList.add(new ScheduleListModel("Royal Challengers Bangalore vs Rajasthan Royals", "IPL 39th T20 Match", "26 April at 07:30 PM - Tue", "RCB", "RR", R.drawable.rcbion, R.drawable.rricon, "Maharashtra Cricket Association Stadium, Pune",false));
        scheduleListModelArrayList.add(new ScheduleListModel("Gujarat Titans vs Sunrisers Hyderabad", "IPL 40th T20 Match", "27 April at 07:30 PM - Web", "GT", "SRH", R.drawable.gticon, R.drawable.srhicon, "Wankhede Stadium, Mumbai",false));
        scheduleListModelArrayList.add(new ScheduleListModel("", "", "", "", "", 0, 0, "",true));
        scheduleListModelArrayList.add(new ScheduleListModel("Delhi Capitals vs Kolkata Knight Riders", "IPL 41st T20 Match", "28 April at 07:30 PM - Thu", "DC", "KKR", R.drawable.dcicon, R.drawable.kkricon, "Wankhede Stadium, Mumbai",false));
        scheduleListModelArrayList.add(new ScheduleListModel("Punjab Kings vs Lucknow Super Giants", "IPL 42nd T20 Match", "29 April at 07:30 PM - Fri", "PBKS", "LSB", R.drawable.kxiiicon, R.drawable.lsgicon, "Maharashtra Cricket Association Stadium, Pune",false));
        scheduleListModelArrayList.add(new ScheduleListModel("", "", "", "", "", 0, 0, "",true));
        scheduleListModelArrayList.add(new ScheduleListModel("Gujarat Titans vs Royal Challengers Bangalore", "IPL 43rd T20 Match", "30 April at 03:30 PM - Sat", "GT", "RCB", R.drawable.gticon, R.drawable.rcbion, "Brabourne Stadium, Mumbai",false));
        scheduleListModelArrayList.add(new ScheduleListModel("Rajasthan Royals vs Mumbai Indians", "IPL 44th T20 Match", "30 April at 07:30 PM - Sat", "RR", "MI", R.drawable.rricon, R.drawable.micon, "Dr DY Patil Sports Academy, Mumbai",false));
        scheduleListModelArrayList.add(new ScheduleListModel("", "", "", "", "", 0, 0, "",true));
        scheduleListModelArrayList.add(new ScheduleListModel("Delhi Capitals vs Lucknow Super Giants", "IPL 45th T20 Match", "1 May at 03:30 PM - Sun", "DC", "LSG", R.drawable.dcicon, R.drawable.lsgicon, "Wankhede Stadium, Mumbai",false));
        scheduleListModelArrayList.add(new ScheduleListModel("Sunrisers Hyderabad vs Chennai Super Kings", "IPL 46th T20 Match", "1 May at 07:30 PM - Sun", "SRH", "CSK", R.drawable.srhicon, R.drawable.cskicon, "Maharashtra Cricket Association Stadium, Pune",false));
        scheduleListModelArrayList.add(new ScheduleListModel("", "", "", "", "", 0, 0, "",true));
        scheduleListModelArrayList.add(new ScheduleListModel("Kolkata Knight Riders vs Rajasthan Royals", "IPL 47th T20 Match", "2 May at 07:30 PM - Mon", "KKR", "RR", R.drawable.kkricon, R.drawable.rricon, "Wankhede Stadium, Mumbai",false));
        scheduleListModelArrayList.add(new ScheduleListModel("Gujarat Titans vs Punjab Kings, 48th Match", "IPL 48th T20 Match", "3 May at 07:30 PM - Tue", "GT", "PBKS", R.drawable.gticon, R.drawable.kxiiicon, "Dr DY Patil Sports Academy, Mumbai",false));
        scheduleListModelArrayList.add(new ScheduleListModel("", "", "", "", "", 0, 0, "",true));
        scheduleListModelArrayList.add(new ScheduleListModel("Royal Challengers Bangalore vs Chennai Super Kings", "IPL 49th T20 Match", "4 May at 07:30 PM - Web", "RCB", "CSK", R.drawable.rcbion, R.drawable.cskicon, "Maharashtra Cricket Association Stadium, Pune",false));
        scheduleListModelArrayList.add(new ScheduleListModel("Delhi Capitals vs Sunrisers Hyderabad", "IPL 50th T20 Match", "5 May at 07:30 PM - Thu", "DC", "SRH", R.drawable.dcicon, R.drawable.srhicon, "Brabourne Stadium, Mumbai",false));
        scheduleListModelArrayList.add(new ScheduleListModel("", "", "", "", "", 0, 0, "",true));
        scheduleListModelArrayList.add(new ScheduleListModel("Gujarat Titans vs Mumbai Indians", "IPL 51st T20 Match", "6 May at 07:30 PM - Fri", "GT", "MI", R.drawable.gticon, R.drawable.micon, "Brabourne Stadium, Mumbai",false));
        scheduleListModelArrayList.add(new ScheduleListModel("Punjab Kings vs Rajasthan Royals", "IPL 52nd T20 Match", "7 May at 03:30 PM - Sat", "PBKS", "RR", R.drawable.kxiiicon, R.drawable.rricon, "Wankhede Stadium, Mumbai",false));
        scheduleListModelArrayList.add(new ScheduleListModel("", "", "", "", "", 0, 0, "",true));
        scheduleListModelArrayList.add(new ScheduleListModel("Lucknow Super Giants vs Kolkata Knight Riders", "IPL 53rd T20 Match", "7 May at 07:30 PM - Sat", "LSG", "KKR", R.drawable.lsgicon, R.drawable.kkricon, "Maharashtra Cricket Association Stadium, Pune",false));
        scheduleListModelArrayList.add(new ScheduleListModel("Sunrisers Hyderabad vs Royal Challengers Bangalore,", "IPL 54th T20 Match", "8 May at 03:30 PM - Sun", "SRH", "RCB", R.drawable.srhicon, R.drawable.rcbion, "Wankhede Stadium, Mumbai",false));
        scheduleListModelArrayList.add(new ScheduleListModel("", "", "", "", "", 0, 0, "",true));
        scheduleListModelArrayList.add(new ScheduleListModel("Chennai Super Kings vs Delhi Capitals", "IPL 55th T20 Match", "8 May at 07:30 PM - Sun", "CSK", "DC", R.drawable.cskicon, R.drawable.dcicon, "Dr DY Patil Sports Academy, Mumbai",false));
        scheduleListModelArrayList.add(new ScheduleListModel("Mumbai Indians vs Kolkata Knight Riders", "IPL 56th T20 Match", "9 May at 07:30 PM - Mon", "MI", "KKR", R.drawable.micon, R.drawable.kkricon, "Dr DY Patil Sports Academy, Mumbai",false));
        scheduleListModelArrayList.add(new ScheduleListModel("", "", "", "", "", 0, 0, "",true));
        scheduleListModelArrayList.add(new ScheduleListModel("Lucknow Super Giants vs Gujarat Titans", "IPL 57th T20 Match", "10 May at 07:30 PM - Tue", "LSG", "GT", R.drawable.lsgicon, R.drawable.gticon, "Maharashtra Cricket Association Stadium",false));
        scheduleListModelArrayList.add(new ScheduleListModel("Rajasthan Royals vs Delhi Capitals", "IPL 58th T20 Match", "11 May at 07:30 PM - Web", "RR", "DC", R.drawable.rricon, R.drawable.dcicon, "Dr DY Patil Sports Academy, Mumbai",false));
        scheduleListModelArrayList.add(new ScheduleListModel("", "", "", "", "", 0, 0, "",true));
        scheduleListModelArrayList.add(new ScheduleListModel("Chennai Super Kings vs Mumbai Indians", "IPL 59th T20 Match", "12 May at 07:30 PM - Thu", "CSK", "MI", R.drawable.cskicon, R.drawable.micon, "Wankhede Stadium, Mumbai",false));
        scheduleListModelArrayList.add(new ScheduleListModel("Royal Challengers Bangalore vs Punjab Kings", "IPL 60th T20 Match", "13 May at 07:30 PM - Fri", "RCB", "PBKS", R.drawable.rcbion, R.drawable.kxiiicon, "Brabourne Stadium, Mumbai",false));
        scheduleListModelArrayList.add(new ScheduleListModel("", "", "", "", "", 0, 0, "",true));
        scheduleListModelArrayList.add(new ScheduleListModel("Kolkata Knight Riders vs Sunrisers Hyderabad", "IPL 61st T20 Match", "14 May at 07:30 PM - Sat", "KKR", "SRH", R.drawable.kkricon, R.drawable.srhicon, "Maharashtra Cricket Association Stadium, Pune",false));
        scheduleListModelArrayList.add(new ScheduleListModel("Chennai Super Kings vs Gujarat Titans", "IPL 62nd T20 Match", "15 May at 03:30 PM - Sun", "CSK", "GT", R.drawable.cskicon, R.drawable.gticon, "Wankhede Stadium, Mumbai",false));
        scheduleListModelArrayList.add(new ScheduleListModel("", "", "", "", "", 0, 0, "",true));
        scheduleListModelArrayList.add(new ScheduleListModel("Lucknow Super Giants vs Rajasthan Royals", "IPL 63rd T20 Match", "15 May at 07:30 PM - Sun", "LSG", "RR", R.drawable.lsgicon, R.drawable.rricon, "Brabourne Stadium, Mumbai",false));
        scheduleListModelArrayList.add(new ScheduleListModel("Punjab Kings vs Delhi Capitals", "IPL 64th T20 Match", "16 May at 07:30 PM - Mon", "PBKS", "DC", R.drawable.kxiiicon, R.drawable.dcicon, "Dr DY Patil Sports Academy, Mumbai",false));
        scheduleListModelArrayList.add(new ScheduleListModel("", "", "", "", "", 0, 0, "",true));
        scheduleListModelArrayList.add(new ScheduleListModel("Mumbai Indians vs Sunrisers Hyderabad", "IPL 65th T20 Match", "17 May at 07:30 PM - Tue", "MI", "SRH", R.drawable.micon, R.drawable.srhicon, "Wankhede Stadium, Mumbai",false));
        scheduleListModelArrayList.add(new ScheduleListModel("Kolkata Knight Riders vs Lucknow Super Giants", "IPL 66th T20 Match", "18 May at 07:30 PM - Web", "KKR", "LSG", R.drawable.kkricon, R.drawable.lsgicon, "Dr DY Patil Sports Academy, Mumbai",false));
        scheduleListModelArrayList.add(new ScheduleListModel("", "", "", "", "", 0, 0, "",true));
        scheduleListModelArrayList.add(new ScheduleListModel("Royal Challengers Bangalore vs Gujarat Titans", "IPL 67th T20 Match", "19 May at 07:30 PM - Thu", "RCB", "GT", R.drawable.rcbion, R.drawable.gticon, "Wankhede Stadium, Mumbai",false));
        scheduleListModelArrayList.add(new ScheduleListModel("Rajasthan Royals vs Chennai Super Kings", "IPL 68th T20 Match", "20 May at 07:30 PM - Fir", "RR", "CSK", R.drawable.rricon, R.drawable.cskicon, "Brabourne Stadium, Mumbai",false));
        scheduleListModelArrayList.add(new ScheduleListModel("", "", "", "", "", 0, 0, "",true));
        scheduleListModelArrayList.add(new ScheduleListModel("Mumbai Indians vs Delhi Capitals", "IPL 69th T20 Match", "21 May at 07:30 PM - Sat", "MI", "DC", R.drawable.micon, R.drawable.dcicon, "Wankhede Stadium, Mumbai",false));
        scheduleListModelArrayList.add(new ScheduleListModel("Sunrisers Hyderabad vs Punjab Kings", "IPL 70th T20 Match", "22 May at 07:30 PM - Sun", "SRH", "PBKS", R.drawable.srhicon, R.drawable.kxiiicon, "Wankhede Stadium, Mumbai",false));
        scheduleListModelArrayList.add(new ScheduleListModel("", "", "", "", "", 0, 0, "",true));
        scheduleListModelArrayList.add(new ScheduleListModel("QUALIFIER 1", "IPL 71th T20 Match", "TBD", "TBD", "TBD", R.drawable.logo, R.drawable.logo, "Narendra Modi Stadium, Ahmedabad",false));
        scheduleListModelArrayList.add(new ScheduleListModel("ELIMINATOR", "IPL 72th T20 Match", "TBD", "TBD", "TBD", R.drawable.logo, R.drawable.logo, "Narendra Modi Stadium, Ahmedabad",false));
        scheduleListModelArrayList.add(new ScheduleListModel("", "", "", "", "", 0, 0, "",true));
        scheduleListModelArrayList.add(new ScheduleListModel("QUALIFIER 2", "IPL 73th T20 Match", "TBD", "TBD", "TBD", R.drawable.logo, R.drawable.logo, "Narendra Modi Stadium, Ahmedabad",false));
        scheduleListModelArrayList.add(new ScheduleListModel("FINAL", "IPL 74th T20 Match", "29 May at 07:30 PM - Sun", "TBD", "TBD", R.drawable.logo, R.drawable.logo, "Narendra Modi Stadium, Ahmedabad",false));
        scheduleListModelArrayList.add(new ScheduleListModel("", "", "", "", "", 0, 0, "",true));
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(new ScheduleListAdapter(this,scheduleListModelArrayList));
    }




    @Override
    public void onBackPressed() {
        try {
            if (mInterstitialAd != null && !isActivityLeft) {
                mInterstitialAd.show(SchedulesActivity.this);
                MyApplication.appOpenManager.isAdShow = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        finish();
    }

    public void onPause() {
        super.onPause();
        this.isActivityLeft = true;
    }

    public void onResume() {
        super.onResume();
        this.isActivityLeft = false;
    }

    protected void onStop() {
        super.onStop();
        this.isActivityLeft = true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        isActivityLeft = true;
    }
    public void LoadAdInterstitial() {
        AdRequest adRequest = new AdRequest.Builder().build();

        InterstitialAd.load(this, MyApplication.get_Admob_interstitial_Id(), adRequest, new InterstitialAdLoadCallback() {
            @Override
            public void onAdLoaded(@NonNull InterstitialAd interstitialAd) {
                mInterstitialAd = interstitialAd;
                mInterstitialAd.setFullScreenContentCallback(new FullScreenContentCallback() {
                    @Override
                    public void onAdDismissedFullScreenContent() {
                        MyApplication.appOpenManager.isAdShow = false;
                        mInterstitialAd = null;
                    }

                    @Override
                    public void onAdFailedToShowFullScreenContent(@NonNull AdError adError) {
                        MyApplication.appOpenManager.isAdShow = false;
                    }

                    @Override
                    public void onAdShowedFullScreenContent() {
                        mInterstitialAd = null;
                        MyApplication.appOpenManager.isAdShow = true;
                    }
                });
            }

            @Override
            public void onAdFailedToLoad(@NonNull LoadAdError loadAdError) {
                mInterstitialAd = null;
                MyApplication.appOpenManager.isAdShow = false;

            }
        });
    }

    private void GetData() {
        try {
            if (NetworkCheck.isNetworkAvailable(SchedulesActivity.this)) {
                if (retrofit == null) {
                    retrofit = new Retrofit.Builder().baseUrl("https://smartadz.in/api/").client(new OkHttpClient.Builder().addInterceptor(chain -> chain.proceed(chain.request().newBuilder().build())).connectTimeout(100, TimeUnit.SECONDS).readTimeout(100, TimeUnit.SECONDS).build()).addConverterFactory(GsonConverterFactory.create()).build();
                }
                APIService nativeClient = retrofit.create(APIService.class);
                nativeClient.GetData().enqueue(new Callback<Playdata>() {
                    @Override
                    public void onResponse(@NotNull Call<Playdata> call, @NotNull retrofit2.Response<Playdata> response) {
                        try {
                            if (response.body() != null) {
                                if (response.body().isStatus()) {
                                    if (response.body().getData().isFlage()) {
                                        qurekaLink = response.body().getData().getUrl();
                                        findViewById(R.id.qurekaads).setVisibility(View.VISIBLE);
                                        Glide.with(SchedulesActivity.this).load(response.body().getData().getImage()).listener(new RequestListener<Drawable>() {
                                            @Override
                                            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                                return false;
                                            }

                                            @Override
                                            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                                return false;
                                            }
                                        }).into((ImageView) findViewById(R.id.img));
                                    }
                                }
                            }

                        } catch (Exception exception) {
                            exception.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<Playdata> call, @NonNull Throwable t) {
                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}