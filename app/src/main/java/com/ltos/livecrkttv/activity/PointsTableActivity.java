package com.ltos.livecrkttv.activity;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.ads.AdError;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.FullScreenContentCallback;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.interstitial.InterstitialAd;
import com.google.android.gms.ads.interstitial.InterstitialAdLoadCallback;
import com.ltos.livecrkttv.R;
import com.ltos.livecrkttv.adapter.TeamPointAdapter;
import com.ltos.livecrkttv.adapter.TeamPointAdapter2022;
import com.ltos.livecrkttv.admodels.APIInterFace;
import com.ltos.livecrkttv.model.Datum;
import com.ltos.livecrkttv.model.Example;
import com.ltos.livecrkttv.model.Pointtablenew;
import com.ltos.livecrkttv.utils.MyApplication;

import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class PointsTableActivity extends AppCompatActivity {
    ImageView mFilter;
    ArrayList<Datum> pointData;
    ArrayList<Pointtablenew.Data.GetPointsTable.Standing.Team> pointData2022;
    RecyclerView rvPointtable;
    TextView title;

    InterstitialAd mInterstitialAd;
    boolean isActivityLeft;
    ProgressDialog progressDialog;


    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_points_table);
        getWindow().setBackgroundDrawable(null);
        isActivityLeft = false;
        LoadAdInterstitial();
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait...");
        pointData = new ArrayList<>();
        pointData2022 = new ArrayList<>();
        this.rvPointtable = findViewById(R.id.rvPointtable);
        this.title = findViewById(R.id.title);
        this.mFilter = findViewById(R.id.mFilter);
        findViewById(R.id.back_btn).setOnClickListener(view -> onBackPressed());
        onClickEvent();
        try {
            CallData2022();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void CallData2021() {
        progressDialog.show();
        this.rvPointtable.setVisibility(View.VISIBLE);
        if (!MyApplication.get_cricApipoint().equals("")) {
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(MyApplication.get_cricApipoint())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            APIInterFace service = retrofit.create(APIInterFace.class);
            service.getUser2021().enqueue(new Callback<Example>() {
                public void onResponse(@NonNull Call<Example> call, @NonNull Response<Example> response) {
                    if (progressDialog != null && progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }
                    if (response.isSuccessful()) {
                        if (response.body() != null) {
                            pointData = new ArrayList<>();
                            PointsTableActivity.this.pointData.addAll(response.body().getData().getData());
                            TeamPointAdapter teamPointAdapter = new TeamPointAdapter(PointsTableActivity.this, pointData);
                            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(PointsTableActivity.this, RecyclerView.VERTICAL, false);
                            PointsTableActivity.this.rvPointtable.setAdapter(teamPointAdapter);
                            PointsTableActivity.this.rvPointtable.setLayoutManager(linearLayoutManager);
                        }
                        findViewById(R.id.tv_no_data).setVisibility(View.GONE);
                    } else {
                        findViewById(R.id.tv_no_data).setVisibility(View.VISIBLE);
                    }
                }

                public void onFailure(@NonNull Call<Example> call, @NonNull Throwable th) {
                    findViewById(R.id.tv_no_data).setVisibility(View.VISIBLE);
                    if (progressDialog != null && progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }
                }
            });
        } else {
            findViewById(R.id.tv_no_data).setVisibility(View.VISIBLE);
            if (progressDialog != null && progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
        }
    }


    public void CallData2020() {
        progressDialog.show();
        this.rvPointtable.setVisibility(View.VISIBLE);
        if (!MyApplication.get_cricApipoint().equals("")) {
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(MyApplication.get_cricApipoint())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            APIInterFace service = retrofit.create(APIInterFace.class);
            service.getUser2020().enqueue(new Callback<Example>() {
                public void onResponse(@NonNull Call<Example> call, @NonNull Response<Example> response) {
                    if (progressDialog != null && progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }
                    if (response.isSuccessful()) {
                        if (response.body() != null) {
                            pointData = new ArrayList<>();
                            PointsTableActivity.this.pointData.addAll(response.body().getData().getData());
                            PointsTableActivity pointsTable_Activity = PointsTableActivity.this;
                            TeamPointAdapter teamPointAdapter = new TeamPointAdapter(pointsTable_Activity, pointsTable_Activity.pointData);
                            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(PointsTableActivity.this, RecyclerView.VERTICAL, false);
                            PointsTableActivity.this.rvPointtable.setAdapter(teamPointAdapter);
                            PointsTableActivity.this.rvPointtable.setLayoutManager(linearLayoutManager);
                        }
                        findViewById(R.id.tv_no_data).setVisibility(View.GONE);
                    } else {
                        findViewById(R.id.tv_no_data).setVisibility(View.VISIBLE);
                    }
                }

                public void onFailure(@NonNull Call<Example> call, @NonNull Throwable th) {
                    findViewById(R.id.tv_no_data).setVisibility(View.VISIBLE);
                    if (progressDialog != null && progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }
                }
            });
        } else {
            findViewById(R.id.tv_no_data).setVisibility(View.VISIBLE);
            if (progressDialog != null && progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
        }
    }

    public void CallData2022() {
        progressDialog.show();
        if (!MyApplication.get_cricApipoint2022().equals("")) {
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(MyApplication.get_cricApipoint2022())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            APIInterFace service = retrofit.create(APIInterFace.class);
            MediaType mediaType = MediaType.parse("application/json");
            RequestBody body = RequestBody.create(mediaType, "{\"query\":\"\\n\\tquery getPointsTable($tourID: String!) {\\n\\t\\tgetPointsTable(tourID: $tourID) {\\n\\t\\t\\tstandings {\\n\\t\\t\\t\\tname\\n\\t\\t\\t\\tteams {\\n\\t\\t\\t\\t\\tpos\\n\\t\\t\\t\\t\\tteamID\\n\\t\\t\\t\\t\\tteamName\\n\\t\\t\\t\\t\\tteamShortName\\n\\t\\t\\t\\t\\tall\\n\\t\\t\\t\\t\\twins\\n\\t\\t\\t\\t\\tlost\\n\\t\\t\\t\\t\\tpoints\\n\\t\\t\\t\\t\\tnrr\\n\\t\\t\\t\\t\\tisQualified\\n\\t\\t\\t\\t}\\n\\t\\t\\t}\\n\\t\\t}\\n\\t}\\n\",\"variables\":{\"tourID\":\"2220\"}}");
            service.getUsernew2022(body).enqueue(new Callback<Pointtablenew>() {
                public void onResponse(@NonNull Call<Pointtablenew> call, @NonNull Response<Pointtablenew> response) {
                    if (progressDialog != null && progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }
                    if (response.code() == 200) {
                        if (response.isSuccessful()) {
                            if (response.body() != null) {
                                if (response.body().getData() != null) {
                                    pointData2022 = new ArrayList<>();
                                    for (int i = 0; i < response.body().getData().getGetPointsTable().getStandings().size(); i++) {
                                        PointsTableActivity.this.pointData2022.addAll(response.body().getData().getGetPointsTable().getStandings().get(i).getTeams());
                                    }
                                    TeamPointAdapter2022 teamPointAdapter = new TeamPointAdapter2022(PointsTableActivity.this, pointData2022);
                                    LinearLayoutManager linearLayoutManager = new LinearLayoutManager(PointsTableActivity.this, RecyclerView.VERTICAL, false);
                                    PointsTableActivity.this.rvPointtable.setAdapter(teamPointAdapter);
                                    PointsTableActivity.this.rvPointtable.setLayoutManager(linearLayoutManager);
                                }
                            }
                        }
                    } else {
                        findViewById(R.id.tv_no_data).setVisibility(View.GONE);
                    }
                }

                public void onFailure(@NonNull Call<Pointtablenew> call, @NonNull Throwable th) {
                    findViewById(R.id.tv_no_data).setVisibility(View.GONE);
                    if (progressDialog != null && progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }
                }
            });
        } else {
            findViewById(R.id.tv_no_data).setVisibility(View.VISIBLE);
            if (progressDialog != null && progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
        }
    }

    public void CallData2019() {
        progressDialog.show();
        this.rvPointtable.setVisibility(View.VISIBLE);
        if (!MyApplication.get_cricApipoint().equals("")) {
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(MyApplication.get_cricApipoint())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            APIInterFace service = retrofit.create(APIInterFace.class);
            service.getUser2019().enqueue(new Callback<Example>() {
                public void onResponse(@NonNull Call<Example> call, @NonNull Response<Example> response) {
                    if (progressDialog != null && progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }
                    if (response.isSuccessful()) {
                        if (response.body() != null) {
                            pointData = new ArrayList<>();
                            PointsTableActivity.this.pointData.addAll(response.body().getData().getData());
                            TeamPointAdapter teamPointAdapter = new TeamPointAdapter(PointsTableActivity.this, pointData);
                            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(PointsTableActivity.this,
                                    RecyclerView.VERTICAL,
                                    false);
                            PointsTableActivity.this.rvPointtable.setAdapter(teamPointAdapter);
                            PointsTableActivity.this.rvPointtable.setLayoutManager(linearLayoutManager);
                        }
                        findViewById(R.id.tv_no_data).setVisibility(View.GONE);
                    } else {
                        findViewById(R.id.tv_no_data).setVisibility(View.VISIBLE);
                    }
                }

                public void onFailure(@NonNull Call<Example> call, @NonNull Throwable th) {
                    findViewById(R.id.tv_no_data).setVisibility(View.VISIBLE);
                    if (progressDialog != null && progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }
                }
            });
        } else {
            findViewById(R.id.tv_no_data).setVisibility(View.VISIBLE);
            if (progressDialog != null && progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
        }
    }

    private void onClickEvent() {
        this.mFilter.setOnClickListener(view -> PointsTableActivity.this.showDialog());
    }

    @SuppressLint("WrongConstant")
    public void showDialog() {
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.filter_dig);
        Window window = dialog.getWindow();
        WindowManager.LayoutParams attributes = window.getAttributes();
        dialog.setCancelable(true);
        attributes.gravity = 80;
        window.setAttributes(attributes);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
        dialog.findViewById(R.id.txt_2019).setOnClickListener(view -> {
            CallData2019();
            dialog.dismiss();
        });
        dialog.findViewById(R.id.txt_2020).setOnClickListener(view -> {
            CallData2020();
            dialog.dismiss();
        });
        dialog.findViewById(R.id.txt_2021).setOnClickListener(view -> {
            CallData2021();
            dialog.dismiss();
        });
        dialog.findViewById(R.id.txt_2022).setOnClickListener(view -> {
            CallData2022();
            dialog.dismiss();
        });
        dialog.findViewById(R.id.close).setOnClickListener(view -> dialog.dismiss());
        dialog.show();
    }


    @Override
    public void onBackPressed() {
        try {
            if (mInterstitialAd != null && !isActivityLeft) {
                mInterstitialAd.show(PointsTableActivity.this);
                MyApplication.appOpenManager.isAdShow = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        finish();
    }

    public void onPause() {
        super.onPause();
        this.isActivityLeft = true;
    }

    public void onResume() {
        super.onResume();
        this.isActivityLeft = false;
    }

    protected void onStop() {
        super.onStop();
        this.isActivityLeft = true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        isActivityLeft = true;
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

    public void LoadAdInterstitial() {
        AdRequest adRequest = new AdRequest.Builder().build();

        InterstitialAd.load(this, MyApplication.get_Admob_interstitial_Id(), adRequest, new InterstitialAdLoadCallback() {
            @Override
            public void onAdLoaded(@NonNull InterstitialAd interstitialAd) {
                mInterstitialAd = interstitialAd;
                mInterstitialAd.setFullScreenContentCallback(new FullScreenContentCallback() {
                    @Override
                    public void onAdDismissedFullScreenContent() {
                        MyApplication.appOpenManager.isAdShow = false;
                        mInterstitialAd = null;
                    }

                    @Override
                    public void onAdFailedToShowFullScreenContent(@NonNull AdError adError) {
                        MyApplication.appOpenManager.isAdShow = false;
                    }

                    @Override
                    public void onAdShowedFullScreenContent() {
                        mInterstitialAd = null;
                        MyApplication.appOpenManager.isAdShow = true;
                    }
                });
            }

            @Override
            public void onAdFailedToLoad(@NonNull LoadAdError loadAdError) {
                mInterstitialAd = null;
                MyApplication.appOpenManager.isAdShow = false;
            }
        });
    }


}
