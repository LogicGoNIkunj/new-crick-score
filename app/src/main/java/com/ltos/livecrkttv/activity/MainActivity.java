package com.ltos.livecrkttv.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.browser.customtabs.CustomTabsIntent;

import com.google.android.gms.ads.AdError;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdLoader;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.FullScreenContentCallback;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.interstitial.InterstitialAd;
import com.google.android.gms.ads.interstitial.InterstitialAdLoadCallback;
import com.google.android.gms.ads.nativead.MediaView;
import com.google.android.gms.ads.nativead.NativeAd;
import com.google.android.gms.ads.nativead.NativeAdView;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.ltos.livecrkttv.R;
import com.ltos.livecrkttv.interfaces.APIService;
import com.ltos.livecrkttv.model.Playdata;
import com.ltos.livecrkttv.utils.CustomTabActivityHelper;
import com.ltos.livecrkttv.utils.MyApplication;
import com.ltos.livecrkttv.utils.NetworkCheck;
import com.ltos.livecrkttv.utils.WebviewFallback;
import com.onesignal.OneSignal;

import org.jetbrains.annotations.NotNull;

import java.util.Objects;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {


    InterstitialAd mInterstitialAd;
    boolean isActivityLeft;
    FrameLayout mFrmlay;
    NativeAd nativeAds;
    private Retrofit retrofit;
    String qurekaLink = "";
    TextView card_banner;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Analytics();
        isActivityLeft = false;
        LoadAdInterstitial();
        mFrmlay = findViewById(R.id.fl_adplaceholder);
        card_banner = findViewById(R.id.ads_space);
        RefreshAd();
        GetData();

        findViewById(R.id.live_cric_text).setSelected(true);
        findViewById(R.id.live_cricket_board).setOnClickListener(view -> {
            startActivity(new Intent(MainActivity.this, LiveCricketActivity.class));
            try {
                if (mInterstitialAd != null && !isActivityLeft) {
                    mInterstitialAd.show(MainActivity.this);
                    MyApplication.appOpenManager.isAdShow = true;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });


        findViewById(R.id.live_cricket).setOnClickListener(view -> {
            startActivity(new Intent(MainActivity.this, LiveCricketActivity.class));
            try {
                if (mInterstitialAd != null && !isActivityLeft) {
                    mInterstitialAd.show(MainActivity.this);
                    MyApplication.appOpenManager.isAdShow = true;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        findViewById(R.id.IPL_Winner).setOnClickListener(view -> {
            startActivity(new Intent(MainActivity.this, WinnerActivity.class));
            try {
                if (mInterstitialAd != null && !isActivityLeft) {
                    mInterstitialAd.show(MainActivity.this);
                    MyApplication.appOpenManager.isAdShow = true;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        findViewById(R.id.Team_list).setOnClickListener(view -> {
            startActivity(new Intent(MainActivity.this, TeamListActivity.class));
            try {
                if (mInterstitialAd != null && !isActivityLeft) {
                    mInterstitialAd.show(MainActivity.this);
                    MyApplication.appOpenManager.isAdShow = true;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        findViewById(R.id.IPL_Schedule).setOnClickListener(view -> {
            startActivity(new Intent(MainActivity.this, SchedulesActivity.class));
            try {
                if (mInterstitialAd != null && !isActivityLeft) {
                    mInterstitialAd.show(MainActivity.this);
                    MyApplication.appOpenManager.isAdShow = true;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        findViewById(R.id.IPL_Point).setOnClickListener(view -> {
            startActivity(new Intent(MainActivity.this, PointsTableActivity.class));
            try {
                if (mInterstitialAd != null && !isActivityLeft) {
                    mInterstitialAd.show(MainActivity.this);
                    MyApplication.appOpenManager.isAdShow = true;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        findViewById(R.id.Play_querka).setOnClickListener(view -> {
            if (NetworkCheck.isNetworkAvailable(this)) {
                if (!qurekaLink.equals("")) {
                    try {
                        CustomTabsIntent customTabsIntent = new CustomTabsIntent.Builder().build();
                        CustomTabActivityHelper.openCustomTab(MainActivity.this, customTabsIntent, Uri.parse(qurekaLink), new WebviewFallback());
                    } catch (Exception e) {
                        CustomTabsIntent.Builder builder = new CustomTabsIntent.Builder();
                        CustomTabsIntent customTabsIntent = builder.build();
                        customTabsIntent.launchUrl(MainActivity.this, Uri.parse(qurekaLink));
                    }
                }
            } else {
                Toast.makeText(this, "Please Check Your Internet Connection", Toast.LENGTH_SHORT).show();
            }

        });

        findViewById(R.id.iv_setting).setOnClickListener(view -> startActivity(new Intent(MainActivity.this, SettingActivity.class)));
    }


    private void Analytics() {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        FirebaseAnalytics.getInstance(this);
        FirebaseCrashlytics.getInstance();
        OneSignal.initWithContext(this);
        OneSignal.setAppId(getResources().getString(R.string.one_signal));
    }


    public void onPause() {
        super.onPause();
        this.isActivityLeft = true;
    }

    public void onResume() {
        super.onResume();
        this.isActivityLeft = false;
    }

    protected void onStop() {
        super.onStop();
        this.isActivityLeft = true;
    }


    @Override
    public void onBackPressed() {
        Intent intent = new Intent(getApplicationContext(), ExitActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(intent);
    }

    public void LoadAdInterstitial() {
        AdRequest adRequest = new AdRequest.Builder().build();

        InterstitialAd.load(this, MyApplication.get_Admob_interstitial_Id(), adRequest, new InterstitialAdLoadCallback() {
            @Override
            public void onAdLoaded(@NonNull InterstitialAd interstitialAd) {
                mInterstitialAd = interstitialAd;
                mInterstitialAd.setFullScreenContentCallback(new FullScreenContentCallback() {
                    @Override
                    public void onAdDismissedFullScreenContent() {
                        MyApplication.appOpenManager.isAdShow = false;
                        mInterstitialAd = null;
                        LoadAdInterstitial();
                    }

                    @Override
                    public void onAdFailedToShowFullScreenContent(@NonNull AdError adError) {
                        MyApplication.appOpenManager.isAdShow = false;
                    }

                    @Override
                    public void onAdShowedFullScreenContent() {
                        mInterstitialAd = null;
                        MyApplication.appOpenManager.isAdShow = true;
                    }
                });
            }

            @Override
            public void onAdFailedToLoad(@NonNull LoadAdError loadAdError) {
                mInterstitialAd = null;
                MyApplication.appOpenManager.isAdShow = false;

            }
        });
    }

    private void RefreshAd() {

        AdLoader.Builder builder = new AdLoader.Builder(this, MyApplication.get_Admob_native_Id())
                .forNativeAd(nativeAd -> {
                    nativeAds = nativeAd;
                    @SuppressLint("InflateParams") NativeAdView adView = (NativeAdView) getLayoutInflater()
                            .inflate(R.layout.main_ads_native, null);
                    populateUnifiedNativeAdView(nativeAd, adView);
                    mFrmlay.removeAllViews();
                    mFrmlay.addView(adView);
                    card_banner.setVisibility(View.GONE);
                });
        AdLoader adLoader = builder.withAdListener(new AdListener() {
            @Override
            public void onAdFailedToLoad(@NonNull @NotNull LoadAdError loadAdError) {
                super.onAdFailedToLoad(loadAdError);
                card_banner.setVisibility(View.GONE);
            }
        }).build();

        adLoader.loadAd(new AdRequest.Builder().build());


    }

    public void populateUnifiedNativeAdView(NativeAd nativeAd, NativeAdView adView) {
        MediaView mediaView = adView.findViewById(R.id.ad_media);
        adView.setMediaView(mediaView);
        adView.setHeadlineView(adView.findViewById(R.id.ad_headline));
        adView.setBodyView(adView.findViewById(R.id.ad_body));
        adView.setCallToActionView(adView.findViewById(R.id.ad_call_to_action));
        adView.setIconView(adView.findViewById(R.id.ad_app_icon));
        ((TextView) Objects.requireNonNull(adView.getHeadlineView())).setText(nativeAd.getHeadline());
        if (nativeAd.getBody() == null) {
            Objects.requireNonNull(adView.getBodyView()).setVisibility(View.INVISIBLE);
        } else {
            Objects.requireNonNull(adView.getBodyView()).setVisibility(View.VISIBLE);
            ((TextView) adView.getBodyView()).setText(nativeAd.getBody());
        }
        if (nativeAd.getCallToAction() == null) {
            Objects.requireNonNull(adView.getCallToActionView()).setVisibility(View.INVISIBLE);
        } else {
            Objects.requireNonNull(adView.getCallToActionView()).setVisibility(View.VISIBLE);
            ((TextView) adView.getCallToActionView()).setText(nativeAd.getCallToAction());
        }
        if (nativeAd.getIcon() == null) {
            Objects.requireNonNull(adView.getIconView()).setVisibility(View.GONE);
        } else {
            ((ImageView) Objects.requireNonNull(adView.getIconView())).setImageDrawable(
                    nativeAd.getIcon().getDrawable());
            adView.getIconView().setVisibility(View.VISIBLE);
        }
        adView.setNativeAd(nativeAd);
    }

    private void GetData() {
        try {
            if (NetworkCheck.isNetworkAvailable(MainActivity.this)) {
                if (retrofit == null) {
                    retrofit = new Retrofit.Builder().baseUrl("https://smartadz.in/api/").client(new OkHttpClient.Builder().addInterceptor(chain -> chain.proceed(chain.request().newBuilder().build())).connectTimeout(100, TimeUnit.SECONDS).readTimeout(100, TimeUnit.SECONDS).build()).addConverterFactory(GsonConverterFactory.create()).build();
                }
                APIService nativeClient = retrofit.create(APIService.class);
                nativeClient.GetData().enqueue(new Callback<Playdata>() {
                    @Override
                    public void onResponse(@NotNull Call<Playdata> call, @NotNull retrofit2.Response<Playdata> response) {
                        try {
                            if (response.body() != null) {
                                if (response.body().isStatus()) {
                                    if (response.body().getData().isFlage()) {
                                        qurekaLink = response.body().getData().getUrl();
                                    }
                                }
                            }

                        } catch (Exception exception) {
                            exception.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<Playdata> call, @NonNull Throwable t) {
                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        isActivityLeft = true;
        if (nativeAds != null) {
            nativeAds.destroy();
        }
    }
}