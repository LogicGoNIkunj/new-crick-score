package com.ltos.livecrkttv.fragments;

import static android.view.View.VISIBLE;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.ltos.livecrkttv.R;
import com.ltos.livecrkttv.interfaces.APIService;
import com.ltos.livecrkttv.model.MatchesModel;
import com.ltos.livecrkttv.model.Scorecard;
import com.ltos.livecrkttv.model.Team_Model;
import com.ltos.livecrkttv.utils.MyApplication;
import com.google.gson.Gson;
import com.skydoves.expandablelayout.ExpandableLayout;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MatchInfoFragment extends Fragment {
    static String smatchdata;
    JSONObject compjsonObject;
    JSONObject homejsonObject;
    JSONObject awayjsonObject;
    JSONObject vanuejsonObject;
    static ArrayList<MatchesModel.Datas> mdatasArrayList  = new ArrayList<>();
    static int mposition;
    String fixture_data, home_team_id, away_team_id;
    ExpandableLayout expandable1, expandable2;
    TextView teamName, teamName2, iswinnertoss, iswinnertoss2;
    RecyclerView rvteam1, rvteam2;
    LinearLayout ll_team;
    final ArrayList<Team_Model> arrayList = new ArrayList<>();
    final ArrayList<Team_Model> arrayList2 = new ArrayList<>();

    public static void getInstance(String matchdata, ArrayList<MatchesModel.Datas> datasArrayList, int position) {
        try {
            smatchdata = matchdata;
            mdatasArrayList = datasArrayList;
            mposition = position;
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }


    public MatchInfoFragment() {
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_match_info, container, false);
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        view.findViewById(R.id.cv).setLayerType(View.LAYER_TYPE_SOFTWARE, null);

        ll_team = view.findViewById(R.id.ll_team);
        expandable1 = view.findViewById(R.id.expandable1);
        teamName = expandable1.parentLayout.findViewById(R.id.teamName);
        iswinnertoss = expandable1.parentLayout.findViewById(R.id.iswinnertoss);
        rvteam1 = expandable1.secondLayout.findViewById(R.id.rvteam1);
        expandable2 = view.findViewById(R.id.expandable2);
        teamName2 = expandable2.parentLayout.findViewById(R.id.teamName2);
        iswinnertoss2 = expandable2.parentLayout.findViewById(R.id.iswinnertoss2);
        rvteam2 = expandable2.secondLayout.findViewById(R.id.rvteam2);

        try {
            getCommentry();
        } catch (Exception e) {
            e.printStackTrace();
        }

        expandable1.setSpinnerDrawable(getResources().getDrawable(R.drawable.baseline_keyboard_arrow_down_black_24dp));
        expandable1.setSpinnerColor(getResources().getColor(R.color.black));
        expandable1.setSpinnerMargin(20);
        expandable2.setSpinnerDrawable(getResources().getDrawable(R.drawable.baseline_keyboard_arrow_down_black_24dp));
        expandable2.setSpinnerColor(getResources().getColor(R.color.black));
        expandable2.setSpinnerMargin(20);
        expandable1.setOnExpandListener(b -> {
            rvteam1.setLayoutManager(new GridLayoutManager(requireActivity(), 3));
            Team_Adapter team_adapter = new Team_Adapter(requireActivity(), arrayList);
            rvteam1.setAdapter(team_adapter);
        });
        expandable2.setOnExpandListener(b -> {
            rvteam2.setLayoutManager(new GridLayoutManager(requireActivity(), 3));
            Team_Adapter team_adapter = new Team_Adapter(requireActivity(), arrayList2);
            rvteam2.setAdapter(team_adapter);
        });
        expandable1.parentLayout.setOnClickListener(view1 -> expandable1.toggleLayout());
        expandable2.parentLayout.setOnClickListener(view12 -> expandable2.toggleLayout());

        try {
            compjsonObject = (new JSONObject(smatchdata)).getJSONObject("Competition");
            homejsonObject = (new JSONObject(smatchdata)).getJSONObject("HomeTeam");
            awayjsonObject = (new JSONObject(smatchdata)).getJSONObject("AwayTeam");
            vanuejsonObject = (new JSONObject(smatchdata)).getJSONObject("Venue");


            home_team_id = homejsonObject.getString("Id");
            away_team_id = awayjsonObject.getString("Id");

            teamName.setText(homejsonObject.getString("Name"));
            teamName2.setText(awayjsonObject.getString("Name"));
            if (homejsonObject.getBoolean("IsTossWinner")) {
                iswinnertoss.setVisibility(VISIBLE);
            } else {
                iswinnertoss2.setVisibility(VISIBLE);
            }

            ((TextView) view.findViewById(R.id.tv_series)).setText(compjsonObject.getString("Name"));
            ((TextView) view.findViewById(R.id.tv_date)).setText(String.format("%s - %s", parseDateToddMMyyyy(compjsonObject.getString("StartDateTime")), parseDateToddMMyyyy(compjsonObject.getString("EndDateTime"))));

            String vanue = vanuejsonObject.getString("Name");
            ((TextView) view.findViewById(R.id.tv_vanue)).setText((vanue.substring(vanue.indexOf(",") + 1)).trim());
            ((TextView) view.findViewById(R.id.tv_stadium)).setText(vanuejsonObject.getString("Name"));

            ((TextView) view.findViewById(R.id.tv_match_details)).setText(mdatasArrayList.get(mposition).getName());
            ((TextView) view.findViewById(R.id.tv_toss)).setText(mdatasArrayList.get(mposition).getTossResult());

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void getCommentry() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(MyApplication.get_cricApi())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        APIService service = retrofit.create(APIService.class);
        Call<Scorecard> call = service.getScorecard(mdatasArrayList.get(mposition).getId(), "eccn:true");
        call.enqueue(new Callback<Scorecard>() {
            @Override
            public void onResponse(@NonNull Call<Scorecard> call, @NonNull Response<Scorecard> response) {
                try {
                    if (response.body() != null) {
                        Gson gson = new Gson();
                        fixture_data = gson.toJson(response.body());
                        try {
                            JSONArray jsonArray = new JSONObject(fixture_data).getJSONArray("players");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject = jsonArray.getJSONObject(i);
                                if (jsonObject.getString("teamId").equals(home_team_id)) {
                                    Team_Model team_model = new Team_Model();
                                    team_model.setImage(jsonObject.getString("imageUrl"));
                                    team_model.setName(jsonObject.getString("displayName"));
                                    team_model.setCaptain(jsonObject.getBoolean("isCaptain"));
                                    team_model.setWicketKeeper(jsonObject.getBoolean("isWicketKeeper"));
                                    arrayList.add(team_model);

                                } else {
                                    Team_Model team_model = new Team_Model();
                                    team_model.setImage(jsonObject.getString("imageUrl"));
                                    team_model.setName(jsonObject.getString("displayName"));
                                    team_model.setCaptain(jsonObject.getBoolean("isCaptain"));
                                    team_model.setWicketKeeper(jsonObject.getBoolean("isWicketKeeper"));
                                    arrayList2.add(team_model);

                                }
                            }
                            if (arrayList.size() > 0) {
                                ll_team.setVisibility(VISIBLE);
                            } else {
                                ll_team.setVisibility(View.GONE);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(@NotNull Call<Scorecard> call, @NotNull Throwable t) {
                ll_team.setVisibility(View.GONE);
            }
        });
    }


    @SuppressLint("SimpleDateFormat")
    public String parseDateToddMMyyyy(String time) {
        String inputPattern = "yyyy-MM-dd'T'HH:mm:ss'Z'";
        String outputPattern = "EEE, MMM d";

        String date = null;
        try {
            @SuppressLint("SimpleDateFormat") SimpleDateFormat format = new SimpleDateFormat(inputPattern);
            Date newDate = format.parse(time);

            format = new SimpleDateFormat(outputPattern);
            if (newDate != null) {
                date = format.format(newDate);
            }
            return date;
        } catch (ParseException e) {
            return time;
        }
    }

    public static class Team_Adapter extends RecyclerView.Adapter<Team_Adapter.myh> {
        final Context context;
        final ArrayList<Team_Model> arrayList;

        public Team_Adapter(Context context, ArrayList<Team_Model> arrayList) {
            this.context = context;
            this.arrayList = arrayList;
        }

        @NonNull
        @Override
        public myh onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(context).inflate(R.layout.team_item, parent, false);
            return new myh(view);
        }

        @Override
        public void onBindViewHolder(@NonNull myh holder, int position) {
            Glide.with(context).load(arrayList.get(position).getImage()).placeholder(R.drawable.manplaceholder).error(R.drawable.manplaceholder).into(holder.playerimg);
            holder.tctname.setText(arrayList.get(position).getName());
            if (arrayList.get(position).isCaptain()) {
                holder.iscap.setVisibility(VISIBLE);
                holder.iscap.setText("C");
            } else if (arrayList.get(position).isWicketKeeper()) {
                holder.iscap.setVisibility(VISIBLE);
                holder.iscap.setText("W");
            }
        }

        @Override
        public int getItemCount() {
            return arrayList.size();
        }

        private static class myh extends RecyclerView.ViewHolder {
            final ImageView playerimg;
            final TextView tctname;
            final TextView iscap;

            public myh(@NonNull View itemView) {
                super(itemView);
                tctname = itemView.findViewById(R.id.tctname);
                playerimg = itemView.findViewById(R.id.playerimg);
                iscap = itemView.findViewById(R.id.iscap);
            }
        }
    }
}