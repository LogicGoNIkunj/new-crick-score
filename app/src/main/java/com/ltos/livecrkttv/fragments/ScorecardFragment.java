package com.ltos.livecrkttv.fragments;

import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.ltos.livecrkttv.R;
import com.ltos.livecrkttv.adapter.ScorecardAdapter;
import com.ltos.livecrkttv.interfaces.APIService;
import com.ltos.livecrkttv.model.MatchesModel;
import com.ltos.livecrkttv.model.Scorecard;
import com.ltos.livecrkttv.utils.MyApplication;
import com.google.gson.Gson;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Objects;


import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class ScorecardFragment extends Fragment {
    static ArrayList<MatchesModel.Datas> mdatasArrayList = new ArrayList<>();
    static int mposition;
    ProgressBar mypb;
    ScorecardAdapter matchAdapter;
    RecyclerView rv_team_details;
    TextView tv_status;
    LinearLayoutManager linearLayoutManager;
    static ArrayList<Scorecard.Fixture.Innings> inningsArrayList = new ArrayList<>();
    TextView tv_no_records;
    SwipeRefreshLayout swipeRefreshLayout;
    final Handler handler = new Handler();
    Runnable runnable;
    final int delay = 20000;

    public static void getInstance(ArrayList<MatchesModel.Datas> datasArrayList, int position) {
        try {
            mdatasArrayList = datasArrayList;
            mposition = position;
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    public ScorecardFragment() {
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_scorecard, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mypb = view.findViewById(R.id.mypb);
        tv_status = view.findViewById(R.id.tv_status);

        rv_team_details = view.findViewById(R.id.rv_team_details);
        linearLayoutManager = new LinearLayoutManager(getActivity());
        rv_team_details.setLayoutManager(linearLayoutManager);
        tv_no_records = view.findViewById(R.id.tv_no_records);

        handler.postDelayed(runnable = new Runnable() {
            public void run() {
                getCommentry();
                handler.postDelayed(this, delay);
            }
        }, delay);

        try {
            if (mdatasArrayList.get(mposition).getGameStatusId().equals("Prematch")) {
                tv_status.setVisibility(View.GONE);
                rv_team_details.setVisibility(View.GONE);
                tv_no_records.setVisibility(View.VISIBLE);
                tv_no_records.setText(R.string.no_started);
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }
        getCommentry();

        swipeRefreshLayout = view.findViewById(R.id.swipeToRefresh);
        swipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary);
        swipeRefreshLayout.setOnRefreshListener(() -> {
            try {
                if (mdatasArrayList.get(mposition).getGameStatusId().equals("Prematch")) {
                    tv_status.setVisibility(View.GONE);
                    rv_team_details.setVisibility(View.GONE);
                    tv_no_records.setVisibility(View.VISIBLE);
                    tv_no_records.setText(R.string.no_started);
                }
            } catch (Exception exception) {
                exception.printStackTrace();
            }
            getCommentry();
            swipeRefreshLayout.setRefreshing(false);
        });

        rv_team_details.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int topRowVerticalPosition = recyclerView.getChildCount() == 0 ? 0 : recyclerView.getChildAt(0).getTop();
                swipeRefreshLayout.setEnabled(topRowVerticalPosition >= 0);
            }
        });
    }

    private void getCommentry() {
        mypb.setVisibility(View.VISIBLE);
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(MyApplication.get_cricApi())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        APIService service = retrofit.create(APIService.class);
        Call<Scorecard> call = service.getScorecard(mdatasArrayList.get(mposition).getId(), "eccn:true");
        call.enqueue(new Callback<Scorecard>() {
            @Override
            public void onResponse(@NonNull Call<Scorecard> call, @NonNull Response<Scorecard> response) {
                mypb.setVisibility(View.GONE);
                try {
                    if (response.body() != null) {
                        inningsArrayList = response.body().getFixture().getInnings();
                        Gson gson = new Gson();
                        String fixture_data = gson.toJson(response.body());
                        tv_status.setText(response.body().getFixture().getResultText());
                        if (inningsArrayList.size() == 0) {
                            tv_no_records.setVisibility(View.VISIBLE);
                            tv_no_records.setText(R.string.no_record);
                        } else {
                            tv_no_records.setVisibility(View.GONE);
                        }
                        rv_team_details.setVisibility(View.VISIBLE);
                        matchAdapter = new ScorecardAdapter(getContext(), inningsArrayList, fixture_data);
                        rv_team_details.setAdapter(matchAdapter);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(@NotNull Call<Scorecard> call, @NotNull Throwable t) {
                mypb.setVisibility(View.GONE);
                tv_no_records.setVisibility(View.VISIBLE);
                rv_team_details.setVisibility(View.GONE);
                if (Objects.requireNonNull(t.getMessage()).contains("Unable to resolve host") || t.getMessage().contains("failed to connect")) {
                    tv_no_records.setText(R.string.no_internet_connection);
                } else {
                    tv_no_records.setText(R.string.no_record);
                }
            }
        });
    }

    @Override
    public void onPause() {
        handler.removeCallbacks(runnable);
        super.onPause();
    }

    @Override
    public void onDestroy() {
        handler.removeCallbacks(runnable);
        super.onDestroy();
    }
}