package com.ltos.livecrkttv.fragments;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.ltos.livecrkttv.R;
import com.ltos.livecrkttv.adapter.CommentaryAdapter;
import com.ltos.livecrkttv.interfaces.APIService;
import com.ltos.livecrkttv.model.CommentaryModel;
import com.ltos.livecrkttv.model.MatchesModel;
import com.ltos.livecrkttv.utils.MyApplication;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Objects;
import java.util.TimeZone;


import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class LiveInfoFragment extends Fragment {
    RecyclerView rv_commentry;
    static ArrayList<MatchesModel.Datas> mdatasArrayList = new ArrayList<>();
    public static final ArrayList<CommentaryModel.Innings.Overs> updatelistiem = new ArrayList<>();
    ArrayList<CommentaryModel.Innings.Overs> oversArrayList;
    static int mposition;
    static String smatchdata;
    JSONObject compjsonObject;
    JSONObject homejsonObject;
    JSONObject awayjsonObject;
    JSONObject vanuejsonObject;
    JSONArray inningjsonArray;
    TextView tv_inning1, tv_country_inning1, tv_country_name, tv_inning2, tv_score, tv_runrate, tv_result, tv_rqrunrate, tv_upcoming_status, tv_inning3, tv_inning4;
    boolean isFetching = false;
    boolean isLoading = false;
    ProgressBar mypb;
    LinearLayoutManager linearLayoutManager;
    CommentaryAdapter matchAdapter;
    SwipeRefreshLayout swipeRefreshLayout;
    final Handler handler = new Handler();
    Runnable runnable;
    final int delay = 20000;
    TextView tv_no_records;

    public static void getInstance(String matchdata, ArrayList<MatchesModel.Datas> datasArrayList, int position) {
        try {
            smatchdata = matchdata;
            mdatasArrayList = datasArrayList;
            mposition = position;
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    public LiveInfoFragment() {
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_live_info, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        tv_no_records = view.findViewById(R.id.tv_no_records);
        rv_commentry = view.findViewById(R.id.rv_commentry);
        tv_inning1 = view.findViewById(R.id.tv_inning1);
        tv_country_inning1 = view.findViewById(R.id.tv_country_inning1);
        tv_country_name = view.findViewById(R.id.tv_country_name);
        tv_inning2 = view.findViewById(R.id.tv_inning2);
        tv_inning3 = view.findViewById(R.id.tv_inning3);
        tv_inning4 = view.findViewById(R.id.tv_inning4);
        tv_score = view.findViewById(R.id.tv_score);
        tv_runrate = view.findViewById(R.id.tv_runrate);
        tv_rqrunrate = view.findViewById(R.id.tv_rqrunrate);
        tv_result = view.findViewById(R.id.tv_result);
        tv_upcoming_status = view.findViewById(R.id.tv_upcoming_status);
        mypb = view.findViewById(R.id.mypb);

        linearLayoutManager = new LinearLayoutManager(getActivity());
        rv_commentry.setLayoutManager(linearLayoutManager);

        try {
            tv_result.setText(mdatasArrayList.get(mposition).getResultText());

            compjsonObject = (new JSONObject(smatchdata)).getJSONObject("Competition");
            homejsonObject = (new JSONObject(smatchdata)).getJSONObject("HomeTeam");
            awayjsonObject = (new JSONObject(smatchdata)).getJSONObject("AwayTeam");
            vanuejsonObject = (new JSONObject(smatchdata)).getJSONObject("Venue");
            inningjsonArray = new JSONObject(smatchdata).getJSONArray("Innings");

            if (inningjsonArray.length() == 1) {
                JSONObject inObject1 = inningjsonArray.getJSONObject(0);
                if (inObject1.getInt("BattingTeamId") == homejsonObject.getInt("Id")) {
                    tv_country_inning1.setText(String.format("%s", homejsonObject.getString("ShortName")));
                    tv_country_name.setVisibility(View.GONE);
                    tv_country_inning1.setTextColor(Color.BLACK);
                    tv_inning1.setText(R.string._1st_inn);
                } else if (inObject1.getInt("BattingTeamId") == awayjsonObject.getInt("Id")) {
                    tv_country_name.setText(String.format("%s", awayjsonObject.getString("ShortName")));
                    tv_country_inning1.setVisibility(View.GONE);
                    tv_country_name.setTextColor(Color.BLACK);
                    tv_inning2.setText(R.string._1st_inn);
                }
            } else if (inningjsonArray.length() == 2) {
                JSONObject inObject1 = inningjsonArray.getJSONObject(0);
                if (inObject1.getInt("BattingTeamId") == homejsonObject.getInt("Id")) {
                    tv_country_name.setText(String.format("%s", homejsonObject.getString("ShortName")));
                    tv_country_inning1.setText(String.format("%s", awayjsonObject.getString("ShortName")));
                    tv_inning1.setText(String.format("%s-%s (%s)", inObject1.getInt("RunsScored"), inObject1.getInt("NumberOfWicketsFallen"), inObject1.getInt("OversBowled")));
                } else if (inObject1.getInt("BattingTeamId") == awayjsonObject.getInt("Id")) {
                    tv_country_inning1.setText(String.format("%s", homejsonObject.getString("ShortName")));
                    tv_country_name.setText(String.format("%s", awayjsonObject.getString("ShortName")));
                    tv_inning1.setText(String.format("%s-%s (%s)", inObject1.getInt("RunsScored"), inObject1.getInt("NumberOfWicketsFallen"), inObject1.getInt("OversBowled")));
                }
                JSONObject inObject2 = inningjsonArray.getJSONObject(1);
                if (inObject2.getInt("BattingTeamId") == homejsonObject.getInt("Id")) {
                    tv_country_name.setText(String.format("%s", homejsonObject.getString("ShortName")));
                    tv_country_inning1.setText(String.format("%s", awayjsonObject.getString("ShortName")));
                    tv_country_name.setTextColor(Color.BLACK);
                    tv_inning1.setText(R.string._2nd_in);
                } else if (inObject2.getInt("BattingTeamId") == awayjsonObject.getInt("Id")) {
                    tv_country_inning1.setText(String.format("%s", homejsonObject.getString("ShortName")));
                    tv_country_name.setText(String.format("%s", awayjsonObject.getString("ShortName")));
                    tv_country_name.setTextColor(Color.BLACK);
                    tv_inning2.setText(R.string._2nd_in);
                }
            } else if (inningjsonArray.length() == 3) {
                JSONObject inObject1 = inningjsonArray.getJSONObject(0);
                JSONObject inObject2 = inningjsonArray.getJSONObject(1);
                JSONObject inObject3 = inningjsonArray.getJSONObject(2);

                if (inObject1.getInt("BattingTeamId") == homejsonObject.getInt("Id")) {
                    tv_country_name.setText(String.format("%s", homejsonObject.getString("ShortName")));
                    tv_country_inning1.setText(String.format("%s", awayjsonObject.getString("ShortName")));
                    tv_inning1.setText(String.format("%s-%s (%s)", inObject1.getInt("RunsScored"), inObject1.getInt("NumberOfWicketsFallen"), inObject1.getInt("OversBowled")));
                } else if (inObject1.getInt("BattingTeamId") == awayjsonObject.getInt("Id")) {
                    tv_country_inning1.setText(String.format("%s", homejsonObject.getString("ShortName")));
                    tv_country_name.setText(String.format("%s", awayjsonObject.getString("ShortName")));
                    tv_inning1.setText(String.format("%s-%s (%s)", inObject1.getInt("RunsScored"), inObject1.getInt("NumberOfWicketsFallen"), inObject1.getInt("OversBowled")));
                }
                if (inObject2.getInt("BattingTeamId") == homejsonObject.getInt("Id")) {
                    tv_country_name.setText(String.format("%s", homejsonObject.getString("ShortName")));
                    tv_country_inning1.setText(String.format("%s", awayjsonObject.getString("ShortName")));
                    tv_country_inning1.setTextColor(Color.BLACK);
                    tv_inning2.setText(String.format("%s-%s (%s)", inObject2.getInt("RunsScored"), inObject2.getInt("NumberOfWicketsFallen"), inObject2.getInt("OversBowled")));
                } else if (inObject2.getInt("BattingTeamId") == awayjsonObject.getInt("Id")) {
                    tv_country_inning1.setText(String.format("%s", homejsonObject.getString("ShortName")));
                    tv_country_name.setText(String.format("%s", awayjsonObject.getString("ShortName")));
                    tv_country_name.setTextColor(Color.BLACK);
                    tv_inning2.setText(String.format("%s-%s (%s)", inObject2.getInt("RunsScored"), inObject2.getInt("NumberOfWicketsFallen"), inObject2.getInt("OversBowled")));
                }

                if (inObject3.getInt("BattingTeamId") == homejsonObject.getInt("Id")) {
                    tv_country_inning1.setTextColor(Color.BLACK);
                    tv_inning3.setText(String.format(" & %s-%s (%s)", inObject3.getInt("RunsScored"), inObject3.getInt("NumberOfWicketsFallen"), inObject3.getInt("OversBowled")));
                } else if (inObject3.getInt("BattingTeamId") == awayjsonObject.getInt("Id")) {
                    tv_country_name.setTextColor(Color.BLACK);
                    tv_inning3.setText(String.format(" & %s-%s (%s)", inObject3.getInt("RunsScored"), inObject3.getInt("NumberOfWicketsFallen"), inObject3.getInt("OversBowled")));
                }
            } else if (inningjsonArray.length() == 4) {
                JSONObject inObject1 = inningjsonArray.getJSONObject(0);
                JSONObject inObject2 = inningjsonArray.getJSONObject(1);
                JSONObject inObject3 = inningjsonArray.getJSONObject(2);
                JSONObject inObject4 = inningjsonArray.getJSONObject(3);
                tv_score.setVisibility(View.GONE);
                if (inObject1.getInt("BattingTeamId") == homejsonObject.getInt("Id")) {
                    tv_country_name.setText(String.format("%s", homejsonObject.getString("ShortName")));
                    tv_country_inning1.setText(String.format("%s", awayjsonObject.getString("ShortName")));
                    tv_inning1.setText(String.format("%s-%s (%s)", inObject1.getInt("RunsScored"), inObject1.getInt("NumberOfWicketsFallen"), inObject1.getInt("OversBowled")));
                } else if (inObject1.getInt("BattingTeamId") == awayjsonObject.getInt("Id")) {
                    tv_country_inning1.setText(String.format("%s", homejsonObject.getString("ShortName")));
                    tv_country_name.setText(String.format("%s", awayjsonObject.getString("ShortName")));
                    tv_inning1.setText(String.format("%s-%s (%s)", inObject1.getInt("RunsScored"), inObject1.getInt("NumberOfWicketsFallen"), inObject1.getInt("OversBowled")));
                }
                if (inObject2.getInt("BattingTeamId") == homejsonObject.getInt("Id")) {
                    tv_country_name.setText(String.format("%s", homejsonObject.getString("ShortName")));
                    tv_country_inning1.setText(String.format("%s", awayjsonObject.getString("ShortName")));
                    tv_country_name.setTextColor(Color.BLACK);
                    tv_inning2.setText(String.format("%s-%s (%s)", inObject2.getInt("RunsScored"), inObject2.getInt("NumberOfWicketsFallen"), inObject2.getInt("OversBowled")));
                } else if (inObject2.getInt("BattingTeamId") == awayjsonObject.getInt("Id")) {
                    tv_country_inning1.setText(String.format("%s", homejsonObject.getString("ShortName")));
                    tv_country_name.setText(String.format("%s", awayjsonObject.getString("ShortName")));
                    tv_country_name.setTextColor(Color.BLACK);
                    tv_inning2.setText(String.format("%s-%s (%s)", inObject2.getInt("RunsScored"), inObject2.getInt("NumberOfWicketsFallen"), inObject2.getInt("OversBowled")));
                }

                if (inObject3.getInt("BattingTeamId") == homejsonObject.getInt("Id")) {
                    tv_inning3.setText(String.format(" & %s-%s (%s)", inObject3.getInt("RunsScored"), inObject3.getInt("NumberOfWicketsFallen"), inObject3.getInt("OversBowled")));
                } else if (inObject3.getInt("BattingTeamId") == awayjsonObject.getInt("Id")) {
                    tv_inning3.setText(String.format(" & %s-%s (%s)", inObject3.getInt("RunsScored"), inObject3.getInt("NumberOfWicketsFallen"), inObject3.getInt("OversBowled")));
                }

                if (!(mdatasArrayList.get(mposition).getGameStatusId().equals("InPlay"))) {
                    if (inObject4.getInt("BattingTeamId") == homejsonObject.getInt("Id")) {
                        tv_inning4.setText(String.format(" & %s-%s (%s)", inObject4.getInt("RunsScored"), inObject4.getInt("NumberOfWicketsFallen"), inObject4.getInt("OversBowled")));
                    } else if (inObject4.getInt("BattingTeamId") == awayjsonObject.getInt("Id")) {
                        tv_inning4.setText(String.format(" & %s-%s (%s)", inObject4.getInt("RunsScored"), inObject4.getInt("NumberOfWicketsFallen"), inObject4.getInt("OversBowled")));
                    }
                } else {
                    tv_score.setVisibility(View.VISIBLE);
                }
            }
            if (mdatasArrayList.get(mposition).getGameStatusId().equals("Prematch")) {
                view.findViewById(R.id.ll1).setVisibility(View.GONE);
                tv_result.setVisibility(View.GONE);
                tv_country_inning1.setVisibility(View.GONE);
                tv_country_name.setVisibility(View.GONE);
                tv_upcoming_status.setText(String.format("Starts on %s\n\n%s", parseDateToddMMyyyy(mdatasArrayList.get(mposition).getStartDateTime()), mdatasArrayList.get(mposition).getResultText()));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        new Thread() {
            public void run() {
                try {
                    requireActivity().runOnUiThread(() -> getCommentry());
                    Thread.sleep(300);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }.start();

        handler.postDelayed(runnable = new Runnable() {
            public void run() {
                getCommentry();
                handler.postDelayed(this, delay);
            }
        }, delay);


        swipeRefreshLayout = view.findViewById(R.id.swipeToRefresh);
        swipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary);
        swipeRefreshLayout.setOnRefreshListener(() -> {
            getCommentry();
            swipeRefreshLayout.setRefreshing(false);
        });
        rv_commentry.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int topRowVerticalPosition = recyclerView.getChildCount() == 0 ? 0 : recyclerView.getChildAt(0).getTop();
                swipeRefreshLayout.setEnabled(topRowVerticalPosition >= 0);
            }
        });
    }

    private void getCommentry() {
        mypb.setVisibility(View.VISIBLE);
        if (!this.isFetching) {
            isFetching = true;
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(MyApplication.get_cricApi())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            APIService service = retrofit.create(APIService.class);
            Call<CommentaryModel> call = service.getComments(mdatasArrayList.get(mposition).getId(), "eccn:true", 5, "", true, inningjsonArray.length(), "");
            call.enqueue(new Callback<CommentaryModel>() {
                @Override
                public void onResponse(@NonNull Call<CommentaryModel> call, @NonNull Response<CommentaryModel> response) {
                    mypb.setVisibility(View.GONE);
                    try {
                        updatelistiem.clear();
                        if (oversArrayList != null) {
                            oversArrayList.clear();
                        }
                        isFetching = false;
                        if (response.body() != null) {
                            oversArrayList = response.body().getInnings().getOvers();
                            try {
                                tv_score.setText(String.format("%s-%s (%s)", oversArrayList.get(0).getTotalInningRuns(), oversArrayList.get(0).getTotalInningWickets(), oversArrayList.get(0).getOverNumber()));
                            } catch (Exception exception) {
                                exception.printStackTrace();
                            }

                            updatelistiem.addAll(oversArrayList);
                            rv_commentry.setVisibility(View.VISIBLE);
                            matchAdapter = new CommentaryAdapter(getContext(), updatelistiem, 1);
                            rv_commentry.setAdapter(matchAdapter);
                            int lastover = oversArrayList.get((oversArrayList.size() - 1)).getOverNumber();
                            initScrollListener(lastover);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NotNull Call<CommentaryModel> call, @NotNull Throwable t) {
                    mypb.setVisibility(View.GONE);
                    isFetching = false;
                    tv_no_records.setVisibility(View.VISIBLE);
                    rv_commentry.setVisibility(View.GONE);
                    if (Objects.requireNonNull(t.getMessage()).contains("Unable to resolve host") || t.getMessage().contains("failed to connect")) {
                        tv_no_records.setText(R.string.no_internet_connection);
                    } else {
                        tv_no_records.setText(R.string.no_record);
                    }
                }
            });
        }
    }

    private void initScrollListener(int lastover) {
        rv_commentry.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int topRowVerticalPosition = recyclerView.getChildCount() == 0 ? 0 : recyclerView.getChildAt(0).getTop();
                swipeRefreshLayout.setEnabled(topRowVerticalPosition >= 0);
                if (!isLoading) {
                    if (linearLayoutManager != null && linearLayoutManager.findLastCompletelyVisibleItemPosition() == updatelistiem.size() - 1) {
                        loadMore(lastover);
                        isLoading = true;
                    }
                }
            }
        });
    }

    private void loadMore(int lastover) {
        LoadMoreDataList(lastover);
    }

    private void LoadMoreDataList(int lastover) {
        try {
            mypb.setVisibility(View.VISIBLE);
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(MyApplication.get_cricApi())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            APIService service = retrofit.create(APIService.class);
            Call<CommentaryModel> call = service.getComments(mdatasArrayList.get(mposition).getId(), "eccn:true", 5, lastover + "", true, inningjsonArray.length(), "");
            call.enqueue(new Callback<CommentaryModel>() {
                @Override
                public void onResponse(@NonNull Call<CommentaryModel> call, @NonNull Response<CommentaryModel> response) {
                    try {
                        rv_commentry.setVisibility(View.VISIBLE);
                        Handler handler = new Handler();
                        handler.postDelayed(() -> {
                            isFetching = false;
                            if (response.body() != null) {
                                oversArrayList = response.body().getInnings().getOvers();
                                int currentSize;
                                currentSize = updatelistiem.size();
                                int nextLimit = currentSize + oversArrayList.size();
                                int a = 0;
                                for (int i = updatelistiem.size(); i < nextLimit; i++) {
                                    updatelistiem.add(oversArrayList.get(a));
                                    a++;
                                    matchAdapter.notifyItemChanged(i);
                                }
                                try {
                                    tv_score.setText(String.format("%s-%s (%s)", updatelistiem.get(0).getTotalInningRuns(), updatelistiem.get(0).getTotalInningWickets(), updatelistiem.get(0).getOverNumber()));
                                } catch (Exception exception) {
                                    exception.printStackTrace();
                                }
                                mypb.setVisibility(View.GONE);
                                isLoading = false;
                            }
                        }, 2000);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NotNull Call<CommentaryModel> call, @NotNull Throwable t) {
                    mypb.setVisibility(View.GONE);
                    tv_no_records.setVisibility(View.VISIBLE);
                    rv_commentry.setVisibility(View.GONE);
                    if (t.getMessage().contains("Unable to resolve host") || t.getMessage().contains("failed to connect")) {
                        tv_no_records.setText(R.string.no_internet_connection);
                    } else {
                        tv_no_records.setText(R.string.no_record);
                    }
                }
            });
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    @SuppressLint("SimpleDateFormat")
    public String parseDateToddMMyyyy(String time) {
        String inputPattern = "yyyy-MM-dd'T'HH:mm:ss'Z'";
        String outputPattern = "EEE, MMM d hh:mm aaa";

        String date = null;
        try {
            @SuppressLint("SimpleDateFormat") SimpleDateFormat format = new SimpleDateFormat(inputPattern);
            format.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date newDate = format.parse(time);

            format = new SimpleDateFormat(outputPattern);

            if (newDate != null) {
                date = format.format(newDate);
            }
            return date;
        } catch (ParseException e) {
            return time;
        }
    }

    @Override
    public void onPause() {
        handler.removeCallbacks(runnable);
        super.onPause();
    }

    @Override
    public void onDestroy() {
        handler.removeCallbacks(runnable);
        super.onDestroy();
    }

}