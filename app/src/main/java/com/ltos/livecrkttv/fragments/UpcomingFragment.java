package com.ltos.livecrkttv.fragments;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static com.ltos.livecrkttv.activity.LiveCricketActivity.click;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.ltos.livecrkttv.R;
import com.ltos.livecrkttv.activity.MatchInfoActivity;
import com.ltos.livecrkttv.adapter.MatchAdapter;
import com.ltos.livecrkttv.interfaces.APIService;
import com.ltos.livecrkttv.model.MatchesModel;
import com.ltos.livecrkttv.utils.MyApplication;
import com.ltos.livecrkttv.utils.RecyclerTouchListener;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.gson.Gson;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class UpcomingFragment extends Fragment {
    RecyclerView rv_matches;
    ProgressBar mypb;
    SwipeRefreshLayout swipeRefreshLayout;
    LinearLayout ll_no_wifi;
    private LinearLayout nativeAdContainer;
    TextView tv_no_data;
    Handler handlerint;
    public static ArrayList<MatchesModel.Datas> arrayList = new ArrayList<>();
    int countUpcoming;

    ShimmerFrameLayout shimmer_view_container;

    public UpcomingFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        handlerint = new Handler();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_live_match, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        shimmer_view_container = view.findViewById(R.id.shimmer_view_container);
        tv_no_data = view.findViewById(R.id.tv_no_data);
        rv_matches = view.findViewById(R.id.rv_match);
        mypb = view.findViewById(R.id.mypb);
        ll_no_wifi = view.findViewById(R.id.ll_no_wifi);
        TextView tv_matches = view.findViewById(R.id.tv_matches);
        tv_matches.setText(R.string.upcomingm);

        view.findViewById(R.id.btn_try_again).setOnClickListener(view1 -> loadData());

        LinearLayoutManager linearLayoutManager = new GridLayoutManager(requireActivity(), 1);
        rv_matches.setLayoutManager(linearLayoutManager);
        loadData();

        swipeRefreshLayout = view.findViewById(R.id.swipeToRefresh);
        swipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary);
        swipeRefreshLayout.setOnRefreshListener(() -> {
            loadData();
            swipeRefreshLayout.setRefreshing(false);
        });

        rv_matches.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int topRowVerticalPosition = recyclerView.getChildCount() == 0 ? 0 : recyclerView.getChildAt(0).getTop();
                swipeRefreshLayout.setEnabled(topRowVerticalPosition >= 0);
            }
        });

        nativeAdContainer = view.findViewById(R.id.nativeAdContainer);

        rv_matches.addOnItemTouchListener(new RecyclerTouchListener(requireActivity(), rv_matches, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                try {
                    click++;
                    Gson gson = new Gson();
                    String matchdata = gson.toJson(arrayList.get(position));

                    if (click == 1) {
                        try {
                            handlerint.postDelayed(() -> {
                                try {
                                    Intent intent = new Intent(requireActivity(), MatchInfoActivity.class);
                                    intent.putParcelableArrayListExtra("MATCHDETAILS", arrayList);
                                    intent.putExtra("POSITION", position);
                                    intent.putExtra("abc", "pqr");
                                    intent.putExtra("MATCHDATA", matchdata);
                                    intent.putExtra("homecolor", arrayList.get(position).getHomeTeam().getTeamColor());
                                    intent.putExtra("awaycolor", arrayList.get(position).getAwayTeam().getTeamColor());
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                                    requireActivity().startActivity(intent);
                                    countUpcoming++;

                                } catch (Exception exception) {
                                    exception.printStackTrace();
                                }
                            }, 4000);
                        } catch (Exception ignored) {
                        }
                    } else if (click == 5) {
                        click = 1;
                        try {
                            handlerint.postDelayed(() -> {
                                try {
                                    Intent intent = new Intent(requireActivity(), MatchInfoActivity.class);
                                    intent.putParcelableArrayListExtra("MATCHDETAILS", arrayList);
                                    intent.putExtra("POSITION", position);
                                    intent.putExtra("abc", "pqr");
                                    intent.putExtra("homecolor", arrayList.get(position).getHomeTeam().getTeamColor());
                                    intent.putExtra("awaycolor", arrayList.get(position).getAwayTeam().getTeamColor());
                                    intent.putExtra("MATCHDATA", matchdata);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                                    requireActivity().startActivity(intent);
                                    countUpcoming++;

                                } catch (Exception exception) {
                                    exception.printStackTrace();
                                }
                            }, 4000);
                        } catch (Exception ignored) {
                        }
                    } else {
                        Intent intent = new Intent(requireActivity(), MatchInfoActivity.class);
                        intent.putParcelableArrayListExtra("MATCHDETAILS", arrayList);
                        intent.putExtra("POSITION", position);
                        intent.putExtra("abc", "pqr");
                        intent.putExtra("homecolor", arrayList.get(position).getHomeTeam().getTeamColor());
                        intent.putExtra("awaycolor", arrayList.get(position).getAwayTeam().getTeamColor());
                        intent.putExtra("MATCHDATA", matchdata);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                        requireActivity().startActivity(intent);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
    }

    @SuppressLint("StaticFieldLeak")
    public void loadData() {
        new AsyncTask<Void, Void, Boolean>() {
            public Boolean doInBackground(Void[] objArr) {
                getLiveMatchData();
                return true;
            }

            public void onPostExecute(Boolean obj) {
                mypb.setVisibility(View.GONE);
            }

            public void onPreExecute() {
                super.onPreExecute();
                shimmer_view_container.startShimmer();
                ll_no_wifi.setVisibility(View.GONE);
                mypb.setVisibility(View.VISIBLE);
            }
        }.execute();
    }

    @SuppressLint("SetTextI18n")
    private void getLiveMatchData() {
        if (!MyApplication.get_cricApi().equals("")) {
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(MyApplication.get_cricApi())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            APIService service = retrofit.create(APIService.class);
            Call<MatchesModel> call = service.getCategories(12, 12, 12);
            call.enqueue(new Callback<MatchesModel>() {
                @Override
                public void onResponse(@NonNull Call<MatchesModel> call, @NonNull Response<MatchesModel> response) {
                    try {
                        if (response.body() != null) {
                            arrayList = response.body().getUpcomingFixtures();
                            Gson gson = new Gson();
                            String matchdata = gson.toJson(response.body());
                            if (arrayList.size() == 0) {
                                nativeAdContainer.removeAllViews();
                                nativeAdContainer.setVisibility(GONE);
                                tv_no_data.setVisibility(VISIBLE);
                                tv_no_data.setText(R.string.no_upcoming);
                                rv_matches.setVisibility(GONE);
                            } else {
                                tv_no_data.setVisibility(GONE);
                                rv_matches.setVisibility(VISIBLE);
                            }
                            shimmer_view_container.stopShimmer();
                            shimmer_view_container.setVisibility(GONE);
                            MatchAdapter matchAdapter = new MatchAdapter(getContext(), arrayList, matchdata, 3);
                            rv_matches.setAdapter(matchAdapter);
                        }
                    } catch (Exception e) {
                        shimmer_view_container.stopShimmer();
                        shimmer_view_container.setVisibility(GONE);
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NotNull Call<MatchesModel> call, @NotNull Throwable t) {
                    shimmer_view_container.stopShimmer();
                    shimmer_view_container.setVisibility(GONE);
                    mypb.setVisibility(View.GONE);
                    rv_matches.setVisibility(View.GONE);
                    if (Objects.requireNonNull(t.getMessage()).contains("Unable to resolve host") || t.getMessage().contains("failed to connect")) {
                        ll_no_wifi.setVisibility(View.VISIBLE);
                    } else {
                        ll_no_wifi.setVisibility(View.GONE);
                    }
                }
            });
        } else {
            shimmer_view_container.setVisibility(GONE);
            tv_no_data.setVisibility(VISIBLE);
            tv_no_data.setText("Something went wrong");
            rv_matches.setVisibility(GONE);
        }
    }

}