package com.ltos.livecrkttv.fragments;

import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.ltos.livecrkttv.R;
import com.ltos.livecrkttv.adapter.CommentaryAdapter;
import com.ltos.livecrkttv.interfaces.APIService;
import com.ltos.livecrkttv.model.CommentaryModel;
import com.ltos.livecrkttv.model.MatchesModel;
import com.ltos.livecrkttv.utils.MyApplication;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class HighlightsFragment extends Fragment {
    RecyclerView rv_commentry;
    static ArrayList<MatchesModel.Datas> mdatasArrayList = new ArrayList<>();
    public static final ArrayList<CommentaryModel.Innings.Overs> updatelistiem = new ArrayList<>();
    ArrayList<CommentaryModel.Innings.Overs> oversArrayList;
    static int mposition;
    static String smatchdata;
    JSONObject compjsonObject;
    JSONObject homejsonObject;
    JSONObject awayjsonObject;
    JSONObject vanuejsonObject;
    JSONArray inningjsonArray;
    TextView tv_no_records;
    boolean isFetching = false;
    boolean isLoading = false;
    ProgressBar mypb;
    LinearLayoutManager linearLayoutManager;
    Spinner spinner_country;
    Spinner spinner_highlights;
    final String[] hightlights = {"Four", "Sixes", "Wickets"};
    final String[] hightlights1 = {"four", "six", "wicket"};
    final ArrayList<String> innings = new ArrayList<>();
    public static int inning;
    public static String highlightss;
    LinearLayout ll_spinner;
    CommentaryAdapter matchAdapter;
    SwipeRefreshLayout swipeRefreshLayout;
    final Handler handler = new Handler();
    Runnable runnable;
    final int delay = 20000;

    public HighlightsFragment() {
    }

    public static void getInstance(String matchdata, ArrayList<MatchesModel.Datas> datasArrayList, int position) {
        try {
            smatchdata = matchdata;
            mdatasArrayList = datasArrayList;
            mposition = position;
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_highlights_info, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ll_spinner = view.findViewById(R.id.ll_spinner);
        rv_commentry = view.findViewById(R.id.rv_commentry);
        spinner_country = view.findViewById(R.id.spinner_country);
        spinner_highlights = view.findViewById(R.id.spinner_highlights);
        mypb = view.findViewById(R.id.mypb);
        tv_no_records = view.findViewById(R.id.tv_no_records);


        ArrayAdapter aa = new ArrayAdapter(getActivity(), android.R.layout.simple_spinner_item, hightlights);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_highlights.setAdapter(aa);

        linearLayoutManager = new LinearLayoutManager(getActivity());
        rv_commentry.setLayoutManager(linearLayoutManager);
        try {
            compjsonObject = (new JSONObject(smatchdata)).getJSONObject("Competition");
            homejsonObject = (new JSONObject(smatchdata)).getJSONObject("HomeTeam");
            awayjsonObject = (new JSONObject(smatchdata)).getJSONObject("AwayTeam");
            vanuejsonObject = (new JSONObject(smatchdata)).getJSONObject("Venue");
            inningjsonArray = new JSONObject(smatchdata).getJSONArray("Innings");

            if (inningjsonArray.length() != 0) {
                if (inningjsonArray.length() > 1) {
                    inning = inningjsonArray.length() - 1;
                } else {
                    inning = inningjsonArray.length();
                }
                highlightss = "four";

                for (int i = 0; i < inningjsonArray.length(); i++) {
                    innings.add("Inning " + (i + 1));
                }
                ArrayAdapter aa2 = new ArrayAdapter(getActivity(), android.R.layout.simple_spinner_item, innings);
                aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinner_country.setAdapter(aa2);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        spinner_country.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                inning = Integer.parseInt((innings.get(i)).replace("Inning ", ""));
                getCommentry(highlightss, inning);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        spinner_highlights.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                highlightss = hightlights1[i];
                getCommentry(highlightss, inning);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        getCommentry(highlightss, inning);
        try {
            if (mdatasArrayList.get(mposition).getGameStatusId().equals("Prematch")) {
                rv_commentry.setVisibility(View.GONE);
                ll_spinner.setVisibility(View.GONE);
                tv_no_records.setVisibility(View.VISIBLE);
                tv_no_records.setText(R.string.no_started);
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }
        swipeRefreshLayout = view.findViewById(R.id.swipeToRefresh);
        swipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary);
        swipeRefreshLayout.setOnRefreshListener(() -> {
            try {
                if (mdatasArrayList.get(mposition).getGameStatusId().equals("Prematch")) {
                    rv_commentry.setVisibility(View.GONE);
                    ll_spinner.setVisibility(View.GONE);
                    tv_no_records.setVisibility(View.VISIBLE);
                    tv_no_records.setText(R.string.no_started);
                }
            } catch (Exception exception) {
                exception.printStackTrace();
            }
            getCommentry(highlightss, inning);
            swipeRefreshLayout.setRefreshing(false);
        });
        rv_commentry.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int topRowVerticalPosition = recyclerView.getChildCount() == 0 ? 0 : recyclerView.getChildAt(0).getTop();
                swipeRefreshLayout.setEnabled(topRowVerticalPosition >= 0);
            }
        });


        handler.postDelayed(runnable = new Runnable() {
            public void run() {
                getCommentry(highlightss, inning);
                handler.postDelayed(this, delay);
            }
        }, delay);
    }


    private void getCommentry(String mhightlight, int innings) {
        mypb.setVisibility(View.VISIBLE);
        if (!this.isFetching) {
            isFetching = true;
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(MyApplication.get_cricApi())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            APIService service = retrofit.create(APIService.class);

            Call<CommentaryModel> call = service.getComments(mdatasArrayList.get(mposition).getId(), "eccn:true", 5, "", true, innings, mhightlight);
            call.enqueue(new Callback<CommentaryModel>() {
                @Override
                public void onResponse(@NonNull Call<CommentaryModel> call, @NonNull Response<CommentaryModel> response) {
                    mypb.setVisibility(View.GONE);
                    try {
                        updatelistiem.clear();
                        if (oversArrayList != null) {
                            oversArrayList.clear();
                        }
                        isFetching = false;
                        if (response.body() != null) {
                            oversArrayList = response.body().getInnings().getOvers();
                            if (oversArrayList.size() != 0) {
                                rv_commentry.setVisibility(View.VISIBLE);
                                updatelistiem.addAll(oversArrayList);
                                matchAdapter = new CommentaryAdapter(getContext(), updatelistiem, 2);
                                rv_commentry.setAdapter(matchAdapter);
                                int lastover = oversArrayList.get((oversArrayList.size() - 1)).getOverNumber();
                                if (lastover > 1) {
                                    initScrollListener(lastover, mhightlight, innings);
                                }
                                tv_no_records.setVisibility(View.GONE);
                            } else {
                                rv_commentry.setVisibility(View.GONE);
                                tv_no_records.setVisibility(View.VISIBLE);
                                tv_no_records.setText(R.string.no_record);
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NotNull Call<CommentaryModel> call, @NotNull Throwable t) {
                    tv_no_records.setVisibility(View.VISIBLE);
                    rv_commentry.setVisibility(View.GONE);
                    mypb.setVisibility(View.GONE);
                    isFetching = false;
                    if (Objects.requireNonNull(t.getMessage()).contains("Unable to resolve host") || t.getMessage().contains("failed to connect")) {
                        tv_no_records.setText(R.string.no_internet_connection);
                    } else {
                        tv_no_records.setText(R.string.no_record);
                    }
                }
            });
        }
    }

    private void initScrollListener(int lastover, String mhightlight, int innings) {
        rv_commentry.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int topRowVerticalPosition = recyclerView.getChildCount() == 0 ? 0 : recyclerView.getChildAt(0).getTop();
                swipeRefreshLayout.setEnabled(topRowVerticalPosition >= 0);
                if (!isLoading) {
                    if (linearLayoutManager != null && linearLayoutManager.findLastCompletelyVisibleItemPosition() == updatelistiem.size() - 1) {
                        loadMore(lastover, mhightlight, innings);
                        isLoading = true;
                    }
                }
            }
        });
    }

    private void loadMore(int lastover, String mhightlight, int innings) {
        LoadMoreDataList(lastover, mhightlight, innings);
    }

    private void LoadMoreDataList(int lastover, String mhightlight, int innings) {
        try {
            mypb.setVisibility(View.VISIBLE);
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(MyApplication.get_cricApi())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            APIService service = retrofit.create(APIService.class);
            Call<CommentaryModel> call = service.getComments(mdatasArrayList.get(mposition).getId(), "eccn:true", 10, lastover + "", true, innings, mhightlight);
            call.enqueue(new Callback<CommentaryModel>() {
                @Override
                public void onResponse(@NonNull Call<CommentaryModel> call, @NonNull Response<CommentaryModel> response) {
                    try {
                        rv_commentry.setVisibility(View.VISIBLE);
                        Handler handler = new Handler();
                        handler.postDelayed(() -> {
                            isFetching = false;
                            if (response.body() != null) {
                                oversArrayList = response.body().getInnings().getOvers();
                                int currentSize;
                                currentSize = updatelistiem.size();
                                int nextLimit = currentSize + oversArrayList.size();
                                int a = 0;
                                for (int i = updatelistiem.size(); i < nextLimit; i++) {
                                    updatelistiem.add(oversArrayList.get(a));
                                    a++;
                                    matchAdapter.notifyItemChanged(i);
                                }
                                mypb.setVisibility(View.GONE);
                                isLoading = false;
                            }
                        }, 2000);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NotNull Call<CommentaryModel> call, @NotNull Throwable t) {
                    mypb.setVisibility(View.GONE);
                    tv_no_records.setVisibility(View.VISIBLE);
                    rv_commentry.setVisibility(View.GONE);
                    if (Objects.requireNonNull(t.getMessage()).contains("Unable to resolve host") || t.getMessage().contains("failed to connect")) {
                        tv_no_records.setText(R.string.no_internet_connection);
                    } else {
                        tv_no_records.setText(R.string.no_record);
                    }
                }
            });
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    @Override
    public void onPause() {
        handler.removeCallbacks(runnable);
        super.onPause();
    }

    @Override
    public void onDestroy() {
        handler.removeCallbacks(runnable);
        super.onDestroy();
    }
}