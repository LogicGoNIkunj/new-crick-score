package com.ltos.livecrkttv.utils;

import android.app.Activity;
import android.net.Uri;

public class WebviewFallback implements CustomTabActivityHelper.CustomTabFallback {
    @Override
    public void openUri(Activity activity, Uri uri) {}
}