package com.ltos.livecrkttv.utils;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.util.Base64;

import androidx.appcompat.app.AppCompatDelegate;

import com.ltos.livecrkttv.R;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;


public class MyApplication extends Application {

    public static SharedPreferences sharedPreferences;
    public static SharedPreferences.Editor editor;
    public static String MYSECRET;
    @SuppressLint("StaticFieldLeak")
    public static AppOpenManager appOpenManager;
    @SuppressLint("StaticFieldLeak")
    public static Context context;


    @Override
    public void onCreate() {
        super.onCreate();
        context = this;
        appOpenManager = new AppOpenManager(this);
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);

        sharedPreferences = getSharedPreferences("ps", MODE_PRIVATE);
        editor = sharedPreferences.edit();

        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "com.ltos.livecrkttv", PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String key = Base64.encodeToString(md.digest(), Base64.NO_WRAP);
                key = key.replaceAll("[^a-zA-Z0-9]", "");
                MYSECRET = key;
            }
        } catch (PackageManager.NameNotFoundException | NoSuchAlgorithmException ignored) {

        }

    }


    //for api
    public static void set_cricApi(String url) {
        editor.putString("criapi", url).commit();
    }

    public static String get_cricApi() {
        return sharedPreferences.getString("criapi", "");
    }

    //for point
    public static void set_cricApipoint(String url) {
        editor.putString("criapipoint", url).commit();
    }

    public static String get_cricApipoint() {
        return sharedPreferences.getString("criapipoint", "");
    }


    //for point 2022
    public static void set_cricApipoint2022(String url) {
        editor.putString("criapipoint2022", url).commit();
    }

    public static String get_cricApipoint2022() {
        return sharedPreferences.getString("criapipoint2022", "");
    }


    //ads

    public static void set_AdsInt(int adsInt) {
        editor.putInt("adsInt", adsInt).commit();
    }

    public static int get_AdsInt() {
        return sharedPreferences.getInt("adsInt", 3);
    }

    //admob interstitial
    public static void set_Admob_interstitial_Id(String Admob_interstitial_Id) {
        editor.putString("Admob_interstitial_Id", Admob_interstitial_Id).commit();
    }

    public static String get_Admob_interstitial_Id() {
        return sharedPreferences.getString("Admob_interstitial_Id", context.getString(R.string.interestial_add));
    }

    //admob open
    public static void set_Admob_banner_Id(String Admob_banner_Id) {
        editor.putString("Admob_banner_Id", Admob_banner_Id).commit();
    }

    public static String get_Admob_banner_Id() {
        return sharedPreferences.getString("Admob_banner_Id", context.getString(R.string.banner_id));
    }

    //admob native
    public static void set_Admob_native_Id(String Admob_native_Id) {
        editor.putString("Admob_native_Id", Admob_native_Id).commit();
    }

    public static String get_Admob_native_Id() {
        return sharedPreferences.getString("Admob_native_Id", context.getString(R.string.native_big_add));
    }

    //admob openapp
    public static void set_Admob_openapp(String Admob_native_Id) {
        editor.putString("Admob_open_Id", Admob_native_Id).commit();
    }

    public static String get_Admob_openapp() {
        return sharedPreferences.getString("Admob_open_Id", context.getString(R.string.open_app_ad_id));
    }

}
