package com.ltos.livecrkttv.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class MatchesModel {
    @SerializedName("UpcomingFixtures")
    public ArrayList<Datas> UpcomingFixtures;
    @SerializedName("InProgressFixtures")
    public ArrayList<Datas> InProgressFixtures;
    @SerializedName("CompletedFixtures")
    public ArrayList<Datas> CompletedFixtures;

    public ArrayList<Datas> getUpcomingFixtures() {
        return UpcomingFixtures;
    }

    public ArrayList<Datas> getInProgressFixtures() {
        return InProgressFixtures;
    }

    public ArrayList<Datas> getCompletedFixtures() {
        return CompletedFixtures;
    }

    public static class Datas implements Parcelable {
        @SerializedName("Competition")
        public Competition competition;
        @SerializedName("HomeTeam")
        public Team homeTeam;
        @SerializedName("AwayTeam")
        public Team awayTeam;
        @SerializedName("Venue")
        public Venue venue;
        @SerializedName("Innings")
        public ArrayList<Innings> innings;
        @SerializedName("Id")
        public int Id;
        @SerializedName("Name")
        public String Name;
        @SerializedName("StartDateTime")
        public String StartDateTime;
        @SerializedName("EndDateTime")
        public String EndDateTime;
        @SerializedName("GameTypeId")
        public int GameTypeId;
        @SerializedName("GameType")
        public String GameType;
        @SerializedName("IsLive")
        public boolean IsLive;
        @SerializedName("IsCompleted")
        public boolean IsCompleted;
        @SerializedName("IsDuckworthLewis")
        public boolean IsDuckworthLewis;
        @SerializedName("MatchDay")
        public int MatchDay;
        @SerializedName("NumberOfDays")
        public int NumberOfDays;
        @SerializedName("ResultText")
        public String ResultText;
        @SerializedName("ResultTypeId")
        public String ResultTypeId;
        @SerializedName("ResultType")
        public String ResultType;
        @SerializedName("GameStatusId")
        public String GameStatusId;
        @SerializedName("GameStatus")
        public String GameStatus;
        @SerializedName("TossResult")
        public String TossResult;
        @SerializedName("TossDecision")
        public String TossDecision;
        @SerializedName("TicketUrl")
        public String TicketUrl;
        @SerializedName("Featured")
        public boolean Featured;
        @SerializedName("IsWomensMatch")
        public boolean IsWomensMatch;
        @SerializedName("FanHashTag")
        public String FanHashTag;
        @SerializedName("TwitterHandle")
        public String TwitterHandle;
        @SerializedName("SocialEventId")
        public String SocialEventId;
        @SerializedName("TuneIn")
        public boolean TuneIn;
        @SerializedName("MatchDayHomePageImageUrl")
        public String MatchDayHomePageImageUrl;
        @SerializedName("FanSocialEventId")
        public String FanSocialEventId;
        @SerializedName("IsVideoReplays")
        public boolean IsVideoReplays;
        @SerializedName("GamedayStatus")
        public String GamedayStatus;
        @SerializedName("IsEnableGameday")
        public boolean IsEnableGameday;
        @SerializedName("MoreInfoUrl")
        public String MoreInfoUrl;
        @SerializedName("OversRemaining")
        public String OversRemaining;
        @SerializedName("Order")
        public int Order;
        @SerializedName("CompetitionId")
        public int CompetitionId;
        @SerializedName("VenueId")
        public int VenueId;
        @SerializedName("HomeTeamId")
        public int HomeTeamId;
        @SerializedName("AwayTeamId")
        public int AwayTeamId;
        @SerializedName("LegacyFixtureId")
        public int LegacyFixtureId;
        @SerializedName("IsInProgress")
        public boolean IsInProgress;
        @SerializedName("TotalOvers")
        public String TotalOvers;
        @SerializedName("IsPublished")
        public boolean IsPublished;

        @SerializedName("channels")
        public ArrayList<Channels> channels;
        @SerializedName("officials")
        public ArrayList<Officials> officials;

        public int type;

        public Datas(int type) {
            this.type = type;
        }

        protected Datas(Parcel in) {
            Id = in.readInt();
            Name = in.readString();
            StartDateTime = in.readString();
            EndDateTime = in.readString();
            GameTypeId = in.readInt();
            GameType = in.readString();
            IsLive = in.readByte() != 0;
            IsCompleted = in.readByte() != 0;
            IsDuckworthLewis = in.readByte() != 0;
            MatchDay = in.readInt();
            NumberOfDays = in.readInt();
            ResultText = in.readString();
            ResultTypeId = in.readString();
            ResultType = in.readString();
            GameStatusId = in.readString();
            GameStatus = in.readString();
            TossResult = in.readString();
            TossDecision = in.readString();
            TicketUrl = in.readString();
            Featured = in.readByte() != 0;
            IsWomensMatch = in.readByte() != 0;
            FanHashTag = in.readString();
            TwitterHandle = in.readString();
            SocialEventId = in.readString();
            TuneIn = in.readByte() != 0;
            MatchDayHomePageImageUrl = in.readString();
            FanSocialEventId = in.readString();
            IsVideoReplays = in.readByte() != 0;
            GamedayStatus = in.readString();
            IsEnableGameday = in.readByte() != 0;
            MoreInfoUrl = in.readString();
            OversRemaining = in.readString();
            Order = in.readInt();
            CompetitionId = in.readInt();
            VenueId = in.readInt();
            HomeTeamId = in.readInt();
            AwayTeamId = in.readInt();
            LegacyFixtureId = in.readInt();
            IsInProgress = in.readByte() != 0;
            TotalOvers = in.readString();
            IsPublished = in.readByte() != 0;
        }

        public static final Creator<Datas> CREATOR = new Creator<Datas>() {
            @Override
            public Datas createFromParcel(Parcel in) {
                return new Datas(in);
            }

            @Override
            public Datas[] newArray(int size) {
                return new Datas[size];
            }
        };

        public Competition getCompetition() {
            return competition;
        }

        public Team getHomeTeam() {
            return homeTeam;
        }

        public Team getAwayTeam() {
            return awayTeam;
        }

        public Venue getVenue() {
            return venue;
        }

        public ArrayList<Innings> getInnings() {
            return innings;
        }

        public int getId() {
            return Id;
        }

        public String getName() {
            return Name;
        }

        public String getStartDateTime() {
            return StartDateTime;
        }

        public String getEndDateTime() {
            return EndDateTime;
        }

        public int getGameTypeId() {
            return GameTypeId;
        }

        public String getGameType() {
            return GameType;
        }

        public boolean isLive() {
            return IsLive;
        }

        public boolean isCompleted() {
            return IsCompleted;
        }

        public boolean isDuckworthLewis() {
            return IsDuckworthLewis;
        }

        public int getMatchDay() {
            return MatchDay;
        }

        public int getNumberOfDays() {
            return NumberOfDays;
        }

        public String getResultText() {
            return ResultText;
        }

        public String getResultTypeId() {
            return ResultTypeId;
        }

        public String getResultType() {
            return ResultType;
        }

        public String getGameStatusId() {
            return GameStatusId;
        }

        public String getGameStatus() {
            return GameStatus;
        }

        public String getTossResult() {
            return TossResult;
        }

        public String getTossDecision() {
            return TossDecision;
        }

        public String getTicketUrl() {
            return TicketUrl;
        }

        public boolean isFeatured() {
            return Featured;
        }

        public boolean isWomensMatch() {
            return IsWomensMatch;
        }

        public String getFanHashTag() {
            return FanHashTag;
        }

        public String getTwitterHandle() {
            return TwitterHandle;
        }

        public String getSocialEventId() {
            return SocialEventId;
        }

        public boolean isTuneIn() {
            return TuneIn;
        }

        public String getMatchDayHomePageImageUrl() {
            return MatchDayHomePageImageUrl;
        }

        public String getFanSocialEventId() {
            return FanSocialEventId;
        }

        public boolean isVideoReplays() {
            return IsVideoReplays;
        }

        public String getGamedayStatus() {
            return GamedayStatus;
        }

        public boolean isEnableGameday() {
            return IsEnableGameday;
        }

        public String getMoreInfoUrl() {
            return MoreInfoUrl;
        }

        public String getOversRemaining() {
            return OversRemaining;
        }

        public int getOrder() {
            return Order;
        }

        public int getCompetitionId() {
            return CompetitionId;
        }

        public int getVenueId() {
            return VenueId;
        }

        public int getHomeTeamId() {
            return HomeTeamId;
        }

        public int getAwayTeamId() {
            return AwayTeamId;
        }

        public int getLegacyFixtureId() {
            return LegacyFixtureId;
        }

        public boolean isInProgress() {
            return IsInProgress;
        }

        public String getTotalOvers() {
            return TotalOvers;
        }

        public boolean isPublished() {
            return IsPublished;
        }

        public ArrayList<Channels> getChannels() {
            return channels;
        }

        public ArrayList<Officials> getOfficials() {
            return officials;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeInt(Id);
            parcel.writeString(Name);
            parcel.writeString(StartDateTime);
            parcel.writeString(EndDateTime);
            parcel.writeInt(GameTypeId);
            parcel.writeString(GameType);
            parcel.writeByte((byte) (IsLive ? 1 : 0));
            parcel.writeByte((byte) (IsCompleted ? 1 : 0));
            parcel.writeByte((byte) (IsDuckworthLewis ? 1 : 0));
            parcel.writeInt(MatchDay);
            parcel.writeInt(NumberOfDays);
            parcel.writeString(ResultText);
            parcel.writeString(ResultTypeId);
            parcel.writeString(ResultType);
            parcel.writeString(GameStatusId);
            parcel.writeString(GameStatus);
            parcel.writeString(TossResult);
            parcel.writeString(TossDecision);
            parcel.writeString(TicketUrl);
            parcel.writeByte((byte) (Featured ? 1 : 0));
            parcel.writeByte((byte) (IsWomensMatch ? 1 : 0));
            parcel.writeString(FanHashTag);
            parcel.writeString(TwitterHandle);
            parcel.writeString(SocialEventId);
            parcel.writeByte((byte) (TuneIn ? 1 : 0));
            parcel.writeString(MatchDayHomePageImageUrl);
            parcel.writeString(FanSocialEventId);
            parcel.writeByte((byte) (IsVideoReplays ? 1 : 0));
            parcel.writeString(GamedayStatus);
            parcel.writeByte((byte) (IsEnableGameday ? 1 : 0));
            parcel.writeString(MoreInfoUrl);
            parcel.writeString(OversRemaining);
            parcel.writeInt(Order);
            parcel.writeInt(CompetitionId);
            parcel.writeInt(VenueId);
            parcel.writeInt(HomeTeamId);
            parcel.writeInt(AwayTeamId);
            parcel.writeInt(LegacyFixtureId);
            parcel.writeByte((byte) (IsInProgress ? 1 : 0));
            parcel.writeString(TotalOvers);
            parcel.writeByte((byte) (IsPublished ? 1 : 0));
        }

        public static class Competition implements Parcelable{
            @SerializedName("Id")
            public final int Id;
            @SerializedName("Name")
            public final String Name;
            @SerializedName("Url")
            public final String Url;
            @SerializedName("ImageUrl")
            public final String ImageUrl;
            @SerializedName("BannerImageUrl")
            public final String BannerImageUrl;
            @SerializedName("DrinksNotificationEnabled")
            public final boolean DrinksNotificationEnabled;
            @SerializedName("Order")
            public final int Order;
            @SerializedName("TwitterHandle")
            public final String TwitterHandle;
            @SerializedName("StartDateTime")
            public final String StartDateTime;
            @SerializedName("EndDateTime")
            public final String EndDateTime;
            @SerializedName("ThemeUrl")
            public final String ThemeUrl;
            @SerializedName("ViewerVerdict")
            public final boolean ViewerVerdict;
            @SerializedName("Priority")
            public final String Priority;
            @SerializedName("StatisticsProvider")
            public final String StatisticsProvider;
            @SerializedName("RelatedSeriesIds")
            public final String RelatedSeriesIds;
            @SerializedName("IsWomensCompetition")
            public final boolean IsWomensCompetition;
            @SerializedName("Formats")
            public ArrayList<formats> Formats;
            @SerializedName("LegacyCompetitionId")
            public final int LegacyCompetitionId;
            @SerializedName("IsPublished")
            public final boolean IsPublished;
            @SerializedName("SitecoreId")
            public final String SitecoreId;

            protected Competition(Parcel in) {
                Id = in.readInt();
                Name = in.readString();
                Url = in.readString();
                ImageUrl = in.readString();
                BannerImageUrl = in.readString();
                DrinksNotificationEnabled = in.readByte() != 0;
                Order = in.readInt();
                TwitterHandle = in.readString();
                StartDateTime = in.readString();
                EndDateTime = in.readString();
                ThemeUrl = in.readString();
                ViewerVerdict = in.readByte() != 0;
                Priority = in.readString();
                StatisticsProvider = in.readString();
                RelatedSeriesIds = in.readString();
                IsWomensCompetition = in.readByte() != 0;
                LegacyCompetitionId = in.readInt();
                IsPublished = in.readByte() != 0;
                SitecoreId = in.readString();
            }

            public static final Creator<Competition> CREATOR = new Creator<Competition>() {
                @Override
                public Competition createFromParcel(Parcel in) {
                    return new Competition(in);
                }

                @Override
                public Competition[] newArray(int size) {
                    return new Competition[size];
                }
            };

            public int getId() {
                return Id;
            }

            public String getName() {
                return Name;
            }

            public String getUrl() {
                return Url;
            }

            public String getImageUrl() {
                return ImageUrl;
            }

            public String getBannerImageUrl() {
                return BannerImageUrl;
            }

            public boolean isDrinksNotificationEnabled() {
                return DrinksNotificationEnabled;
            }

            public int getOrder() {
                return Order;
            }

            public String getTwitterHandle() {
                return TwitterHandle;
            }

            public String getStartDateTime() {
                return StartDateTime;
            }

            public String getEndDateTime() {
                return EndDateTime;
            }

            public String getThemeUrl() {
                return ThemeUrl;
            }

            public boolean isViewerVerdict() {
                return ViewerVerdict;
            }

            public String getPriority() {
                return Priority;
            }

            public String getStatisticsProvider() {
                return StatisticsProvider;
            }

            public String getRelatedSeriesIds() {
                return RelatedSeriesIds;
            }

            public boolean isWomensCompetition() {
                return IsWomensCompetition;
            }

            public ArrayList<formats> getFormats() {
                return Formats;
            }

            public int getLegacyCompetitionId() {
                return LegacyCompetitionId;
            }

            public boolean isPublished() {
                return IsPublished;
            }

            public String getSitecoreId() {
                return SitecoreId;
            }

            @Override
            public int describeContents() {
                return 0;
            }

            @Override
            public void writeToParcel(Parcel parcel, int i) {
                parcel.writeInt(Id);
                parcel.writeString(Name);
                parcel.writeString(Url);
                parcel.writeString(ImageUrl);
                parcel.writeString(BannerImageUrl);
                parcel.writeByte((byte) (DrinksNotificationEnabled ? 1 : 0));
                parcel.writeInt(Order);
                parcel.writeString(TwitterHandle);
                parcel.writeString(StartDateTime);
                parcel.writeString(EndDateTime);
                parcel.writeString(ThemeUrl);
                parcel.writeByte((byte) (ViewerVerdict ? 1 : 0));
                parcel.writeString(Priority);
                parcel.writeString(StatisticsProvider);
                parcel.writeString(RelatedSeriesIds);
                parcel.writeByte((byte) (IsWomensCompetition ? 1 : 0));
                parcel.writeInt(LegacyCompetitionId);
                parcel.writeByte((byte) (IsPublished ? 1 : 0));
                parcel.writeString(SitecoreId);
            }

            public static class formats {
                public String DisplayName;
                public String AssociatedMatchType;
                public String SeriesFormatName;

                public String getDisplayName() {
                    return DisplayName;
                }

                public String getAssociatedMatchType() {
                    return AssociatedMatchType;
                }

                public String getSeriesFormatName() {
                    return SeriesFormatName;
                }
            }
        }

        public static class Team {
            @SerializedName("Id")
            public int Id;
            @SerializedName("Name")
            public String Name;
            @SerializedName("ShortName")
            public String ShortName;
            @SerializedName("TeamColor")
            public String TeamColor;
            @SerializedName("LogoUrl")
            public String LogoUrl;
            @SerializedName("BackgroundImageUrl")
            public String BackgroundImageUrl;
            @SerializedName("TeambadgeImageUrl")
            public String TeambadgeImageUrl;
            @SerializedName("IsHomeTeam")
            public boolean IsHomeTeam;
            @SerializedName("IsTossWinner")
            public boolean IsTossWinner;
            @SerializedName("IsMatchWinner")
            public boolean IsMatchWinner;
            @SerializedName("BattingBonus")
            public float BattingBonus;
            @SerializedName("BowlingBonus")
            public float BowlingBonus;
            @SerializedName("IsActive")
            public boolean IsActive;
            @SerializedName("LegacyTeamId")
            public int LegacyTeamId;
            @SerializedName("FixtureId")
            public int FixtureId;

            public int getId() {
                return Id;
            }

            public String getName() {
                return Name;
            }

            public String getShortName() {
                return ShortName;
            }

            public String getTeamColor() {
                return TeamColor;
            }

            public String getLogoUrl() {
                return LogoUrl;
            }

            public String getBackgroundImageUrl() {
                return BackgroundImageUrl;
            }

            public String getTeambadgeImageUrl() {
                return TeambadgeImageUrl;
            }

            public boolean isHomeTeam() {
                return IsHomeTeam;
            }

            public boolean isTossWinner() {
                return IsTossWinner;
            }

            public boolean isMatchWinner() {
                return IsMatchWinner;
            }

            public float getBattingBonus() {
                return BattingBonus;
            }

            public float getBowlingBonus() {
                return BowlingBonus;
            }

            public boolean isActive() {
                return IsActive;
            }

            public int getLegacyTeamId() {
                return LegacyTeamId;
            }

            public int getFixtureId() {
                return FixtureId;
            }
        }

        public static class Venue {
            @SerializedName("Id")
            public int Id;
            @SerializedName("Name")
            public String Name;
            @SerializedName("City")
            public String City;
            @SerializedName("ImageUrl")
            public String ImageUrl;
            @SerializedName("Latitude")
            public String Latitude;
            @SerializedName("Longitude")
            public String Longitude;
            @SerializedName("PhoneNumber")
            public String PhoneNumber;

            public int getId() {
                return Id;
            }

            public String getName() {
                return Name;
            }

            public String getCity() {
                return City;
            }

            public String getImageUrl() {
                return ImageUrl;
            }

            public String getLatitude() {
                return Latitude;
            }

            public String getLongitude() {
                return Longitude;
            }

            public String getPhoneNumber() {
                return PhoneNumber;
            }
        }

        public static class Innings {
            @SerializedName("Id")
            public int Id;
            @SerializedName("FixtureId")
            public int FixtureId;
            @SerializedName("InningNumber")
            public int InningNumber;
            @SerializedName("BattingTeamId")
            public int BattingTeamId;
            @SerializedName("BowlingTeamId")
            public int BowlingTeamId;
            @SerializedName("IsDeclared")
            public boolean IsDeclared;
            @SerializedName("IsFollowOn")
            public boolean IsFollowOn;
            @SerializedName("IsForfeited")
            public boolean IsForfeited;
            @SerializedName("OvernightRuns")
            public int OvernightRuns;
            @SerializedName("OvernightWickets")
            public int OvernightWickets;
            @SerializedName("ByesRuns")
            public int ByesRuns;
            @SerializedName("LegByesRuns")
            public int LegByesRuns;
            @SerializedName("NoBalls")
            public int NoBalls;
            @SerializedName("Penalties")
            public int Penalties;
            @SerializedName("TotalExtras")
            public int TotalExtras;
            @SerializedName("WideBalls")
            public int WideBalls;
            @SerializedName("OversBowled")
            public String OversBowled;
            @SerializedName("RunsScored")
            public int RunsScored;
            @SerializedName("RequiredRunRate")
            public float RequiredRunRate;
            @SerializedName("CurrentRunRate")
            public float CurrentRunRate;
            @SerializedName("NumberOfWicketsFallen")
            public int NumberOfWicketsFallen;
            @SerializedName("Day")
            public int Day;
            @SerializedName("DuckworthLewisOvers")
            public String DuckworthLewisOvers;
            @SerializedName("overs")
            public ArrayList<CommentaryModel.Innings.Overs> overs;
            @SerializedName("batsmen")
            public ArrayList<Batsman> batsmen;
            @SerializedName("bowlers")
            public ArrayList<Bowlers> bowlers;
            @SerializedName("wickets")
            public ArrayList<Wickets> wickets;
            @SerializedName("partnership")
            public Partnership partnership;

            public int getId() {
                return Id;
            }

            public int getFixtureId() {
                return FixtureId;
            }

            public int getInningNumber() {
                return InningNumber;
            }

            public int getBattingTeamId() {
                return BattingTeamId;
            }

            public int getBowlingTeamId() {
                return BowlingTeamId;
            }

            public boolean isDeclared() {
                return IsDeclared;
            }

            public boolean isFollowOn() {
                return IsFollowOn;
            }

            public boolean isForfeited() {
                return IsForfeited;
            }

            public int getOvernightRuns() {
                return OvernightRuns;
            }

            public int getOvernightWickets() {
                return OvernightWickets;
            }

            public int getByesRuns() {
                return ByesRuns;
            }

            public int getLegByesRuns() {
                return LegByesRuns;
            }

            public int getNoBalls() {
                return NoBalls;
            }

            public int getPenalties() {
                return Penalties;
            }

            public int getTotalExtras() {
                return TotalExtras;
            }

            public int getWideBalls() {
                return WideBalls;
            }

            public String getOversBowled() {
                return OversBowled;
            }

            public int getRunsScored() {
                return RunsScored;
            }

            public float getCurrentRunRate() {
                return CurrentRunRate;
            }

            public int getNumberOfWicketsFallen() {
                return NumberOfWicketsFallen;
            }

            public int getDay() {
                return Day;
            }

            public String getDuckworthLewisOvers() {
                return DuckworthLewisOvers;
            }

            private static class Batsman {
                @SerializedName("playerId")
                public int playerId;
                @SerializedName("ballsFaced")
                public int ballsFaced;
                @SerializedName("bowledByPlayerId")
                public int bowledByPlayerId;
                @SerializedName("dismissalTypeId")
                public String dismissalTypeId;
                @SerializedName("dismissedByPlayerId")
                public int dismissedByPlayerId;
                @SerializedName("dismissalText")
                public String dismissalText;
                @SerializedName("battingStartDay")
                public int battingStartDay;
                @SerializedName("foursScored")
                public int foursScored;
                @SerializedName("sixesScored")
                public int sixesScored;
                @SerializedName("runsScored")
                public int runsScored;
                @SerializedName("battingMinutes")
                public int battingMinutes;
                @SerializedName("isOnStrike")
                public boolean isOnStrike;
                @SerializedName("isBatting")
                public boolean isBatting;
                @SerializedName("battingOrder")
                public int battingOrder;
                @SerializedName("isOut")
                public boolean isOut;
                @SerializedName("strikeRate")
                public float strikeRate;
            }

            private static class Bowlers {
                @SerializedName("playerId")
                public int playerId;
                @SerializedName("oversBowled")
                public String oversBowled;
                @SerializedName("maidensBowled")
                public int maidensBowled;
                @SerializedName("ballsBowled")
                public int ballsBowled;
                @SerializedName("dotBalls")
                public int dotBalls;
                @SerializedName("noBalls")
                public int noBalls;
                @SerializedName("wideBalls")
                public int wideBalls;
                @SerializedName("order")
                public int order;
                @SerializedName("runsConceded")
                public int runsConceded;
                @SerializedName("wicketsTaken")
                public int wicketsTaken;
                @SerializedName("isOnStrike")
                public boolean isOnStrike;
                @SerializedName("economy")
                public float economy;
            }

            private static class Wickets {
                @SerializedName("playerId")
                public int playerId;
                @SerializedName("order")
                public int order;
                @SerializedName("overBallDisplay")
                public String overBallDisplay;
                @SerializedName("runs")
                public int runs;
                @SerializedName("inningsBallId")
                public int inningsBallId;
            }

            private static class Partnership {
                @SerializedName("firstPlayerId")
                public int firstPlayerId;
                @SerializedName("firstPlayerBallsFaced")
                public int firstPlayerBallsFaced;
                @SerializedName("firstPlayerRunsScored")
                public int firstPlayerRunsScored;
                @SerializedName("firstPlayerIsOnStrike")
                public boolean firstPlayerIsOnStrike;
                @SerializedName("secondPlayerId")
                public int secondPlayerId;
                @SerializedName("secondPlayerBallsFaced")
                public int secondPlayerBallsFaced;
                @SerializedName("secondPlayerRunsScored")
                public int secondPlayerRunsScored;
                @SerializedName("secondPlayerIsOnStrike")
                public boolean secondPlayerIsOnStrike;
                @SerializedName("totalRunsScored")
                public int totalRunsScored;
                @SerializedName("isCurrent")
                public boolean isCurrent;
                @SerializedName("runrate")
                public float runrate;
                @SerializedName("totalBallsFaced")
                public int totalBallsFaced;
            }
        }

        public static class Channels {
            @SerializedName("channelConstantId")
            public int channelConstantId;
            @SerializedName("id")
            public int id;
            @SerializedName("channelLogo")
            public String channelLogo;
            @SerializedName("channelName")
            public String channelName;
            @SerializedName("channelURL")
            public String channelURL;
            @SerializedName("channelType")
            public String channelType;
            @SerializedName("registrationUrl")
            public String registrationUrl;

            public int getChannelConstantId() {
                return channelConstantId;
            }

            public int getId() {
                return id;
            }

            public String getChannelLogo() {
                return channelLogo;
            }

            public String getChannelName() {
                return channelName;
            }

            public String getChannelURL() {
                return channelURL;
            }

            public String getChannelType() {
                return channelType;
            }

            public String getRegistrationUrl() {
                return registrationUrl;
            }
        }

        public static class Officials {
            @SerializedName("firstName")
            public String firstName;
            @SerializedName("lastName")
            public String lastName;
            @SerializedName("initials")
            public String initials;
            @SerializedName("umpireType")
            public String umpireType;
            @SerializedName("hasRetired")
            public boolean hasRetired;

            public String getFirstName() {
                return firstName;
            }

            public String getLastName() {
                return lastName;
            }

            public String getInitials() {
                return initials;
            }

            public String getUmpireType() {
                return umpireType;
            }

            public boolean isHasRetired() {
                return hasRetired;
            }
        }
    }
}
