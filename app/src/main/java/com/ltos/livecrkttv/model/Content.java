package com.ltos.livecrkttv.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.List;

public class Content {
    @SerializedName("for")
    @Expose
    private String _for;
    @SerializedName("against")
    @Expose
    private String against;
    @SerializedName("Category")
    @Expose
    private String category;
    @SerializedName("CompetitionID")
    @Expose
    private Integer competitionID;
    @SerializedName("Deficit")
    @Expose
    private Integer deficit;
    @SerializedName("Draw")
    @Expose
    private Integer draw;
    @SerializedName("form")
    @Expose
    private List<String> form = null;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("IsQualified")
    @Expose
    private Object isQualified;
    @SerializedName("LeadBy")
    @Expose
    private Integer leadBy;
    @SerializedName("logo")
    @Expose
    private String logo;
    @SerializedName("logo_medium")
    @Expose
    private String logoMedium;
    @SerializedName("logoOutline")
    @Expose
    private String logoOutline;
    @SerializedName("lost")
    @Expose
    private Integer lost;
    @SerializedName("Matches")
    @Expose
    private Integer matches;
    @SerializedName("netrr")
    @Expose
    private String netrr;
    @SerializedName("NoResult")
    @Expose
    private Integer noResult;
    @SerializedName("OrderNo")
    @Expose
    private Integer orderNo;
    @SerializedName("pld")
    @Expose
    private Integer pld;
    @SerializedName("Points")
    @Expose
    private String points;
    @SerializedName("pts")
    @Expose
    private Integer pts;
    @SerializedName("Quotient")
    @Expose
    private String quotient;
    @SerializedName("roundBig")
    @Expose
    private String roundBig;
    @SerializedName("roundSmall")
    @Expose
    private String roundSmall;
    @SerializedName("StandingFlag")
    @Expose
    private String standingFlag;
    @SerializedName("team")
    @Expose
    private String team;
    @SerializedName("TeamID")
    @Expose
    private String teamID;
    @SerializedName("TeamLogo")
    @Expose
    private String teamLogo;
    @SerializedName("TeamName")
    @Expose
    private String teamName;
    @SerializedName("tied")
    @Expose
    private Integer tied;
    @SerializedName("won")
    @Expose
    private Integer won;

    public String getTeam() {
        return this.team;
    }

    public void setTeam(String str) {
        this.team = str;
    }

    public String getTeamName() {
        return this.teamName;
    }

    public void setTeamName(String str) {
        this.teamName = str;
    }

    public String getStandingFlag() {
        return this.standingFlag;
    }

    public void setStandingFlag(String str) {
        this.standingFlag = str;
    }

    public String getCategory() {
        return this.category;
    }

    public void setCategory(String str) {
        this.category = str;
    }

    public Integer getCompetitionID() {
        return this.competitionID;
    }

    public void setCompetitionID(Integer num) {
        this.competitionID = num;
    }

    public String getTeamLogo() {
        return this.teamLogo;
    }

    public void setTeamLogo(String str) {
        this.teamLogo = str;
    }

    public String getTeamID() {
        return this.teamID;
    }

    public void setTeamID(String str) {
        this.teamID = str;
    }

    public Integer getPld() {
        return this.pld;
    }

    public void setPld(Integer num) {
        this.pld = num;
    }

    public String getNetrr() {
        return this.netrr;
    }

    public void setNetrr(String str) {
        this.netrr = str;
    }

    public Integer getPts() {
        return this.pts;
    }

    public void setPts(Integer num) {
        this.pts = num;
    }

    public Integer getWon() {
        return this.won;
    }

    public void setWon(Integer num) {
        this.won = num;
    }

    public Integer getLost() {
        return this.lost;
    }

    public void setLost(Integer num) {
        this.lost = num;
    }

    public Integer getTied() {
        return this.tied;
    }

    public void setTied(Integer num) {
        this.tied = num;
    }

    public String getFor() {
        return this._for;
    }

    public void setFor(String str) {
        this._for = str;
    }

    public String getAgainst() {
        return this.against;
    }

    public void setAgainst(String str) {
        this.against = str;
    }

    public String getQuotient() {
        return this.quotient;
    }

    public void setQuotient(String str) {
        this.quotient = str;
    }

    public Integer getMatches() {
        return this.matches;
    }

    public void setMatches(Integer num) {
        this.matches = num;
    }

    public Integer getOrderNo() {
        return this.orderNo;
    }

    public void setOrderNo(Integer num) {
        this.orderNo = num;
    }

    public Object getIsQualified() {
        return this.isQualified;
    }

    public void setIsQualified(Object obj) {
        this.isQualified = obj;
    }

    public Integer getLeadBy() {
        return this.leadBy;
    }

    public void setLeadBy(Integer num) {
        this.leadBy = num;
    }

    public Integer getDeficit() {
        return this.deficit;
    }

    public void setDeficit(Integer num) {
        this.deficit = num;
    }

    public Integer getDraw() {
        return this.draw;
    }

    public void setDraw(Integer num) {
        this.draw = num;
    }

    public Integer getNoResult() {
        return this.noResult;
    }

    public void setNoResult(Integer num) {
        this.noResult = num;
    }

    public List<String> getForm() {
        return this.form;
    }

    public void setForm(List<String> list) {
        this.form = list;
    }

    public String getImage() {
        return this.image;
    }

    public void setImage(String str) {
        this.image = str;
    }

    public String getLogo() {
        return this.logo;
    }

    public void setLogo(String str) {
        this.logo = str;
    }

    public String getLogoMedium() {
        return this.logoMedium;
    }

    public void setLogoMedium(String str) {
        this.logoMedium = str;
    }

    public String getRoundSmall() {
        return this.roundSmall;
    }

    public void setRoundSmall(String str) {
        this.roundSmall = str;
    }

    public String getRoundBig() {
        return this.roundBig;
    }

    public void setRoundBig(String str) {
        this.roundBig = str;
    }

    public String getLogoOutline() {
        return this.logoOutline;
    }

    public void setLogoOutline(String str) {
        this.logoOutline = str;
    }

    public String getPoints() {
        return this.points;
    }

    public void setPoints(String str) {
        this.points = str;
    }
}
