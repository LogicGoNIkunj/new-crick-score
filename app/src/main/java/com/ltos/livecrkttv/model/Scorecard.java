package com.ltos.livecrkttv.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Scorecard {
    @SerializedName("fixture")
    public Fixture fixture;

    @SerializedName("players")
    public ArrayList<Players> players;

    public Fixture getFixture() {
        return fixture;
    }

    public ArrayList<Players> getPlayers() {
        return players;
    }

    public static class Fixture {
        @SerializedName("competition")
        public MatchesModel.Datas.Competition competition;
        @SerializedName("homeTeam")
        public Team homeTeam;
        @SerializedName("awayTeam")
        public Team awayTeam;
        @SerializedName("venue")
        public MatchesModel.Datas.Venue venue;
        @SerializedName("innings")
        public ArrayList<Innings> innings;
        @SerializedName("id")
        public int Id;
        @SerializedName("name")
        public String Name;
        @SerializedName("startDateTime")
        public String StartDateTime;
        @SerializedName("endDateTime")
        public String EndDateTime;
        @SerializedName("gameTypeId")
        public int GameTypeId;
        @SerializedName("gameType")
        public String GameType;
        @SerializedName("isLive")
        public boolean IsLive;
        @SerializedName("isCompleted")
        public boolean IsCompleted;
        @SerializedName("isDuckworthLewis")
        public boolean IsDuckworthLewis;
        @SerializedName("matchDay")
        public int MatchDay;
        @SerializedName("numberOfDays")
        public int NumberOfDays;
        @SerializedName("resultText")
        public String ResultText;
        @SerializedName("resultTypeId")
        public String ResultTypeId;
        @SerializedName("resultType")
        public String ResultType;
        @SerializedName("gameStatusId")
        public String GameStatusId;
        @SerializedName("gameStatus")
        public String GameStatus;
        @SerializedName("tossResult")
        public String tossResult;
        @SerializedName("tossDecision")
        public String tossDecision;
        @SerializedName("ticketUrl")
        public String TicketUrl;
        @SerializedName("featured")
        public boolean Featured;
        @SerializedName("isWomensMatch")
        public boolean IsWomensMatch;
        @SerializedName("fanHashTag")
        public String FanHashTag;
        @SerializedName("twitterHandle")
        public String TwitterHandle;
        @SerializedName("socialEventId")
        public String SocialEventId;
        @SerializedName("tuneIn")
        public boolean TuneIn;
        @SerializedName("matchDayHomePageImageUrl")
        public String MatchDayHomePageImageUrl;
        @SerializedName("fanSocialEventId")
        public String FanSocialEventId;
        @SerializedName("isVideoReplays")
        public boolean IsVideoReplays;
        @SerializedName("gamedayStatus")
        public String GamedayStatus;
        @SerializedName("isEnableGameday")
        public boolean IsEnableGameday;
        @SerializedName("moreInfoUrl")
        public String MoreInfoUrl;
        @SerializedName("oversRemaining")
        public String OversRemaining;
        @SerializedName("order")
        public int Order;
        @SerializedName("competitionId")
        public int CompetitionId;
        @SerializedName("venueId")
        public int VenueId;
        @SerializedName("homeTeamId")
        public int HomeTeamId;
        @SerializedName("awayTeamId")
        public int AwayTeamId;
        @SerializedName("legacyFixtureId")
        public int LegacyFixtureId;
        @SerializedName("isInProgress")
        public boolean IsInProgress;
        @SerializedName("totalOvers")
        public String TotalOvers;
        @SerializedName("isPublished")
        public boolean IsPublished;

        @SerializedName("channels")
        public ArrayList<MatchesModel.Datas.Channels> channels;
        @SerializedName("officials")
        public ArrayList<MatchesModel.Datas.Officials> officials;

        public MatchesModel.Datas.Competition getCompetition() {
            return competition;
        }

        public Team getHomeTeam() {
            return homeTeam;
        }

        public Team getAwayTeam() {
            return awayTeam;
        }

        public MatchesModel.Datas.Venue getVenue() {
            return venue;
        }

        public ArrayList<Innings> getInnings() {
            return innings;
        }

        public int getId() {
            return Id;
        }

        public String getName() {
            return Name;
        }

        public String getStartDateTime() {
            return StartDateTime;
        }

        public String getEndDateTime() {
            return EndDateTime;
        }

        public int getGameTypeId() {
            return GameTypeId;
        }

        public String getGameType() {
            return GameType;
        }

        public boolean isLive() {
            return IsLive;
        }

        public boolean isCompleted() {
            return IsCompleted;
        }

        public boolean isDuckworthLewis() {
            return IsDuckworthLewis;
        }

        public int getMatchDay() {
            return MatchDay;
        }

        public int getNumberOfDays() {
            return NumberOfDays;
        }

        public String getResultText() {
            return ResultText;
        }

        public String getResultTypeId() {
            return ResultTypeId;
        }

        public String getResultType() {
            return ResultType;
        }

        public String getGameStatusId() {
            return GameStatusId;
        }

        public String getGameStatus() {
            return GameStatus;
        }

        public String getTossResult() {
            return tossResult;
        }

        public String getTossDecision() {
            return tossDecision;
        }

        public String getTicketUrl() {
            return TicketUrl;
        }

        public boolean isFeatured() {
            return Featured;
        }

        public boolean isWomensMatch() {
            return IsWomensMatch;
        }

        public String getFanHashTag() {
            return FanHashTag;
        }

        public String getTwitterHandle() {
            return TwitterHandle;
        }

        public String getSocialEventId() {
            return SocialEventId;
        }

        public boolean isTuneIn() {
            return TuneIn;
        }

        public String getMatchDayHomePageImageUrl() {
            return MatchDayHomePageImageUrl;
        }

        public String getFanSocialEventId() {
            return FanSocialEventId;
        }

        public boolean isVideoReplays() {
            return IsVideoReplays;
        }

        public String getGamedayStatus() {
            return GamedayStatus;
        }

        public boolean isEnableGameday() {
            return IsEnableGameday;
        }

        public String getMoreInfoUrl() {
            return MoreInfoUrl;
        }

        public String getOversRemaining() {
            return OversRemaining;
        }

        public int getOrder() {
            return Order;
        }

        public int getCompetitionId() {
            return CompetitionId;
        }

        public int getVenueId() {
            return VenueId;
        }

        public int getHomeTeamId() {
            return HomeTeamId;
        }

        public int getAwayTeamId() {
            return AwayTeamId;
        }

        public int getLegacyFixtureId() {
            return LegacyFixtureId;
        }

        public boolean isInProgress() {
            return IsInProgress;
        }

        public String getTotalOvers() {
            return TotalOvers;
        }

        public boolean isPublished() {
            return IsPublished;
        }

        public ArrayList<MatchesModel.Datas.Channels> getChannels() {
            return channels;
        }

        public ArrayList<MatchesModel.Datas.Officials> getOfficials() {
            return officials;
        }

        private static class Team {
            @SerializedName("id")
            public int Id;
            @SerializedName("name")
            public String Name;
            @SerializedName("shortName")
            public String ShortName;
            @SerializedName("teamColor")
            public String TeamColor;
            @SerializedName("logoUrl")
            public String LogoUrl;
            @SerializedName("BackgroundImageUrl")
            public String BackgroundImageUrl;
            @SerializedName("TeambadgeImageUrl")
            public String TeambadgeImageUrl;
            @SerializedName("IsHomeTeam")
            public boolean IsHomeTeam;
            @SerializedName("IsTossWinner")
            public boolean IsTossWinner;
            @SerializedName("IsMatchWinner")
            public boolean IsMatchWinner;
            @SerializedName("BattingBonus")
            public float BattingBonus;
            @SerializedName("BowlingBonus")
            public float BowlingBonus;
            @SerializedName("IsActive")
            public boolean IsActive;
            @SerializedName("LegacyTeamId")
            public int LegacyTeamId;
            @SerializedName("FixtureId")
            public int FixtureId;

            public int getId() {
                return Id;
            }

            public String getName() {
                return Name;
            }

            public String getShortName() {
                return ShortName;
            }

            public String getTeamColor() {
                return TeamColor;
            }

            public String getLogoUrl() {
                return LogoUrl;
            }

            public String getBackgroundImageUrl() {
                return BackgroundImageUrl;
            }

            public String getTeambadgeImageUrl() {
                return TeambadgeImageUrl;
            }

            public boolean isHomeTeam() {
                return IsHomeTeam;
            }

            public boolean isTossWinner() {
                return IsTossWinner;
            }

            public boolean isMatchWinner() {
                return IsMatchWinner;
            }

            public float getBattingBonus() {
                return BattingBonus;
            }

            public float getBowlingBonus() {
                return BowlingBonus;
            }

            public boolean isActive() {
                return IsActive;
            }

            public int getLegacyTeamId() {
                return LegacyTeamId;
            }

            public int getFixtureId() {
                return FixtureId;
            }
        }

        public static class Innings {
            @SerializedName("id")
            public int Id;
            @SerializedName("fixtureId")
            public int FixtureId;
            @SerializedName("inningNumber")
            public int InningNumber;
            @SerializedName("battingTeamId")
            public int BattingTeamId;
            @SerializedName("bowlingTeamId")
            public int BowlingTeamId;
            @SerializedName("isDeclared")
            public boolean IsDeclared;
            @SerializedName("isFollowOn")
            public boolean IsFollowOn;
            @SerializedName("isForfeited")
            public boolean IsForfeited;
            @SerializedName("overnightRuns")
            public int OvernightRuns;
            @SerializedName("overnightWickets")
            public int OvernightWickets;
            @SerializedName("byesRuns")
            public int ByesRuns;
            @SerializedName("legByesRuns")
            public int LegByesRuns;
            @SerializedName("noBalls")
            public int NoBalls;
            @SerializedName("penalties")
            public int Penalties;
            @SerializedName("totalExtras")
            public int TotalExtras;
            @SerializedName("wideBalls")
            public int WideBalls;
            @SerializedName("oversBowled")
            public String OversBowled;
            @SerializedName("runsScored")
            public int RunsScored;
            @SerializedName("requiredRunRate")
            public float RequiredRunRate;
            @SerializedName("currentRunRate")
            public float CurrentRunRate;
            @SerializedName("numberOfWicketsFallen")
            public int NumberOfWicketsFallen;
            @SerializedName("day")
            public int Day;
            @SerializedName("duckworthLewisOvers")
            public String DuckworthLewisOvers;
            @SerializedName("overs")
            public ArrayList<CommentaryModel.Innings.Overs> overs;
            @SerializedName("batsmen")
            public ArrayList<Batsman> batsmen;
            @SerializedName("bowlers")
            public ArrayList<Bowlers> bowlers;
            @SerializedName("wickets")
            public ArrayList<Wickets> wickets;
            @SerializedName("partnership")
            public Partnership partnership;

            public int getId() {
                return Id;
            }

            public int getFixtureId() {
                return FixtureId;
            }

            public int getInningNumber() {
                return InningNumber;
            }

            public int getBattingTeamId() {
                return BattingTeamId;
            }

            public int getBowlingTeamId() {
                return BowlingTeamId;
            }

            public boolean isDeclared() {
                return IsDeclared;
            }

            public boolean isFollowOn() {
                return IsFollowOn;
            }

            public boolean isForfeited() {
                return IsForfeited;
            }

            public int getOvernightRuns() {
                return OvernightRuns;
            }

            public int getOvernightWickets() {
                return OvernightWickets;
            }

            public int getByesRuns() {
                return ByesRuns;
            }

            public int getLegByesRuns() {
                return LegByesRuns;
            }

            public int getNoBalls() {
                return NoBalls;
            }

            public int getPenalties() {
                return Penalties;
            }

            public int getTotalExtras() {
                return TotalExtras;
            }

            public int getWideBalls() {
                return WideBalls;
            }

            public String getOversBowled() {
                return OversBowled;
            }

            public int getRunsScored() {
                return RunsScored;
            }

            public float getCurrentRunRate() {
                return CurrentRunRate;
            }

            public int getNumberOfWicketsFallen() {
                return NumberOfWicketsFallen;
            }

            public int getDay() {
                return Day;
            }

            public String getDuckworthLewisOvers() {
                return DuckworthLewisOvers;
            }

            public float getRequiredRunRate() {
                return RequiredRunRate;
            }

            public ArrayList<CommentaryModel.Innings.Overs> getOvers() {
                return overs;
            }

            public ArrayList<Batsman> getBatsmen() {
                return batsmen;
            }

            public ArrayList<Bowlers> getBowlers() {
                return bowlers;
            }

            public ArrayList<Wickets> getWickets() {
                return wickets;
            }

            public Partnership getPartnership() {
                return partnership;
            }

            public static class Batsman {
                @SerializedName("playerId")
                public int playerId;
                @SerializedName("ballsFaced")
                public int ballsFaced;
                @SerializedName("bowledByPlayerId")
                public int bowledByPlayerId;
                @SerializedName("dismissalTypeId")
                public String dismissalTypeId;
                @SerializedName("dismissedByPlayerId")
                public int dismissedByPlayerId;
                @SerializedName("dismissalText")
                public String dismissalText;
                @SerializedName("battingStartDay")
                public int battingStartDay;
                @SerializedName("foursScored")
                public int foursScored;
                @SerializedName("sixesScored")
                public int sixesScored;
                @SerializedName("runsScored")
                public int runsScored;
                @SerializedName("battingMinutes")
                public int battingMinutes;
                @SerializedName("isOnStrike")
                public boolean isOnStrike;
                @SerializedName("isBatting")
                public boolean isBatting;
                @SerializedName("battingOrder")
                public int battingOrder;
                @SerializedName("isOut")
                public boolean isOut;
                @SerializedName("strikeRate")
                public float strikeRate;
            }

            public static class Bowlers {
                @SerializedName("playerId")
                public int playerId;
                @SerializedName("oversBowled")
                public String oversBowled;
                @SerializedName("maidensBowled")
                public int maidensBowled;
                @SerializedName("ballsBowled")
                public int ballsBowled;
                @SerializedName("dotBalls")
                public int dotBalls;
                @SerializedName("noBalls")
                public int noBalls;
                @SerializedName("wideBalls")
                public int wideBalls;
                @SerializedName("order")
                public int order;
                @SerializedName("runsConceded")
                public int runsConceded;
                @SerializedName("wicketsTaken")
                public int wicketsTaken;
                @SerializedName("isOnStrike")
                public boolean isOnStrike;
                @SerializedName("economy")
                public float economy;
            }

            public static class Wickets {
                @SerializedName("playerId")
                public int playerId;
                @SerializedName("order")
                public int order;
                @SerializedName("overBallDisplay")
                public String overBallDisplay;
                @SerializedName("runs")
                public int runs;
                @SerializedName("inningsBallId")
                public int inningsBallId;
            }

            public static class Partnership {
                @SerializedName("firstPlayerId")
                public int firstPlayerId;
                @SerializedName("firstPlayerBallsFaced")
                public int firstPlayerBallsFaced;
                @SerializedName("firstPlayerRunsScored")
                public int firstPlayerRunsScored;
                @SerializedName("firstPlayerIsOnStrike")
                public boolean firstPlayerIsOnStrike;
                @SerializedName("secondPlayerId")
                public int secondPlayerId;
                @SerializedName("secondPlayerBallsFaced")
                public int secondPlayerBallsFaced;
                @SerializedName("secondPlayerRunsScored")
                public int secondPlayerRunsScored;
                @SerializedName("secondPlayerIsOnStrike")
                public boolean secondPlayerIsOnStrike;
                @SerializedName("totalRunsScored")
                public int totalRunsScored;
                @SerializedName("isCurrent")
                public boolean isCurrent;
                @SerializedName("runrate")
                public float runrate;
                @SerializedName("totalBallsFaced")
                public int totalBallsFaced;
            }
        }

    }

    public static class Players {
        @SerializedName("id")
        public int Id;
        @SerializedName("teamId")
        public int teamId;
        @SerializedName("isCaptain")
        public boolean isCaptain;
        @SerializedName("isWicketKeeper")
        public boolean isWicketKeeper;
        @SerializedName("isTwelthMan")
        public boolean isTwelthMan;
        @SerializedName("isManOfTheMatch")
        public boolean isManOfTheMatch;
        @SerializedName("isManOfTheSeries")
        public boolean isManOfTheSeries;
        @SerializedName("manOfTheMatchOrder")
        public int manOfTheMatchOrder;
        @SerializedName("manOfTheSeriesOrder")
        public int manOfTheSeriesOrder;
        @SerializedName("order")
        public int order;
        @SerializedName("firstName")
        public String firstName;
        @SerializedName("lastName")
        public String lastName;
        @SerializedName("middleName")
        public String middleName;
        @SerializedName("initials")
        public String initials;
        @SerializedName("displayName")
        public String displayName;
        @SerializedName("type")
        public String type;
        @SerializedName("bowlingType")
        public String bowlingType;
        @SerializedName("battingHandId")
        public String battingHandId;
        @SerializedName("battingHand")
        public String battingHand;
        @SerializedName("dob")
        public String dob;
        @SerializedName("birthPlace")
        public String birthPlace;
        @SerializedName("imageUrl")
        public String imageUrl;
        @SerializedName("height")
        public String height;
        @SerializedName("didyouKnow")
        public String didyouKnow;
        @SerializedName("bio")
        public String bio;
        @SerializedName("playerDetails")
        public ArrayList<PlayerDetails> playerDetails;
        @SerializedName("isActive")
        public boolean isActive;
        @SerializedName("sitecoreShortGUID")
        public String sitecoreShortGUID;

        public int getId() {
            return Id;
        }

        public int getTeamId() {
            return teamId;
        }

        public boolean isCaptain() {
            return isCaptain;
        }

        public boolean isWicketKeeper() {
            return isWicketKeeper;
        }

        public boolean isTwelthMan() {
            return isTwelthMan;
        }

        public boolean isManOfTheMatch() {
            return isManOfTheMatch;
        }

        public boolean isManOfTheSeries() {
            return isManOfTheSeries;
        }

        public int getManOfTheMatchOrder() {
            return manOfTheMatchOrder;
        }

        public int getManOfTheSeriesOrder() {
            return manOfTheSeriesOrder;
        }

        public int getOrder() {
            return order;
        }

        public String getFirstName() {
            return firstName;
        }

        public String getLastName() {
            return lastName;
        }

        public String getMiddleName() {
            return middleName;
        }

        public String getInitials() {
            return initials;
        }

        public String getDisplayName() {
            return displayName;
        }

        public String getType() {
            return type;
        }

        public String getBattingHandId() {
            return battingHandId;
        }

        public String getBattingHand() {
            return battingHand;
        }

        public String getDob() {
            return dob;
        }

        public String getBirthPlace() {
            return birthPlace;
        }

        public String getImageUrl() {
            return imageUrl;
        }

        public String getHeight() {
            return height;
        }

        public String getDidyouKnow() {
            return didyouKnow;
        }

        public String getBio() {
            return bio;
        }

        public ArrayList<PlayerDetails> getPlayerDetails() {
            return playerDetails;
        }

        public boolean isActive() {
            return isActive;
        }

        public String getSitecoreShortGUID() {
            return sitecoreShortGUID;
        }

        private static class PlayerDetails {
            @SerializedName("playerId")
            public int playerId;
            @SerializedName("gameTypeId")
            public String gameTypeId;
            @SerializedName("gameType")
            public String gameType;
            @SerializedName("debutDate")
            public String debutDate;
            @SerializedName("imageUrl")
            public String imageUrl;

            public int getPlayerId() {
                return playerId;
            }

            public String getGameTypeId() {
                return gameTypeId;
            }

            public String getGameType() {
                return gameType;
            }

            public String getDebutDate() {
                return debutDate;
            }

            public String getImageUrl() {
                return imageUrl;
            }
        }
    }
}
