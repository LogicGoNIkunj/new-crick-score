package com.ltos.livecrkttv.model;

public class UrlModel {
    boolean status;
    Data data;

    public boolean isStatus() {
        return status;
    }

    public Data getData() {
        return data;
    }

    public static class Data {
        String url;

        String url2;

        String url3;

        public String getUrl() {
            return url;
        }

        public String getUrl2() {
            return url2;
        }

        public String getUrl3() {
            return url3;
        }
    }
}
