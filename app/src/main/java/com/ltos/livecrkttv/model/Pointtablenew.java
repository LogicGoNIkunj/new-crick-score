package com.ltos.livecrkttv.model;

import java.util.ArrayList;

public class Pointtablenew {
    public Data getData() {
        return data;
    }

    public Data data;

    public static class Data {
        public GetPointsTable getPointsTable;

        public GetPointsTable getGetPointsTable() {
            return getPointsTable;
        }

        public static class GetPointsTable {
            public ArrayList<Standing> standings;

            public ArrayList<Standing> getStandings() {
                return standings;
            }

            public static class Standing {
                public String name;
                public ArrayList<Team> teams;

                public String getName() {
                    return name;
                }

                public ArrayList<Team> getTeams() {
                    return teams;
                }


                public static class Team {
                    public int pos;
                    public String teamID;
                    public String teamName;
                    public String teamShortName;
                    public String all;
                    public String wins;
                    public String lost;
                    public String points;
                    public String nrr;
                    public boolean isQualified;

                    public int getPos() {
                        return pos;
                    }

                    public String getTeamID() {
                        return teamID;
                    }

                    public String getTeamName() {
                        return teamName;
                    }

                    public String getTeamShortName() {
                        return teamShortName;
                    }

                    public String getAll() {
                        return all;
                    }

                    public String getWins() {
                        return wins;
                    }

                    public String getLost() {
                        return lost;
                    }

                    public String getPoints() {
                        return points;
                    }

                    public String getNrr() {
                        return nrr;
                    }

                    public boolean isQualified() {
                        return isQualified;
                    }
                }


            }


        }


    }


}
