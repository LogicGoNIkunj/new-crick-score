package com.ltos.livecrkttv.model;

public class ScheduleListModel {

    String Match_title;
    String Match_no;
    String Match_date_time;
    String Match_team_one;
    String Match_team_two;
    int Match_one_picture;
    int Match_two_picture;
    String Match_venue;
    boolean isAd;

    public boolean getAd() {
        return isAd;
    }

    public void setAd(boolean ad) {
        isAd = ad;
    }

    public ScheduleListModel(String match_title, String match_no, String match_date_time, String match_team_one, String match_team_two, int match_one_picture, int match_two_picture, String match_venue,boolean yes) {
        Match_title = match_title;
        Match_no = match_no;
        Match_date_time = match_date_time;
        Match_team_one = match_team_one;
        Match_team_two = match_team_two;
        Match_one_picture = match_one_picture;
        Match_two_picture = match_two_picture;
        Match_venue = match_venue;
        isAd = yes;
    }

    public String getMatch_title() {
        return Match_title;
    }

    public void setMatch_title(String match_title) {
        Match_title = match_title;
    }

    public String getMatch_no() {
        return Match_no;
    }

    public void setMatch_no(String match_no) {
        Match_no = match_no;
    }

    public String getMatch_date_time() {
        return Match_date_time;
    }

    public void setMatch_date_time(String match_date_time) {
        Match_date_time = match_date_time;
    }

    public String getMatch_team_one() {
        return Match_team_one;
    }

    public void setMatch_team_one(String match_team_one) {
        Match_team_one = match_team_one;
    }

    public String getMatch_team_two() {
        return Match_team_two;
    }

    public void setMatch_team_two(String match_team_two) {
        Match_team_two = match_team_two;
    }

    public int getMatch_one_picture() {
        return Match_one_picture;
    }

    public void setMatch_one_picture(int match_one_picture) {
        Match_one_picture = match_one_picture;
    }

    public int getMatch_two_picture() {
        return Match_two_picture;
    }

    public void setMatch_two_picture(int match_two_picture) {
        Match_two_picture = match_two_picture;
    }

    public String getMatch_venue() {
        return Match_venue;
    }

    public void setMatch_venue(String match_venue) {
        Match_venue = match_venue;
    }
}
