package com.ltos.livecrkttv.model;

public class SquadsModel {
    public int Colour;
    public String batting_style;
    public String bowling_style;
    public String nationality;
    public String player_name;
    public String player_price;
    public int player_profile;
    public int player_status;
    public String role;

    public SquadsModel() {
    }

    public SquadsModel(int i2, String str, String str2, int i3, String str3, String str4, String str5, String str6, int i4) {
        this.player_profile = i2;
        this.player_name = str;
        this.player_price = str2;
        this.player_status = i3;
        this.role = str3;
        this.batting_style = str4;
        this.bowling_style = str5;
        this.nationality = str6;
        this.Colour = i4;
    }

    public String getBatting_style() {
        return this.batting_style;
    }

    public String getBowling_style() {
        return this.bowling_style;
    }

    public int getColour() {
        return this.Colour;
    }

    public String getNationality() {
        return this.nationality;
    }

    public String getPlayer_name() {
        return this.player_name;
    }

    public String getPlayer_price() {
        return this.player_price;
    }

    public int getPlayer_profile() {
        return this.player_profile;
    }

    public int getPlayer_status() {
        return this.player_status;
    }

    public String getRole() {
        return this.role;
    }

    public void setBatting_style(String str) {
        this.batting_style = str;
    }

    public void setBowling_style(String str) {
        this.bowling_style = str;
    }

    public void setColour(int i2) {
        this.Colour = i2;
    }

    public void setNationality(String str) {
        this.nationality = str;
    }

    public void setPlayer_name(String str) {
        this.player_name = str;
    }

    public void setPlayer_price(String str) {
        this.player_price = str;
    }

    public void setPlayer_profile(int i2) {
        this.player_profile = i2;
    }

    public void setPlayer_status(int i2) {
        this.player_status = i2;
    }

    public void setRole(String str) {
        this.role = str;
    }
}
