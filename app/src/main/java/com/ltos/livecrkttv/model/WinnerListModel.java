package com.ltos.livecrkttv.model;

public class WinnerListModel {
    public final String manoftheMatch;
    public final String orangeCap;
    public final int picture;
    public final String playeroftheTournament;
    public final String purpleCap;
    public final String runnerUp;
    public final String winner;
    public final String year;
    final boolean isAd;

    public boolean getAd() {
        return isAd;
    }


    public WinnerListModel(int i2, String str, String str2, String str3, String str4, String str5, String str6, String str7,boolean yes) {
        this.picture = i2;
        this.year = str;
        this.winner = str2;
        this.runnerUp = str3;
        this.orangeCap = str4;
        this.purpleCap = str5;
        this.manoftheMatch = str6;
        this.playeroftheTournament = str7;
        isAd = yes;
    }

    public String getManoftheMatch() {
        return this.manoftheMatch;
    }

    public String getOrangeCap() {
        return this.orangeCap;
    }

    public int getPicture() {
        return this.picture;
    }

    public String getPlayeroftheTournament() {
        return this.playeroftheTournament;
    }

    public String getPurpleCap() {
        return this.purpleCap;
    }

    public String getRunnerUp() {
        return this.runnerUp;
    }

    public String getWinner() {
        return this.winner;
    }

    public String getYear() {
        return this.year;
    }

}
