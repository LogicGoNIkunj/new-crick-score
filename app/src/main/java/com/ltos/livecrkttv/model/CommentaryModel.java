package com.ltos.livecrkttv.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class CommentaryModel {
    @SerializedName("inning")
    public Innings innings;
    @SerializedName("nextPage")
    public String nextPage;

    public Innings getInnings() {
        return innings;
    }

    public String getNextPage() {
        return nextPage;
    }

    public static class Innings {
        @SerializedName("id")
        public int id;
        @SerializedName("fixtureId")
        public int fixtureId;
        @SerializedName("inningNumber")
        public int inningNumber;
        @SerializedName("battingTeamId")
        public int battingTeamId;
        @SerializedName("bowlingTeamId")
        public int bowlingTeamId;
        @SerializedName("overs")
        public ArrayList<Overs> overs;
        @SerializedName("isDeclared")
        public boolean isDeclared;
        @SerializedName("isFollowOn")
        public boolean isFollowOn;
        @SerializedName("isForfeited")
        public boolean isForfeited;
        @SerializedName("overnightRuns")
        public int overnightRuns;
        @SerializedName("overnightWickets")
        public int overnightWickets;
        @SerializedName("byesRuns")
        public int byesRuns;
        @SerializedName("legByesRuns")
        public int legByesRuns;
        @SerializedName("noBalls")
        public int noBalls;
        @SerializedName("penalties")
        public int penalties;
        @SerializedName("totalExtras")
        public int totalExtras;
        @SerializedName("wideBalls")
        public int wideBalls;
        @SerializedName("oversBowled")
        public String oversBowled;
        @SerializedName("runsScored")
        public int runsScored;
        @SerializedName("currentRunRate")
        public float currentRunRate;
        @SerializedName("numberOfWicketsFallen")
        public int numberOfWicketsFallen;
        @SerializedName("day")
        public int day;
        @SerializedName("duckworthLewisOvers")
        public String duckworthLewisOvers;

        public int getId() {
            return id;
        }

        public int getFixtureId() {
            return fixtureId;
        }

        public int getInningNumber() {
            return inningNumber;
        }

        public int getBattingTeamId() {
            return battingTeamId;
        }

        public int getBowlingTeamId() {
            return bowlingTeamId;
        }

        public ArrayList<Overs> getOvers() {
            return overs;
        }

        public boolean isDeclared() {
            return isDeclared;
        }

        public boolean isFollowOn() {
            return isFollowOn;
        }

        public boolean isForfeited() {
            return isForfeited;
        }

        public int getOvernightRuns() {
            return overnightRuns;
        }

        public int getOvernightWickets() {
            return overnightWickets;
        }

        public int getByesRuns() {
            return byesRuns;
        }

        public int getLegByesRuns() {
            return legByesRuns;
        }

        public int getNoBalls() {
            return noBalls;
        }

        public int getPenalties() {
            return penalties;
        }

        public int getTotalExtras() {
            return totalExtras;
        }

        public int getWideBalls() {
            return wideBalls;
        }

        public String getOversBowled() {
            return oversBowled;
        }

        public int getRunsScored() {
            return runsScored;
        }

        public float getCurrentRunRate() {
            return currentRunRate;
        }

        public int getNumberOfWicketsFallen() {
            return numberOfWicketsFallen;
        }

        public int getDay() {
            return day;
        }

        public String getDuckworthLewisOvers() {
            return duckworthLewisOvers;
        }

        public static class Overs {
            @SerializedName("id")
            public int id;
            @SerializedName("overNumber")
            public int overNumber;
            @SerializedName("runrate")
            public float runrate;
            @SerializedName("runsConceded")
            public int runsConceded;
            @SerializedName("wickets")
            public int wickets;
            @SerializedName("runsExtras")
            public int runsExtras;
            @SerializedName("totalInningRuns")
            public int totalInningRuns;
            @SerializedName("totalInningWickets")
            public int totalInningWickets;
            @SerializedName("totalRuns")
            public int totalRuns;
            @SerializedName("balls")
            public ArrayList<Balls> balls;

            public int getId() {
                return id;
            }

            public int getOverNumber() {
                return overNumber;
            }

            public float getRunrate() {
                return runrate;
            }

            public int getRunsConceded() {
                return runsConceded;
            }

            public int getWickets() {
                return wickets;
            }

            public int getRunsExtras() {
                return runsExtras;
            }

            public int getTotalInningRuns() {
                return totalInningRuns;
            }

            public int getTotalInningWickets() {
                return totalInningWickets;
            }

            public int getTotalRuns() {
                return totalRuns;
            }

            public ArrayList<Balls> getBalls() {
                return balls;
            }

            public static class Balls {
                @SerializedName("ballDateTime")
                public String ballDateTime;
                @SerializedName("ballNumber")
                public int ballNumber;
                @SerializedName("isWicket")
                public boolean isWicket;
                @SerializedName("bowlerPlayerId")
                public int bowlerPlayerId;
                @SerializedName("battingPlayerId")
                public int battingPlayerId;
                @SerializedName("nonStrikeBattingPlayerId")
                public int nonStrikeBattingPlayerId;
                @SerializedName("shotAngle")
                public int shotAngle;
                @SerializedName("shotMagnitude")
                public int shotMagnitude;
                @SerializedName("battingHandId")
                public String battingHandId;
                @SerializedName("fieldingPosition")
                public String fieldingPosition;
                @SerializedName("runsConceded")
                public int runsConceded;
                @SerializedName("extras")
                public int extras;
                @SerializedName("runs")
                public int runs;
                @SerializedName("runsScored")
                public int runsScored;
                @SerializedName("comments")
                public ArrayList<Comments> comments;

                public String getBallDateTime() {
                    return ballDateTime;
                }

                public int getBallNumber() {
                    return ballNumber;
                }

                public boolean isWicket() {
                    return isWicket;
                }

                public int getBowlerPlayerId() {
                    return bowlerPlayerId;
                }

                public int getBattingPlayerId() {
                    return battingPlayerId;
                }

                public int getNonStrikeBattingPlayerId() {
                    return nonStrikeBattingPlayerId;
                }

                public int getShotAngle() {
                    return shotAngle;
                }

                public int getShotMagnitude() {
                    return shotMagnitude;
                }

                public String getBattingHandId() {
                    return battingHandId;
                }

                public String getFieldingPosition() {
                    return fieldingPosition;
                }

                public int getRunsConceded() {
                    return runsConceded;
                }

                public int getExtras() {
                    return extras;
                }

                public int getRuns() {
                    return runs;
                }

                public int getRunsScored() {
                    return runsScored;
                }

                public ArrayList<Comments> getComments() {
                    return comments;
                }

                public static class Comments {
                    @SerializedName("commentTypeId")
                    public String commentTypeId;
                    @SerializedName("message")
                    public String message;
                    @SerializedName("overNumber")
                    public int overNumber;

                    public String getCommentTypeId() {
                        return commentTypeId;
                    }

                    public String getMessage() {
                        return message;
                    }

                    public int getOverNumber() {
                        return overNumber;
                    }
                }
            }
        }
    }
}
