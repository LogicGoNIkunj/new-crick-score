package com.ltos.livecrkttv.model;

public class Playdata {
    private boolean status;
    private String message;
    private Datass data;

    public String getMessage() {
        return message;
    }

    public boolean isStatus() {
        return status;
    }

    public Datass getData() {
        return data;
    }

    public static class Datass {
        private String title;
        private String url;
        private String image;
        private boolean flage;

        public String getImage() {
            return image;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getUrl() {
            return url;
        }

        public boolean isFlage() {
            return flage;
        }

        public void setFlage(boolean flage) {
            this.flage = flage;
        }
    }
}