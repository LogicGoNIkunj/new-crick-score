package com.ltos.livecrkttv.admodels;

public class Feedback_model {

    boolean status;
    String message;

    public String getMessage() {
        return message;
    }

    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }
}
