package com.ltos.livecrkttv.admodels;

import com.ltos.livecrkttv.model.Example;
import com.ltos.livecrkttv.model.Pointtablenew;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface APIInterFace {
    String adsAPI = "https://stage-ads.punchapp.in/api/";

    @FormUrlEncoded
    @POST("get-ads-list")
    Call<AdsModel> getads(@Field("app_id") int IntVal);

    @FormUrlEncoded
    @POST("feedback")
    Call<Feedback_model> sendfeedback(
            @Field("app_name") String app_name,
            @Field("package_name") String package_name,
            @Field("title") String title,
            @Field("description") String description,
            @Field("device_name") String device_name,
            @Field("android_version") String android_version,
            @Field("version") String version
    );

    @GET("/pointstableData?year=2019")
    Call<Example> getUser2019();

    @GET("/pointstableData?year=2020")
    Call<Example> getUser2020();

    @GET("/pointstableData?year=2021")
    Call<Example> getUser2021();

    @GET("/pointstableData?year=2022")
    Call<Example> getUser2022();


    @POST("cricket")
    Call<Pointtablenew> getUsernew2022(@Body RequestBody params);

}