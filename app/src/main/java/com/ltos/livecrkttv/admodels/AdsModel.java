package com.ltos.livecrkttv.admodels;

import com.google.gson.annotations.SerializedName;

public class AdsModel {
    boolean status;
    String message;
    Data data;

    public boolean isStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    public Data getData() {
        return data;
    }

    public static class Data {

        int publisher_id;
        String publisher_name;
        publishers publishers;

        public int getPublisher_id() {
            return publisher_id;
        }

        public String getPublisher_name() {
            return publisher_name;
        }

        public Data.publishers getPublishers() {
            return publishers;
        }

        public static class publishers {
            @SerializedName("admob")
            Admob admob;


            public Admob getAdmob() {
                return admob;
            }


            public static class Admob {
                @SerializedName("interstitial")
                Admob_Interstitial admob_interstitial;
                @SerializedName("banner")
                Admob_Banner admob_banner;
                @SerializedName("native")
                Admob_Native admob_native;
                @SerializedName("open")
                Admob_Open admob_open;

                public Admob_Interstitial getAdmob_interstitial() {
                    return admob_interstitial;
                }

                public Admob_Banner getAdmob_banner() {
                    return admob_banner;
                }

                public Admob_Native getAdmob_native() {
                    return admob_native;
                }

                public Admob_Open getAdmob_open() {
                    return admob_open;
                }


                public static class Admob_Banner {
                    @SerializedName("id")
                    String admob_banner_id;
                    @SerializedName("show_time")
                    int admob_banner_show_time;

                    public String getAdmob_banner_id() {
                        return admob_banner_id;
                    }

                    public int getAdmob_banner_show_time() {
                        return admob_banner_show_time;
                    }
                }

                public static class Admob_Interstitial {
                    @SerializedName("id")
                    String admob_interstitial_id;
                    @SerializedName("show_time")
                    int admob_interstitial_show_time;

                    public String getAdmob_interstitial_id() {
                        return admob_interstitial_id;
                    }

                    public int getAdmob_interstitial_show_time() {
                        return admob_interstitial_show_time;
                    }
                }

                public static class Admob_Native {
                    @SerializedName("id")
                    String admob_Native_id;
                    @SerializedName("show_time")
                    int admob_Native_show_time;

                    public String getAdmob_Native_id() {
                        return admob_Native_id;
                    }

                    public int getAdmob_Native_show_time() {
                        return admob_Native_show_time;
                    }
                }
                public static class Admob_Open {
                    @SerializedName("id")
                    String admob_open_id;
                    @SerializedName("show_time")
                    int admob_Open_show_time;

                    public String getAdmob_Open_id() {
                        return admob_open_id;
                    }

                    public int getAdmob_Open_show_time() {
                        return admob_Open_show_time;
                    }
                }
            }
        }
    }
}